<?php

class Administration_ResourceController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->redirect("administration/resource/build");
    }

    public function buildAction()
    {
        if($this->getRequest()->isPost()){
         
            //search controllers and actions
            $objResources = new OSD_ACL_Resources();
            $objResources->buildAllArrays();
            $objResources->writeToDB();
        }
    }


}



