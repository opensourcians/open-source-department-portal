<?php

class Administration_Model_Main extends Zend_Db_Table_Abstract
{
    protected  $_name = "settings";
    
    function updateSettings($settings){
        return $this->update($settings,"id=1");
    }
    
    function getSettings(){
        return $this->fetchAll()->toArray();
    }

}

