<?php

class Administration_Model_Competition extends Zend_Db_Table_Abstract
{
    protected $_name="competition";
    public function addCompetition($name,$categoryComp_id) {
        $inserted = array('comp_name'=>$name,'categoryComp_id'=>$categoryComp_id);
        $id = $this->insert($inserted);
        return $id;
    }
    public function editCompetition($name,$categoryComp_id,$id) {
        $edited = array('comp_name'=>$name,'categoryComp_id'=>$categoryComp_id);
        $this->update($edited, "id=$id");
    }
    public function listCompetition() {
        $selected = $this->select()->setIntegrityCheck(false)
             ->from(array('compCat' => 'categoryCompetition'),
                    array('name'))
             ->join(array('comp' => 'competition'),
                    'comp.categoryComp_id = compCat.id');
     $this->fetchAll($selected)->toArray();
    }
    public function deleteCompetition($id) {
        $this->delete("id=$id");
    }
}

