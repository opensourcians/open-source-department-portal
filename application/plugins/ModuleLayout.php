<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModuleLayout
 *
 * @author Islam Askar 
 */
class Application_Plugin_ModuleLayout extends Zend_Controller_Plugin_Abstract {
    
    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $config     = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getOptions();
        $moduleName = $request->getModuleName();
        
        if (isset($config[$moduleName]['resources']['layout'])) {
            Zend_Layout::startMvc($config[$moduleName]['resources']['layout']);
        }
    }
}
