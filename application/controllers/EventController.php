<?php

class EventController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->redirect("/event/list");
    }

    public function listAction()
    {
        $eventModel = new Administration_Model_Event();
        $events = $eventModel->listEvents();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($events);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        
        $this->view->paginator = $paginator;
    }
    
    public function monthEventsAction(){
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isGet()) {
                $month = $this->getRequest()->getParam("month");
                $year = $this->getRequest()->getParam("year");
                $eventsModel = new Administration_Model_Event();
                $monthEvents = $eventsModel->getMonthEvents($month, $year);
                $this->_helper->json($monthEvents);
            }
        }
    }


}



