<?php

class Administration_Form_Category extends Zend_Form
{

    public function init()
    {
        
        $name = new Zend_Form_Element_Text("categoryname");
        $name->setRequired();
        $name->addValidator(new Zend_Validate_Alnum);
        $name->addFilter(new Zend_Filter_StripTags);
        $name->setAttrib("class", "form-control");
        $name->setAttrib("placeholder", "Enter Category Name");
        $name->setAttrib("id", "categoryname");
        $name->setLabel("Write Category");   
        $submit = new Zend_Form_Element_Submit("Add");
        $submit->setAttrib("class", "btn btn-default");
        $this->addElements(array($name,$submit));
        /* Form Elements & Other Definitions Here ... */
    }


}

