<?php

class Administration_Form_ContactCategoryEdit extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
         $this->setMethod('post');
        $this->addElement('text','cont_cat',array(
                                            'label'=>'Category Title:',
                                            'required'=>true , 
                                            'filters'=>array('StringTrim'),
                                            'class'=>'form-control',
                                            'placeholder'=>'Edit a Category') 
                         );
        $this->addElement('submit', 'submit', array(
                            'label'    => 'Update Contact Category',
                            'ignore'   => true,
                            'class'=>'btn btn-primary'
                        ));
    }


}

