<?php

class Administration_Model_AdmissionCategory extends Zend_Db_Table_Abstract
{
    protected $_name = "admissionCategory";
    
     function listAdmissionCategories(){
        return $this->fetchAll()->toArray();
    }
    
    function addAdmissionCategory($admissionCategoryInfo) {
        
        $row = $this->createRow();
        $row->name = $admissionCategoryInfo['name'];
                
        return $row->save();
    }
    
    function getAdmissionCategoryById($admissionCategoryId) {
        
        return $this->find($admissionCategoryId)->toArray();
    }
    
    function editAdmissionCategory($admissionCategoryId, $admissionCategoryInfo) {
        
        return $this->update($admissionCategoryInfo, "id = $admissionCategoryId");

    }
    
    function deleteAdmissionCategory($admissionCategoryId) {
        return $this->delete("id = $admissionCategoryId");
    }

}

