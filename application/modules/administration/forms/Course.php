<?php

class Administration_Form_Course extends Zend_Form
{

    public function init()
    {
        $categorylist = new Administration_Model_Categorycourse();
        $category_list =$categorylist->listAll();
        $coursecat = new Zend_Form_Element_Select("categoryname");
        $coursecat->setLabel("Select Category:");
        $coursecat->setRequired();
        $coursecat->setAttrib("class","form-control input-large");
        $coursecat->setAttrib("id", "category");
        $coursecat->addMultiOptions($category_list); 
        $name = new Zend_Form_Element_Text("coursename");
        $name->setRequired();
        $name->setAllowEmpty(false);
        $name->addValidator(new Zend_Validate_Alnum);
        $name->addFilter(new Zend_Filter_StripTags);
        $name->setAttrib("class", "form-control input-large");
        $name->setAttrib("placeholder", "Enter Course Name");
        $name->setAttrib("id", "coursename");
        $name->setLabel("Write Course Name");   
        $submit = new Zend_Form_Element_Submit("Add Course");
        $submit->setAttrib("class", "btn btn-primary");
        $this->addElements(array($coursecat,$name,$submit));
        /* Form Elements & Other Definitions Here ... */
    }


}

