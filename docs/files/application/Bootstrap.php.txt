<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * This function is used to intialise the layout and view placeholders
     * @author Islam Askar <isalah@iti.gov.eg>
     * 
     */
    protected function _initPlaceholders()   
    {
           $this->bootstrap('View');
           $view = $this->getResource('View');
           $view->doctype('XHTML1_STRICT');
           //Meta
           $view->headMeta()->appendName('keywords', 'ITI, Information, Technology, Institute, Open, Source')
                   ->appendName('description', 'Open Source Department is one of Information Technology Institute departments intiated on 2009 (Intake 30)')
                   ->appendHttpEquiv('viewport', 'width=device-width, initial-scale=1.0');
           // Set the initial title and separator:
           $view->headTitle('Open Source Department Website')
                ->setSeparator(' :: ');

           // Set the initial stylesheet:
           $view->headLink()->prependStylesheet('css/bootstrap.min.css');
           $view->headLink()->prependStylesheet('css/site.css');

           // Set the initial JS to load:
           $view->headScript()->prependFile('js/site.js');
           
    }


}


