<?php

class Administration_ForumcategoryController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $list = new Administration_Model_Forumcategory();
        $this->view->data = $list->listForumcategory();
    }

    public function addAction()
    {
        //$this->view->form = new Application_Form_Forumcategory();
        if($this->_request->isPost())
        {
            $add = new Administration_Model_Forumcategory();
            $add->addForumcategory($this->_request->getParam('name'),
                    $this->_request->getParam('description'));
            $this->redirect("/administration/forumcategory");
        }
    }

    public function editAction()
    {
        $list = new Administration_Model_Forumcategory();
        $id = $this->_request->getParam('id');
        $this->view->data = $list->getforumcategoryByid($this->_request->getParam('id'));
        if($this->_request->isPost())
        {
            $list->editForumcategory($this->_request->getParam('name'), 
                    $this->_request->getParam('description'), $id);
            $this->redirect("/administration/forumcategory/index");
        }
    }

    public function deleteAction()
    {
         $delete = new Administration_Model_Forumcategory();
        if($this->_request->isPost())
        {
            $delete->deleteForumcategory($this->_request->getParam('id'));
            $this->redirect('/administration/forumcategory');
        }
    }


}







