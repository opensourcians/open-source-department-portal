<?php

class Administration_NewscategoryController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/newscategory/list");
      
    }

    public function listAction()
    {
        $newsCategoryModel = new Administration_Model_NewsCategory();
        $newsCategories = $newsCategoryModel->listNewsCategories();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($newsCategories);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
        
    }

    public function addAction()
    {
        $newsCategoryForm = new Administration_Form_NewsCategory();


        $this->view->form = $newsCategoryForm;
        $this->view->post = "/administration/newscategory/add";
        $this->view->viewName = "Add News Category";


        if ($this->getRequest()->isPost()) {
            if ($newsCategoryForm->isValid($this->getRequest()->getParams())) {
                $newsCategoryModel = new Administration_Model_NewsCategory();

                $newsCategoryId = $newsCategoryModel->addNewsCategory($newsCategoryForm->getValues());

                if ($newsCategoryId) {
                    $this->_flashMessenger->addMessage('News Category added successfully!');
                    $this->redirect("/administration/newscategory/list");
                }
            } else {
                $this->view->errors = $newsCategoryForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction()
    {
         //Get instances
        $newsCategoryForm = new Administration_Form_NewsCategory();
        $newsCategoryModel = new Administration_Model_NewsCategory();


        //get user id from request
        $id = $this->getRequest()->getParam('id');

        //get user information
        $newsCategory = $newsCategoryModel->getNewsCategoryById($id);

        //send values to view
        $this->view->post = "/administration/newscategory/edit";
        $this->view->viewName = "Edit News Category";
        
        
        if ($this->getRequest()->isPost()) {
            if ($newsCategoryForm->isValid($this->getRequest()->getParams())) {

                $newsCategoryModel->editNewsCategory($newsCategoryForm->getValue("id"), $newsCategoryForm->getValues());

                $this->_flashMessenger->addMessage('News Category edited successfully!');
                $this->redirect("/administration/newscategory/list");
            } else {
                $this->view->errors = $newsCategoryForm->getMessages();
            }
        }

        if ($newsCategory) {
            $newsCategoryForm->populate($newsCategory[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no news category with this id!');
            $this->redirect("/administration/newscategory/list");
        }

        

        $this->view->form = $newsCategoryForm;
        $this->render("form");
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $newsCategoryModel = new Administration_Model_NewsCategory();
        $result = $newsCategoryModel->deleteNewsCategory($id);
        if ($result) {
            $this->_flashMessenger->addMessage('News Category deleted successfully!');
            echo $result;
        }
    }


}

