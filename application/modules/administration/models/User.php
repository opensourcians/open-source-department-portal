<?php

class Administration_Model_User extends Zend_Db_Table_Abstract
{

    protected $_name = 'user';

    function getChamiloInfo($email)
    {
        $parameters = array(
            'host'     => 'localhost',
            'username' => 'root',
            'password' => '110889',
            'dbname'   => 'chamilo'
        );
        try {
            $db = Zend_Db::factory('Pdo_Mysql', $parameters);
            $db->getConnection();
        } catch (Zend_Db_Adapter_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        } catch (Zend_Exception $e) {
            echo $e->getMessage();
            die('Could not connect to database.');
        }
        $stmt = $db->select()
            ->from("user", ['username', 'password'])
            ->where("username=?", $email)
            ->query();

        return $stmt->fetchAll();
    }

    function listUsers()
    {
        $select = $this->select()->setIntegrityCheck(FALSE)->from("user", array("*", "user.id as uId"))->join("role", "user.roleId=role.id");
        return $this->fetchAll($select)->toArray();
    }

    function listAll() {
        return $this->fetchAll()->toArray();
    }

    function addUser($userInfo)
    {

        $row = $this->createRow();
        $row->firstName = $userInfo['firstName'];
        $row->lastName = $userInfo['lastName'];
        $row->email = $userInfo['email'];
        $row->password = md5($userInfo['password']);
        $row->gender = $userInfo['gender'];
        //generate random name for the image to prevent overriding (Not implemented !)
        if ($userInfo['image']) {
            //get image extention
            $imageExt = substr(strrchr($userInfo['image'], '.'), 1);
            $randomName = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
            $userInfo['image'];
        }
        $row->image = $userInfo['image'];
        $row->isEnabled = $userInfo['isEnabled'];
        $row->roleId = $userInfo['roleId'];
        $row->jobTitle = $userInfo['jobTitle'];
        $row->description = $userInfo['description'];
        $row->facebook = $userInfo['facebook'];
        $row->twitter = $userInfo['twitter'];
        $row->linkedin = $userInfo['linkedin'];
        $row->google = $userInfo['google'];

        $rowId = $row->save();
        
        //If SuperAdmin, admin, student or graduate, add the ITI info
        $roleModel = new Administration_Model_Role();
        if ($userInfo['roleId'] == $roleModel->getSuperAdminRoleId() or
                $userInfo['roleId'] == $roleModel->getAdminRoleId() or
                $userInfo['roleId'] == $roleModel->getGraduateRoleId() or
                $userInfo['roleId'] == $roleModel->getStudentRoleId()) {

            $this->_db->insert("itian", array('userId' => $rowId, 'branchId' => $userInfo['branch'],
                'trackId' => $userInfo['track'], 'intakeId' => $userInfo['intake']));
        }
        return $rowId;
    }

    function editUser($userId, $userInfo)
    {

        //add updated on field
        $userInfo['updatedOn'] = date('Y-m-d H:i:s');
        //unset retype password
        unset($userInfo['passwordRetype']);
        //if password empty remove password
        if (empty($userInfo['password'])) {
            unset($userInfo['password']);
        } else {
            $userInfo['password'] = md5($userInfo['password']);
        }
        //if image empty remove image
        if (empty($userInfo['image']) && empty($userInfo['removeImage'])) {
            unset($userInfo['image']);
            unset($userInfo['removeImage']);
        } else {
            unset($userInfo['removeImage']);
        }

        //unset email
        if ($userInfo['email']) {
            unset($userInfo['email']);
        }
        
        //on change role
         $roleModel = new Administration_Model_Role();
        if ($userInfo['roleId'] == $roleModel->getSuperAdminRoleId() or
                $userInfo['roleId'] == $roleModel->getAdminRoleId() or
                $userInfo['roleId'] == $roleModel->getGraduateRoleId() or
                $userInfo['roleId'] == $roleModel->getStudentRoleId()) {
            $select = $this->_db->select()->from("itian")->where("userId = $userId");
            if ($this->_db->fetchRow($select)) {
                $this->_db->update("itian", array('branchId' => $userInfo['branch'],
                'trackId' => $userInfo['track'], 'intakeId' => $userInfo['intake']),"userId=".$userId);
            } else {
                $this->_db->insert("itian", array('userId' => $userId, 'branchId' => $userInfo['branch'],
                'trackId' => $userInfo['track'], 'intakeId' => $userInfo['intake']));
            }
        }
        unset($userInfo['branch']);
        unset($userInfo['track']);
        unset($userInfo['intake']);
        return $this->update($userInfo, "id = $userId");
    }

    function getUserById($userId) {
        
        $row =  $this->find($userId)->toArray();
        $roleModel = new Administration_Model_Role();
        
        if($row[0]['roleId'] == $roleModel->getSuperAdminRoleId() or
                $row[0]['roleId'] == $roleModel->getAdminRoleId() or
                $row[0]['roleId'] == $roleModel->getGraduateRoleId() or
                $row[0]['roleId'] == $roleModel->getStudentRoleId()) {

                    $select = $this->_db->select()->from("itian")->where("userId =".$row[0]['id']);
                    $result = $this->_db->fetchRow($select);
                    
                    $row[0]['track']=$result['trackId'];
                    $row[0]['branch']=$result['branchId'];
                    $row[0]['intake']=$result['intakeId'];
                }
                return $row;

    }

    function deleteUser($userId)
    {
        return $this->delete("id = $userId");
    }

    function getGraduates($branch = 0, $intake = 0, $track = 0) {
        
        $roleModel = new Administration_Model_Role();
        $graduateId = $roleModel->getGraduateRoleId();
        $superadminId = $roleModel->getSuperAdminRoleId();
        $adminId = $roleModel->getAdminRoleId();
        
        $select = $this->select("*")->setIntegrityCheck(false)->join("itian", "user.id=itian.userId")
                ->join("branch", "branch.id = itian.branchId")
                ->join("track", "track.id = itian.trackId")
                ->join("intake", "intake.id = itian.intakeId")
                ->where("user.roleId = ".$graduateId)
                ->orWhere("user.roleId = ".$superadminId)
                ->orWhere("user.roleId = ".$adminId);

        if ($branch)
            $select->where("branchId =" . $branch);

        if ($track)
            $select->where("trackId =" . $track);

        if ($intake)
            $select->where("intakeId =" . $branch);
        
        return $this->fetchAll($select)->toArray();
    }

}
