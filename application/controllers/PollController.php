<?php

class PollController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }
    
    public function resultAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isGet()) {
                $pollId = $this->getRequest()->getParam("pollId");
                $choiceId = $this->getRequest()->getParam("choiceId");
                if($choiceId){
                    $pollChoicesModel = new Administration_Model_PollChoices();
                    $pollChoicesModel->incrementPollChoice($choiceId);
                }
                $pollModel = new Administration_Model_Poll();
                $pollInfo = $pollModel->getPollForDisplayById($pollId);
                $this->_helper->json($pollInfo);
            }
        }
    }


}

