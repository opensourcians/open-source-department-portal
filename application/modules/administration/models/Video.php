<?php

class Administration_Model_Video extends Zend_Db_Table_Abstract
{
    protected $_name = 'video';
    
    function listVideos(){
        return $this->fetchAll()->toArray();
    }
    
    function addVideo($videoInfo){
        $row = $this->createRow();
        $row->videoTitle = $videoInfo['videoTitle'];
        $row->videoUrl = $videoInfo['videoUrl'];
        return $row->save();
    }
    
    function editVideo($videoId,$videoInfo){
        
        return $this->update($videoInfo, "id = $videoId");
    }
    
    function getVideoById($videoId){
        return $this->find($videoId)->toArray();
    }
    
    function deleteVideo($videoId) {
        return $this->delete("id = $videoId");      
    }


}

