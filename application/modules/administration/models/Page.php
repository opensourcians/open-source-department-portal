<?php

class Administration_Model_Page extends Zend_Db_Table_Abstract
{
    protected $_name = 'page';
    
    function listPages(){
        return $this->fetchAll()->toArray();
    }
    
    function addPage($pageInfo){
        $row = $this->createRow();
        $row->pageTitle = $pageInfo['pageTitle'];
        $row->body = $pageInfo['body'];
        return $row->save();
    }
    
    function editPage($pageId,$pageInfo){
        
        //add updated on field
        $pageInfo['updatedOn']= date('Y-m-d H:i:s');
        return $this->update($pageInfo, "id = $pageId");
    }
    
    function getPageById($pageId){
        return $this->find($pageId)->toArray();
    }
    
    function deletePage($pageId) {
        return $this->delete("id = $pageId");      
    }


}

