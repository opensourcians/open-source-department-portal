<?php

class Administration_Form_AdmissionQuestion extends Zend_Form {

    public function init() {

        $this->setMethod('post');
        $this->setName('news');
        
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $categoryId = new Zend_Form_Element_Select("categoryId");
        $categoryId->setRequired();
        $categoryId->addMultiOption("","Select Question Category");
        $categoryId->setAttrib("class", "form-control");
        $categoryId->setDecorators(array('ViewHelper'));
        $admissionCategoryModel = new Administration_Model_AdmissionCategory();
        $admissionCategories = $admissionCategoryModel->listAdmissionCategories();
        foreach($admissionCategories as $admissionCategory){
            $categoryId->addMultiOption($admissionCategory['id'],$admissionCategory['name']);
        }
        
        $levelId = new Zend_Form_Element_Select("levelId");
        $levelId->setRequired();
        $levelId->addMultiOption("","Select Question Level");
        $levelId->setAttrib("class", "form-control");
        $levelId->setDecorators(array('ViewHelper'));
        $admissionQuestionLevelModel = new Administration_Model_AdmissionQuestionLevel();
        $admissionQuestionLevels = $admissionQuestionLevelModel->listAdmissionQuestionLevels();
        foreach($admissionQuestionLevels as $admissionQuestionLevel){
            $levelId->addMultiOption($admissionQuestionLevel['id'],$admissionQuestionLevel['name']);
        }

        $question = new Zend_Form_Element_Text("title");
        $question->addFilter(new Zend_Filter_StripTags);
        $question->setAttrib("class", "form-control");
        $question->setAttrib("placeholder", "Enter Question");
        $question->setAttrib("id", "title");
        $question->setRequired();
        $question->removeDecorator('Label');
        $question->setDecorators(array('ViewHelper'));

        $answer = new Zend_Form_Element_Textarea("body");
        $answer->setRequired();
        $answer->setAttrib("class", "form-control");
        $answer->setAttrib("id", "ckeditor_full");
        $answer->setDecorators(array('ViewHelper'));
        

        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");

        $this->addElements(array($id, $categoryId, $levelId, $question, $answer, $submit));
    }

}
