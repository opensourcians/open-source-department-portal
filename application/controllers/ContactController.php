<?php

class ContactController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
        
    }

    public function indexAction()
    {
        $contactForm= new Application_Form_Contact();
        $this->view->form=$contactForm;
        $this->view->messages = $this->_flashMessenger->getMessages();
        if($this->_request->isPost())
        {
            if ($contactForm->isValid($this->_request->getParams())) 
            {
                $senderName=$this->_request->getParam('name');
                $senderEmail=$this->_request->getParam('email');
                $subject=$this->_request->getParam('subject');
                $message=$this->_request->getParam('message');
                $contactModel= new Application_Model_Contact();
                $contactCategoryModel = new Administration_Model_Contactcategory();
                $contactCategoryInfo = $contactCategoryModel->getCategoryById($subject);
                $contactModel->createMessages($senderName, $senderEmail, $subject, $message,"islam.askar@gmail.com");
                
                $email_from = $senderEmail;
                $name_from = $senderName;
                $email_to = "islam.askar@gmail.com";
                $name_to = "Admin";
 
                $mail = new Zend_Mail ();
                $mail->setFrom ($email_from, $name_from);
                $mail->addTo ($email_to, $name_to);
                $mail->setSubject ($contactCategoryInfo["cat_name"]." from ".$name_from);
                $mail->setBodyText ($message);
                $mail->send();
                $this->_flashMessenger->addMessage('Thank you, massage sent successfully. We will reply shortly');
                $this->redirect("/contact");
                
            }else{
                $this->view->errors = $contactForm->getMessages();
            }         
            
        }
        
        
    }

    
}









