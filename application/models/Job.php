<?php

class Application_Model_Job extends Zend_Db_Table_Abstract
{
    protected  $_name='jobs';

    public function listAll(){
        $select = $this->select("*")->setIntegrityCheck(FALSE)
            ->join("user", "jobs.createdBy=user.id",array("user.id as uid","firstName","lastName"));
        return  $this->fetchAll($select)->toArray();
    }

    public function getJobById($jobId){
        return $this->find($jobId)->toArray();
    }

    public function addJob($jobInfo){
        //Created by
        $auth = Zend_Auth::getInstance();
        $userInfo = $auth->getIdentity();

        $row = $this->createRow();
        $row->title = $jobInfo['title'];
        $row->description = $jobInfo['description'];
        $row->type = $jobInfo['type'];
        $row->contact = $jobInfo['contact'];
        $row->companyName = $jobInfo['companyName'];
        $row->companyUrl = $jobInfo['companyUrl'];
        $row->companyAddress = $jobInfo['companyAddress'];
        $row->createdBy = $userInfo->id;
        return $row->save();
    }
}