<?php

class Administration_Form_Role extends Zend_Form
{

    public function init()
    {
        
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $name = new Zend_Form_Element_Text("name");
        $name->setRequired();
        $name->addValidator(new Zend_Validate_Alnum);
        $name->addFilter(new Zend_Filter_StripTags);
        $name->setAttrib("class", "form-control");
        $name->setAttrib("placeholder", "Enter Role Name");
        $name->setAttrib("id", "roleName");
        $name->setDecorators(array('ViewHelper'));
        
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($id,$name,$submit));
    }


}

