<?php

class Administration_Model_Attendance extends Zend_Db_Table_Abstract
{
    protected $_name = "attendance";
    
    function listStudents($branchId,$trackId){
        $select = $this->select()
                    ->setIntegrityCheck(FALSE)
                    ->from(array('user'=>'user'),
                            array('id','firstName','lastName'))
                    ->join(array('iti'=>'itian'),
                        'iti.userId=user.id')
                    ->where("user.roleId = 5")
                    ->where("branchId = $branchId")
                    ->where("trackId = $trackId");

        return $this->fetchAll($select)->toArray();

    }

    function listRules(){
        return $this->fetchAll()->toArray();
    }
    
    function addAttendance($attendanceInfo) {
        $newDate = date("Y-m-d", strtotime($attendanceInfo['date']));
        $this->deleteSimilar($attendanceInfo['userId'],$newDate);
        $row = $this->createRow();
        $row->ruleId = $attendanceInfo['ruleId'];
        $row->userId = $attendanceInfo['userId'];
        
        $row->date = $newDate;
        $row->note = $attendanceInfo['note'];
        return $row->save();
    }
    
    function deleteSimilar($userId,$date) {
        return $this->delete(array("userId=$userId","date='$date'"));            
    }

    function calculateGrade() {

        $select = $this->select()
            ->setIntegrityCheck(FALSE)
            ->from(array('att'=>'attendance'),
                    array('att.userId','num'=>'count(*)'))
            ->join(array('rule'=>'attendanceRule'),
                    'rule.id=att.ruleId',
                    array('rule.type','rule.id','rule.maximum','rule.deduction'))
            ->group(array('rule.type','rule.id','att.userId'));

            return $this->fetchAll($select)->toArray();

        // select count(attendance.id),rule.id,rule.maximum,rule.deduction, rule.type, attendance.userId from attendance join attendanceRule as rule on rule.id=attendance.ruleId group by rule.type,rule.id, attendance.userId
    }

    


}

