<?php

class Administration_PageController extends Zend_Controller_Action {

    /**
     * FlashMessenger
     *
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;

    public function init() {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction() {
        $this->redirect("/administration/page/list");
    }

    public function listAction() {
        $pageModel = new Administration_Model_Page();
        $pages = $pageModel->listPages();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($pages);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
    }

    public function addAction() {
        
        $pageForm = new Administration_Form_Page();

        $this->view->form = $pageForm;
        $this->view->post = "/administration/page/add";
        $this->view->viewName = "Add Page";


        if ($this->getRequest()->isPost()) {
            if ($pageForm->isValid($this->getRequest()->getParams())) {
                $pageModel = new Administration_Model_Page();
                $pageId = $pageModel->addPage($pageForm->getValues());

                if ($pageId) {
                    $this->_flashMessenger->addMessage('Page added successfully!');
                    $this->redirect("/administration/page/list");
                }
            } else {

                $this->view->errors = $pageForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction() {
        //Get instances
        $pageForm = new Administration_Form_Page();
        $pageModel = new Administration_Model_Page();

        //get page id from request
        $id = $this->getRequest()->getParam('id');

        //get page information
        $page = $pageModel->getPageByID($id);


        //send values to view
        $this->view->post = "/administration/page/edit";
        $this->view->viewName = "Edit Page";

        if ($page) {
            $pageForm->populate($page[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no page with this id!');
            $this->redirect("/administration/page/list");
        }

        if ($this->getRequest()->isPost()) {
            if ($pageForm->isValid($this->getRequest()->getParams())) {

                $result = $pageModel->editPage($pageForm->getValue("id"), $pageForm->getValues());

                $this->_flashMessenger->addMessage('Page edited successfully!');
                $this->redirect("/administration/page/list");
            }
        }

        $this->view->form = $pageForm;
        $this->render("form");
    }

    public function deleteAction() {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");
        $pageModel = new Administration_Model_Page();
        $result = $pageModel->deletePage($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Page deleted successfully!');
            echo $result;
        }
    }

}
