<?php

class Administration_Form_Services extends Zend_Form {

    public function init() {
        $this->setMethod('post');
        $this->setName('service');
        $this->setEnctype('multipart/form-data');

        $id = new Zend_Form_Element_Hidden("id");
        $id->setValue("service");
        $id->setDecorators(array('ViewHelper'));
        $number = array('One', 'Two', 'Three');
        
        
        for($i = 0; $i <3; $i++) {
            ${"serviceTitle".$number[$i]} = new Zend_Form_Element_Text("serviceTitle".$number[$i]);
            ${"serviceTitle".$number[$i]}->addFilter(new Zend_Filter_StripTags);
            ${"serviceTitle".$number[$i]}->setAttrib("class", "form-control");
            ${"serviceTitle".$number[$i]}->setAttrib("placeholder", "Enter Title ".$number[$i]);
            ${"serviceTitle".$number[$i]}->setAttrib("id", "service".$number[$i]."Title");
            ${"serviceTitle".$number[$i]}->setRequired();
            ${"serviceTitle".$number[$i]}->removeDecorator('Label');
            ${"serviceTitle".$number[$i]}->setDecorators(array('ViewHelper'));

            ${"serviceBody".$number[$i]} = new Zend_Form_Element_Textarea("serviceBody".$number[$i]);
            ${"serviceBody".$number[$i]}->setRequired();
            ${"serviceBody".$number[$i]}->setAttrib("class", "form-control");
            ${"serviceBody".$number[$i]}->setAttrib("id", "ckeditor_full");
            ${"serviceBody".$number[$i]}->setDecorators(array('ViewHelper'));


            ${"serviceIamge".$number[$i]} = new Zend_Form_Element_File('serviceImage'.$number[$i]);
            ${"serviceIamge".$number[$i]}->setDestination(APPLICATION_PATH . "/../public/upload/services");
            ${"serviceIamge".$number[$i]}->addValidator('Count', false, 1);
            ${"serviceIamge".$number[$i]}->addValidator('Size', false, 2002400);
            ${"serviceIamge".$number[$i]}->addValidator('Extension', false, 'jpg,png,gif');
            ${"serviceIamge".$number[$i]}->setAttrib("class", "form-control");
            ${"serviceIamge".$number[$i]}->setAttrib("id", "serviceImage".$number[$i]);
            ${"serviceIamge".$number[$i]}->setRequired();
            ${"serviceIamge".$number[$i]}->removeDecorator('Label');
            ${"serviceIamge".$number[$i]}->removeDecorator('Error');

            ${"readMoreLink".$number[$i]} = new Zend_Form_Element_Text("readMoreLink".$number[$i]);
                ${"readMoreLink".$number[$i]}->setAttrib("class", "form-control")
                ->setAttrib("placeholder", "Enter read more link")
                ->setAttrib("id", "title".$number[$i])
                ->setRequired()
                ->removeDecorator('Label')
                ->setDecorators(array('ViewHelper'));
                
                $this->addElements(array(${"serviceTitle".$number[$i]},${"serviceBody".$number[$i]},${"serviceIamge".$number[$i]},${"readMoreLink".$number[$i]}));
        }
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
        $this->addElements(array($id,$submit));
    }

}
