<?php

class Administration_Form_Video extends Zend_Form
{

    public function init()
    {
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $videoTitle = new Zend_Form_Element_Text("videoTitle");
        $videoTitle->addFilter(new Zend_Filter_StripTags);
        $videoTitle->setRequired();
        $videoTitle->setAttrib("class", "form-control");
        $videoTitle->setAttrib("placeholder", "Enter Video Title");
        $videoTitle->setAttrib("id", "videoTitle");
        $videoTitle->removeDecorator('Label');
        $videoTitle->setDecorators(array('ViewHelper'));
        
        $videoUrl = new Zend_Form_Element_Text("videoUrl");
        $videoUrl->addFilter(new Zend_Filter_StripTags);
        $videoUrl->setRequired();
        $videoUrl->setAttrib("class", "form-control");
        $videoUrl->setAttrib("placeholder", "Enter Video Code");
        $videoUrl->setAttrib("id", "videoUrl");
        $videoUrl->removeDecorator('Label');
        $videoUrl->setDecorators(array('ViewHelper'));
        
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
       
       $this->addElements(array($id,$videoTitle,$videoUrl,$submit));
    }


}

