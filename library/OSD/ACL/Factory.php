<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Factory
 *
 * @author islam
 */
class OSD_ACL_Factory {
    private static $_sessionNameSpace = 'OSD_ACL_Namespace';
    private static $_objAuth;
    private static $_objAclSession;
    private static $_objAcl;
 
    public static function get(Zend_Auth $objAuth,$clearACL=false) {
 
        self::$_objAuth = $objAuth;
	self::$_objAclSession = new Zend_Session_Namespace(self::$_sessionNameSpace);
 
	if($clearACL) {self::_clear();}
 
	    if(isset(self::$_objAclSession->acl)) {
                
		return self::$_objAclSession->acl;
	    } else {
                
	        return self::_loadAclFromDB();
	    }
	}
 
    private static function _clear() {
        unset(self::$_objAclSession->acl);
    }
 
    private static function _saveAclToSession() {
        self::$_objAclSession->acl = self::$_objAcl;
    }
 
    private static function _loadAclFromDB() {
        Zend_Loader::loadFile(APPLICATION_PATH . '/modules/administration/models/Resource.php',NULL,TRUE);
        Zend_Loader::loadFile(APPLICATION_PATH . '/modules/administration/models/Role.php',NULL,TRUE);
        Zend_Loader::loadFile(APPLICATION_PATH . '/modules/administration/models/RoleResource.php',NULL,TRUE);
        
        $roleModel = new Administration_Model_Role();
        $resourceModel = new Administration_Model_Resource();
        $roleResourceModel = new Administration_Model_RoleResource();
        
        $arrRoles = $roleModel->listroles();
	$arrResources = $resourceModel->listAll();
	$arrRoleResources = $roleResourceModel->listAll();
        
        
	self::$_objAcl = new Zend_Acl();

	//self::$_objAcl->addRole(new Zend_Acl_Role($roleModel->getGeustRoleName()));
        
        // Create an array of core roles to check inherited roles against
//            foreach($arrRoles as $role) {
//             if($role['id'] != $roleModel->getAdminRoleId()) {
//               $arrCoreRoles[] = $role['role'];
//              }	
//            }
//        while(count($arrRoles) > 0) {
//            $role = array_shift($arrRoles);
//              if($role['id'] != $roleModel->getAdminRoleId()) {
//                if(isset($role['inherits'])) {
//                  $exists = true;
//                  $isCore = false;
//                  foreach($role['inherits'] as $index => $inherited) {
//                    if(in_array($inherited,$arrCoreRoles)) {
//                      $isCore = true;
//                    } else {
//                      unset($role['inherits'][$index]);
//                    }
//
//                    if(!self::$_objAcl->hasRole($inherited)) {
//                      $exists = false;
//                    }
//                  }
//
//                  if($exists && $isCore) {
//                    $role['inherits'][] = Entities_RoleTable::getGuestRoleName();
//                    self::$_objAcl->addRole(new Zend_Acl_Role($role['role']),$role['inherits']);
//                  } else {
//                    $arrRoles[] = $role;
//                  }
//                } else {
//                  self::$_objAcl->addRole(new Zend_Acl_Role($role['role']),array(Entities_RoleTable::getGuestRoleName()));
//                }
//              }
//            }
//
//            // add admin account and inherit all roles
//            self::$_objAcl->addRole(new Zend_Acl_Role(Entities_RoleTable::getAdminRoleName()),$arrCoreRoles);
//        
	foreach($arrRoles as $role) {
            
            self::$_objAcl->addRole(new Zend_Acl_Role($role['name']));
        }
 
        // add all resources to the acl
        foreach($arrResources as $resource) {
            self::$_objAcl->add(new Zend_Acl_Resource($resource['module'] .'::' .$resource['controller'] .'::' .$resource['action']));
        }
 
        // allow roles to resources
        
        foreach($arrRoleResources as $roleResource) {
            
            self::$_objAcl->allow($roleResource['name'],$roleResource['module'] .'::' .$roleResource['controller'] .'::' .$roleResource['action']);
        }
 
	self::_saveAclToSession();
	return self::$_objAcl;
    }
}