<?php

/* default/layout/hot_courses.tpl */
class __TwigTemplate_b6955868e2e017c50bf0d42055a487cb21c0c271cb9ce850b8c1360b738f5b4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((!(null === (isset($context["hot_courses"]) ? $context["hot_courses"] : null))) && (!twig_test_empty((isset($context["hot_courses"]) ? $context["hot_courses"] : null))))) {
            // line 2
            echo "
<script>
\$(document).ready( function() {
    \$('.star-rating li a').live('click', function(event) {
        var id = \$(this).parents('ul').attr('id');
        \$('#vote_label2_' + id).html(\"";
            // line 7
            echo get_lang("Loading");
            echo "\");
        \$.ajax({
            url: \$(this).attr('data-link'),
            success: function(data) {
                \$(\"#rating_wrapper_\"+id).html(data);
                if (data == 'added') {
                    //\$('#vote_label2_' + id).html(\"";
            // line 13
            echo get_lang("Saved");
            echo "\");
                }
                if (data == 'updated') {
                    //\$('#vote_label2_' + id).html(\"";
            // line 16
            echo get_lang("Saved");
            echo "\");
                }
            }
        });
    });

});
</script>

    <section id=\"hot_courses\">
        <div class=\"row\">
            <div class=\"span9\">
                ";
            // line 28
            if ($this->getAttribute((isset($context["_u"]) ? $context["_u"] : null), "is_admin", array())) {
                // line 29
                echo "                <span class=\"pull-right\">
                    <a title=\"";
                // line 30
                echo get_lang("Hide");
                echo "\" alt=\"";
                echo get_lang("Hide");
                echo "\" href=\"";
                echo $this->getAttribute((isset($context["_p"]) ? $context["_p"] : null), "web_main", array());
                echo "admin/settings.php?search_field=show_hot_courses&submit_button=&_qf__search_settings=&category=search_setting\">
                        <img src=\"";
                // line 31
                echo Template::get_icon_path("visible.png", 32);
                echo "\">
                    </a>
                </span>
                ";
            }
            // line 35
            echo "                ";
            echo Display::page_subheader_and_translate("HottestCourses");
            echo "
            </div>            
                
            ";
            // line 38
            $this->env->loadTemplate("default/layout/hot_course_item.tpl")->display($context);
            // line 39
            echo "        </div>
    </section>
";
        }
    }

    public function getTemplateName()
    {
        return "default/layout/hot_courses.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 39,  85 => 38,  78 => 35,  71 => 31,  63 => 30,  60 => 29,  58 => 28,  43 => 16,  37 => 13,  28 => 7,  21 => 2,  19 => 1,);
    }
}
