<?php

class Administration_ApplicantController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/applicant/list");
    }
    
    public function listAction()
    {
        $applicantModel = new Administration_Model_Applicant();
        $applicants = $applicantModel->listApplicants();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($applicants);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
        
    }
    
    public function addAction()
    {
        $applicantForm = new Administration_Form_Applicant();


        $this->view->form = $applicantForm;
        $this->view->post = "/administration/applicant/add";
        $this->view->viewName = "Add Applicant";


        if ($this->getRequest()->isPost()) {
            if ($applicantForm->isValid($this->getRequest()->getParams())) {
                $applicantModel = new Administration_Model_Applicant();

                $applicantId = $applicantModel->addApplicant($applicantForm->getValues());

                if ($applicantId) {
                    $applicantNamespace = new Zend_Session_Namespace('Applicant');
                    $applicantNamespace->id = $applicantId;
                    $applicantNamespace->name = $applicantForm->getValue("name");
                    $applicantNamespace->code = $applicantForm->getValue("code");
                    $this->redirect("/administration/admissioninterview/start");
                }
            } else {
                $this->view->errors = $applicantForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction()
    {
         //Get instances
        $applicantForm = new Administration_Form_Applicant();
        $applicantModel = new Administration_Model_Applicant();


        //get applicant id from request
        $id = $this->getRequest()->getParam('id');

        //get applicant information
        $applicant = $applicantModel->getApplicantById($id);
       
        //send values to view
        $this->view->post = "/administration/applicant/edit";
        $this->view->viewName = "Edit Applicant";
        
        
        if ($this->getRequest()->isPost()) {
            if ($applicantForm->isValid($this->getRequest()->getParams())) {

                $applicantModel->editApplicant($applicantForm->getValue("id"), $applicantForm->getValues());

                $this->_flashMessenger->addMessage('Applicant edited successfully!');
                $this->redirect("/administration/applicant/list");
            } else {
                $this->view->errors = $applicantForm->getMessages();
            }
        }

        if ($applicant) {
            $applicantInfo = ['name' => $applicant[0]['applicantName'],'code' => $applicant[0]['applicantCode']];
            $applicantForm->populate($applicantInfo);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no applicant with this id!');
            $this->redirect("/administration/applicant/list");
        }

        

        $this->view->form = $applicantForm;
        $this->render("form");
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $applicantModel = new Administration_Model_Applicant();
        $result = $applicantModel->deleteApplicant($id);
        if ($result) {
            $this->_flashMessenger->addMessage('applicant deleted successfully!');
            echo $result;
        }
    }


}

