<?php

class Administration_Model_Coursematerial extends Zend_Db_Table_Abstract
{
    protected  $_name='coursematerial';
    /*function listAll(){
         $select  = $this->_db->select()->from($this->_name,array('key' => 'category_id','value' => 'category_name'));
         $result = $this->getAdapter()->fetchAll($select);
         return $result;
    }*/
    function addCourseMaterial($course,$filename,$materialtype,$materialname){
        $row = $this->createRow();
        $row->materialtype_id =$materialtype;
        $row->coursematerial_name=$materialname;
        $row->course_id=$course;
        $row->materialfilepath=$filename;
        $row->save();   
    }
    function deleteCourseMaterial($coursematerial_id){
          $coursematerialdata=$this->getCourseMaterialById($coursematerial_id);
          unlink(md5($coursematerialdata[0]['materialfilepath']));
          $this->delete("coursematerial_id=$coursematerial_id");
    }
     function getCourseMaterialById($coursematerialid){
        $sql=$this->select()->where("coursematerial_id=$coursematerialid");
        return $this->fetchAll($sql)->toArray();
        
    }
    function listCourseMaterial(){
        $select = $this->_db->select()
                ->from('coursematerial',array('coursematerial_id','coursematerial_name'))
                ->joinInner('course','coursematerial.course_id=course.course_id',array('course_name'))
                ->joinInner('materialtype','coursematerial.materialtype_id=materialtype.materialtype_id',array('materialtype_name'));
        $result = $this->getAdapter()->fetchAll($select);
        return $result;
    }
  
}
