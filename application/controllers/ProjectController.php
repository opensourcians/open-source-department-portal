<?php

class ProjectController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function viewprojectAction()
    {
        // action body
        $id=$this->_request->getParam("id");
        $this->view->list1=$id;
        $list = new Application_Model_Project();
        $this->view->list2=$list->listProject();
        $this->view->list3=$list->listteam();
    }

    public function viewAction()
    {
        // action body
        $list = new Application_Model_Project();
        $this->view->list1=$list->listProject();
        $this->view->list2=$list->listtag(); 
    }

    public function listprojectAction()
    {
        // action body
        $track=$this->_request->getParam("track");
        $intake=$this->_request->getParam("intake");
        $this->view->list1=$intake;
        $this->view->list2=$track;
        $list = new Application_Model_Project();
        $this->view->list3=$list->listProject();
        $this->view->list4=$list->listtag();
    }


}











