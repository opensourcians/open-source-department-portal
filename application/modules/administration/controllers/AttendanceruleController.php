<?php

class Administration_AttendanceruleController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/rules/list");
    }
    
    public function listAction()
    {
        $ruleModel = new Administration_Model_AttendanceRule();
        $rules = $ruleModel->listRules();
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($rules);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
        
    }
    public function editAction()
    {
         //Get instances
        $ruleForm = new Administration_Form_AttendanceRule();
        $ruleModel = new Administration_Model_AttendanceRule();


        //get rule id from request
        $id = $this->getRequest()->getParam('id');

        //get rule information
        $rule = $ruleModel->getRuleById($id);
       
        //send values to view
        $this->view->post = "/administration/attendancerule/edit";
        $this->view->viewName = "Edit Attendance Rule";
        
        
        if ($this->getRequest()->isPost()) {
            if ($ruleForm->isValid($this->getRequest()->getParams())) {

                $ruleModel->editRule($ruleForm->getValue("id"), $ruleForm->getValues());

                $this->_flashMessenger->addMessage('Attendance Rule edited successfully!');
                $this->redirect("/administration/attendancerule/list");
            } else {
                $this->view->errors = $ruleForm->getMessages();
            }
        }

        if ($rule) {
            $ruleInfo = ['id'=>$rule[0]['id'],'type' => $rule[0]['type'],'deduction'=>$rule[0]['deduction'],'maximum' => $rule[0]['maximum']];
            $ruleForm->populate($ruleInfo);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no rule with this id!');
            $this->redirect("/administration/attendancerule/list");
        }

        

        $this->view->form = $ruleForm;
        $this->render("form");
    }
    public function addAction()
    {
        $ruleForm = new Administration_Form_AttendanceRule();

        $this->view->form = $ruleForm;
        $this->view->post = "/administration/attendancerule/add";
        $this->view->viewName = "Add Attendance Rule";

        if ($this->getRequest()->isPost()) {
            if ($ruleForm->isValid($this->getRequest()->getParams())) {
                $ruleModel = new Administration_Model_AttendanceRule();

                $ruleId = $ruleModel->addRule($ruleForm->getValues());

                if ($ruleId) {
                    $this->_flashMessenger->addMessage('Rule added successfully!');
                    $this->redirect("/administration/attendancerule/list");
                }
            } else {
                $this->view->errors = $ruleForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $ruleModel = new Administration_Model_AttendanceRule();
        $result = $ruleModel->deleteRule($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Attendance Rule deleted successfully!');
            echo $result;
        }
    }
}

