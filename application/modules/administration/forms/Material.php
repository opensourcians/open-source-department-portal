<?php

class Administration_Form_Material extends Zend_Form
{

    public function init()
    {
        $this->setName('material_upload');
        $this->setAttrib("enctype","multipart/form-data");
        $categorylist=new Administration_Model_Categorycourse();
        $category_list=$categorylist->listAll();
        $courselist = new Administration_Model_Course();
        $course_list =$courselist->listAll();
        $materialtypelist = new Administration_Model_Materialtype();
        $materialtype_list = $materialtypelist->listAll();
        //select for course category
        /*$category = new Zend_Form_Element_Select("category");
        $category->setLabel("Select Category:");
        $category->setRequired(TRUE);
        $category->setAttrib("class", "form-control input-large");
        $category->setAttrib("id", "category");
        $category->addMultiOptions($category_list);*/
        //course selection
        $course = new Zend_Form_Element_Select("course");
        $course->setLabel("Select Course:");
        $course->setRequired(TRUE);
        $course->setAttrib("class", "form-control input-large");
        $course->setAttrib("id", "course");
        $course->addMultiOptions($course_list);
        // material type selection
        $materialtype= new Zend_Form_Element_Select("materialtype");
        $materialtype->setRequired();
        $materialtype->setAttrib("class", "form-control input-large");
        $materialtype->setAttrib("id", "material");
        $materialtype->addMultiOptions($materialtype_list);
        //text field for material name
        $materialname = new Zend_Form_Element_Text("materialname");
        $materialname->setRequired(TRUE);
        $materialname->setLabel("Material Title:");
        $materialname->addValidator(new Zend_Validate_Alnum);
        $materialname->addFilter(new Zend_Filter_StringTrim);
        $materialname->addFilter(new Zend_Filter_StripTags);
        $materialname->addValidator(new Zend_Validate_Alnum);
        $materialname->setAttrib("class", "form-control input-large");
        $materialname->setAttrib("id", "materialname");
        //file field for choose file
        $materialfile=new Zend_Form_Element_File("materialfile");
        $materialfile->setRequired(true)->addValidator('Count', false, 1)
        				 ->addValidator('Size', false, 2097152)
        				 ->setMaxFileSize(2097152)
        				 ->addValidator('Extension', false, 'pdf');
        $materialfile->setLabel("Choose File:");
        $materialfile->addValidator("NotEmpty");
        $materialfile->setAttrib("class", "form-control input-large");
        $materialfile->setAttrib("id", "materialfile");
        
        $submit = new Zend_Form_Element_Submit("Upload");
        $submit->setAttrib("class","btn btn-primary");
        
        $this->addElements(array($course,$materialtype,$materialname,$materialfile,$submit));
        /* Form Elements & Other Definitions Here ... */
    }


}

