<?php

class Application_Form_Job extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $title = new Zend_Form_Element_Text("title");
        $title->addFilter(new Zend_Filter_StripTags);
        $title->setAttrib("class", "form-control");
        $title->setAttrib("placeholder", "Enter Job Title");
        $title->setAttrib("id", "title");
        $title->setRequired();
        $title->setDecorators(array('ViewHelper'));

        $contact = new Zend_Form_Element_Text("contact");
        $contact->addFilter(new Zend_Filter_StripTags);
        $contact->addValidator(new Zend_Validate_EmailAddress());
        $contact->setAttrib("class", "form-control");
        $contact->setAttrib("placeholder", "email@company.com");
        $contact->setAttrib("id", "email");
        $contact->setRequired();
        $contact->setDecorators(array('ViewHelper'));

        $description = new Zend_Form_Element_Textarea("description");
        $description->setRequired();
        $description->setAttrib("class", "form-control");
        $description->setAttrib("id", "ckeditor_full");
        $description->removeDecorator('Label');
        $description->setDecorators(array('ViewHelper'));


        $type = new Zend_Form_Element_Select('type');
        $type->setMultiOptions(array(
                "f" => "Full Time",
                "p" => "Part Time")
        );
        $type->setAttrib("class", "form-control");
        $type->removeDecorator('Label');
        $type->setAttrib("id", "type");
        $type->setDecorators(array('ViewHelper'));

        $salary = new Zend_Form_Element_Text('salary');
        $salary ->removeDecorator('Label')
                ->setAttrib("class", "form-control")
                ->addValidator(new Zend_Validate_Digits())
                ->setAttrib('placeholder', 'Digits only')
                ->setDecorators(array('ViewHelper'));

        $companyName = new Zend_Form_Element_Text('companyName');
        $companyName ->removeDecorator('Label')
                     ->setAttrib('placeholder', 'Enter your company name')
                     ->setAttrib("class", "form-control")
                     ->setDecorators(array('ViewHelper'));

        $companyUrl = new Zend_Form_Element_Text('companyUrl');
        $companyUrl ->removeDecorator('Label')
                    ->setAttrib("class", "form-control")
                    ->setAttrib('placeholder', 'http://www.your-company.com')
                    ->setDecorators(array('ViewHelper'))
                    ->addValidator(new Zend_Validate_Hostname());

        $companyAddress = new Zend_Form_Element_Text('companyAddress');
        $companyAddress ->removeDecorator('Label')
                        ->setAttrib('placeholder', 'Enter your company address')
                        ->setAttrib("class", "form-control")
                        ->setDecorators(array('ViewHelper'));

        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");

        $elements = [$title, $description, $contact, $submit, $type, $salary,
            $companyName, $companyUrl, $companyAddress];

        $this->addElements($elements);
    }
}