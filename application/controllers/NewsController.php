<?php

class NewsController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $this->redirect("/news/list");
    }

    public function listAction() {
        $newsModel = new Administration_Model_News();
        $categoryId = $this->getRequest()->getParam('categoryId');
        if (isset($categoryId)) {
            $allArticles = $newsModel->listImages($categoryId);
            $this->view->sliderArticles = array_slice($allArticles, 1,5);   
        } else {
            $this->view->sliderArticles = $newsModel->getArticlesForDisplay(5);
            $allArticles = $newsModel->listImages();       
        }
        //remove first five news articles
        if (count($allArticles)>5){
        $articles = array_slice($allArticles, 5);
        } else {
            $articles = $allArticles;
        }
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($articles);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        $this->view->paginator = $paginator;
    }

    public function categoryAction() {
        $newsModel = new Administration_Model_News();
        //get page id from request
        $id = $this->getRequest()->getParam('categoryId');

        $this->articles = $newsModel->listImages($id);
    }

    public function viewAction() {
        //Get instances
        $newsModel = new Administration_Model_News();

        //get page id from request
        $id = $this->getRequest()->getParam('id');

        //get page information
        $article = $newsModel->getArticleForDisplay($id);
        if ($article) {
            $this->view->article = $article;
        } else {
            $this->redirect("/");
        }
    }

}
