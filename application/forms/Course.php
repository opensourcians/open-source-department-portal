<?php

class Application_Form_Course extends Zend_Form
{

    public function init()
    {
        $categories_model =new  Administration_Model_Categorycourse();
        $categories=$categories_model->listCourseCategory();
        $selectCategory = $this->createElement('select','category');
        $selectCategory->setLabel('Category:');
        $selectCategory->setAttrib("id","category_id");
        $selectCategory->setAttrib("class","form-control");
        foreach ($categories as $category) {
            
            $selectCategory->addMultiOption($category['coursecategory_id'],$category['coursecategory_name']);
        }
        $selectCategory->setRequired(TRUE);
        $selectCategory->setAttrib('size',1);
        $this->addElement($selectCategory);
        //course select dropdown menu
        $courses_model = new Application_Model_Course();
        $courses=$courses_model->getCourses();
        $selectcourse = $this->createElement('select','course');
        $selectcourse->setLabel("Select Course:");
        $selectcourse->setRequired()->setName('course_name');
        $selectcourse->setAttrib("class","form-control input-large");
        $selectcourse->setAttrib("id", "course_id");
     //   $selectcourse->addMultiOption('name','course_name');
 
        
                $courseModel=new Application_Model_Course(); 
            	$data = $courseModel->getCoursesByCategory(3);                
                    //print_r($data);
                //this->view->data = Zend_Json::encode($data);
              
         foreach ($data as $course) { 
            $selectcourse->addMultiOption($course['course_id'],$course['course_name']);
        }
         //$selectcourse->a($data->course_id,$data->course_name);
        $selectcourse->setRequired(TRUE);
        $selectcourse->setAttrib('size',1);
        $this->addElement($selectcourse);
    }

}

