<?php

class Application_Model_Graduate extends Zend_Db_Table_Abstract
{
    protected $_name = "graduate";
    
    function updateGraduate($data){
        $graduate = array("job" => $data['job'] , "track_id" => $data['track_id'] , "intake" => $data['intake']);
        $this->update($graduate, "user_id = {$data['id']} ");
        
        $userModel = new Application_Model_User();
        unset($data['job']); unset($data['track_id']); unset($data['intake']);
        $userModel->updateUser($data);
    }
    
    function getGraduate($username){
        $select = $this->select()->setIntegrityCheck(false)->from("graduate")->joinInner("user","`graduate`.`user_id`=`user`.`id`")->where("email='$username'")->join("track","graduate.track_id=track.id");
        
        $result = $this->fetchRow($select);
        if($result == null){
            return array();
        }
        
        $phoneModel = new Application_Model_Userphone();
        $resultFinal = $result->toArray();   
        $resultFinal['id'] = $resultFinal['user_id'];
        $resultFinal['phones'] = $phoneModel->getPhones($resultFinal['user_id']);
        return $resultFinal;       
    }
        
    function getGraduates($baseUrl,$track="",$intake=0,$name="",$offset=0){
        $select = $this->select()->setIntegrityCheck(false)
                                 ->from("graduate",array("graduate.job","graduate.intake","graduate.user_id"))
                                 ->join("user","user_id=id",array(
                                                                    'name' => new Zend_Db_Expr("CONCAT(user.firstName, ' ', user.lastName)"),
                                                                    'img' =>  new Zend_Db_Expr("IFNULL(CONCAT('$baseUrl/upload/profile/',user.image),CONCAT('$baseUrl/images/icons/users/',user.gender,'.png'))"),
                                                                     'url' =>  new Zend_Db_Expr("CONCAT('$baseUrl/graduate/profile/email/',user.email)"),
                                                                     'email'
                                                                 )
                                       )->join("track","graduate.track_id=track.id",array("track.track"));
        
        $cond = "";
        $opr = "";
        if (!empty($track)) {
            $cond .= "graduate.track_id = '$track'";
            $opr = " AND ";
        }
        
        if(!empty($intake)){
            $cond .= $opr."graduate.intake = '$intake'";
            $opr = " AND ";
        }
        
        if(!empty($name)){
            $cond .= $opr."( CONCAT(user.firstName, ' ', user.lastName) LIKE '%$name%' )";
            $opr = " AND ";
        }
        
        if(empty($cond)){
            $cond = "1";
        }
        
        $select->where($cond);
        
        if(!empty($offset)){
            $select->limit(10,$offset*10);
        }else{
            $select->limit(10);
        }
        
        $resultObj = $this->fetchAll($select);
        if($resultObj == null){
            return array();
        }
        
        $result = $resultObj->toArray(); 
        $phoneModel = new Application_Model_Userphone();
        for( $i = 0 ; $i < count($result) ; $i++ ){
            $phones = $phoneModel->getPhones($result[$i]['user_id'],1,0);
            if(!empty($phones)){
                $result[$i]['mobile'] = $phones[0]['phone'];
            }
        }        
                
        return $result;
    }
    
    function countPages($track="",$intake=0,$name=""){
        
        $cond = "";
        $opr = "";
        if (!empty($track)) {
            $cond .= "graduate.track_id = '$track'";
            $opr = " AND ";
        }
        
        if(!empty($intake)){
            $cond .= $opr."graduate.intake = '$intake'";
            $opr = " AND ";
        }
        
        if(!empty($name)){
            $cond .= $opr."( CONCAT(user.first_name, ' ', user.last_name) LIKE '%$name%' )";
            $opr = " AND ";
        }
        
        if(empty($cond)){
            $cond = "1";
        }
        
        $select = $this->select()->setIntegrityCheck(false)
                                 ->from("graduate",array("user_id"))
                                 ->join("user","user_id=id",array())->where($cond);
        $rowCount = count ($this->fetchAll($select));

        return ceil($rowCount/10);
    }

}

