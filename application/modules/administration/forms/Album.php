<?php

class Administration_Form_Album extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $title = new Zend_Form_Element_Text("title");
        $title->addFilter(new Zend_Filter_StripTags);
        $title->setAttrib("class", "form-control");
        $title->setAttrib("placeholder", "Enter Title");
        $title->setAttrib("id", "title");
        $title->setRequired();
        $title->removeDecorator('Label');
        $title->setDecorators(array('ViewHelper'));
        
        $system = new Zend_Form_Element_Checkbox("system");
        $system->setAttrib("id", "system");
        $system->removeDecorator('Label');
        $system->setDecorators(array('ViewHelper'));
        
        $date = new Zend_Form_Element_Text("date");
        $date->setAttrib("class", "form-control");
        //$fromDate->setAttrib("disabled", "disabled");
        
        $date->removeDecorator('Label');
        $date->setDecorators(array('ViewHelper'));
        
        $place = new Zend_Form_Element_Text("place");
        $place->addFilter(new Zend_Filter_StripTags);
        $place->setAttrib("class", "form-control");
        $place->setAttrib("placeholder", "Place");
        $place->setAttrib("id", "place");
        
        $place->removeDecorator('Label');
        $place->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($id,$title,$system,$date,$place,$submit));
    }


}

