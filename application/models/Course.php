<?php

class Application_Model_Course extends Zend_Db_Table_Abstract
{
    protected $_name='course';
    function getCourses(){
        return $this->fetchAll()->toArray();
    }
    function getCoursesByCategory($categoryId){
        $sql=$this->select()->where("category_id=$categoryId");
        return $this->fetchAll($sql)->toArray();
        
    }
    


}

