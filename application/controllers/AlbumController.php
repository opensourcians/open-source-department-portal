<?php

class AlbumController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->redirect("/album/list");
    }
    
    public function listAction()
    {
        $albumModel = new Administration_Model_Album();
        $albums = $albumModel->getAlbumsForDisplay(true);
        $this->view->albums = $albums;
        //var_dump($albums); exit;
    }
    
    public function viewAction() {
        //Get instances
        $albumModel = new Administration_Model_Album();

        //get page id from request
        $id = $this->getRequest()->getParam('id');

        //get page information
        $album = $albumModel->getAlbumForDisplay($id);
        if ($album) {
            $this->view->album = $album;
        } else {
            $this->redirect("/");
        }
    }


}

