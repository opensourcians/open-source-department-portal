<?php

class Administration_MaterialController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function addAction()
    {
         $materialtype=new Administration_Form_Materialtype();
        $this->view->materialtype=$materialtype;
        if($this->_request->isPost()){
            $materialtype_name=$this->_request->getParam('materialtype');
            $materialtype_model = new Administration_Model_Materialtype();
            $materialtype_model->addMaterialType($materialtype_name);
            $this->redirect('/administration/course/index');
        }
    }

    public function listAction()
    {
        $materialtype_model = new Administration_Model_Materialtype();
         $materialtypedata = $materialtype_model->listMaterialType();
         $this->view->materialtypedata=  $materialtypedata;
    }

    public function editAction()
    {
        $materialtype_model=new Administration_Model_Materialtype();
        $materialtypeid=$this->getRequest()->getParam('id');
        if($this->_request->isPost()){
            $materialtypename=$this->getRequest()->getParam('materialtype');
            $materialtype_model->editMaterialType($materialtypeid,$materialtypename);
            $this->view->msg="Course Edited Sucessfully";
            $this->redirect('/administration/material/list');
        }
        $materialtype=new Administration_Form_Materialtype();
        $this->view->materialtype=$materialtype;
        $materialtypedata=$materialtype_model->getMaterialTypeById($materialtypeid);
        $materialtype->getElement("materialtype")->setValue($materialtypedata[0]['materialtype_name']);
        $materialtype->getElement("submit")->setValue("Edit Material Type");
        
    }

    public function deleteAction()
    {
         $id=$this->getRequest()->getParam('id');
         $materialtype_model=new Administration_Model_Materialtype();
         $materialtype_model->deleteMaterialType($id);
         $this->redirect('/administration/material/list');
    }


}









