<?php

class Administration_AttendanceController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {

        $attendanceForm = new Administration_Form_Attendance();

        $this->view->form = $attendanceForm;
        $this->view->post = "/administration/attendance/index";
        $this->view->viewName = "Attendance";
        $grades=array();
        $rules_times=array();
        if ($this->getRequest()->isPost()) {
            if ($attendanceForm->isValid($this->getRequest()->getParams()))
            {
                $attendanceModel = new Administration_Model_Attendance();
                $userModel = new Administration_Model_User();
                $branchId = $attendanceForm->getValue('branchId');

                $users = $userModel->listAll();
                foreach ($users as $user) {
                    $grades[$user['id']]=0;

                }

                $trackId = $attendanceForm->getValue('trackId');
                $students = $attendanceModel->listStudents($branchId,$trackId);
                $ruleModel = new Administration_Model_AttendanceRule();
                $rules = $ruleModel->listRules();
                $data = $attendanceModel->calculateGrade();

                foreach ($users as $user) {
                    $rules_times[$user['id']]=array();
                    foreach ($rules as $rule) {
                        $rules_times[$user['id']][$rule['id']]=0;
                    }
                }
                
                foreach ($data as  $value) {
                    $rules_times[$value['userId']][$value['id']]=$value['num'];
                    if($value['num']>$value['maximum']) {
                        $grades[$value['userId']] +=($value['num']-$value['maximum'])*$value['deduction'];

                        

                    }  else {
                        $grades[$value['userId']]=0;
                    }  
                }
                
                
                $this->view->rules_times=$rules_times;
                $this->view->students=$students;
                $this->view->rules=$rules;
                $this->view->grades=$grades;
            } else {
                $this->view->errors = $attendanceForm->getMessages();
            }
        }
    }
    
    public function insertAction()
    {
       //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        if ($this->getRequest()->isXmlHttpRequest()) {
            if ($this->getRequest()->isPost()) {
                $attendanceModel = new Administration_Model_Attendance();
                $attendanceModel->addAttendance($this->getRequest()->getParams());
            }
        } else {
            $attendanceModel = new Administration_Model_Attendance();
            $data = $attendanceModel->calculateGrade();
            var_dump($data);
            die();
        }
    }
}

