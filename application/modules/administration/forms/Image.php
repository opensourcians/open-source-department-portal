<?php

class Administration_Form_Image extends Zend_Form
{

    public function init()
    {
        $this->setEnctype('multipart/form-data');
        
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $albumId = new Zend_Form_Element_Select("albumId");
        $albumId->setRequired();
        $albumId->addMultiOption("","Select Album");
        $albumId->setAttrib("class", "form-control");
        $albumId->setDecorators(array('ViewHelper'));
        $albumModel = new Administration_Model_Album();
        $albums = $albumModel->listAlbums();
        foreach($albums as $album){
            $albumId->addMultiOption($album['id'],$album['title']);
        }
        $album = new Zend_Form_Element_Hidden("album");
        $album->setDecorators(array('ViewHelper'));
        
        $imageName = new Zend_Form_Element_Hidden("imageName");
        $imageName->setDecorators(array('ViewHelper'));
        
        $albumCover = new Zend_Form_Element_Checkbox("albumCover");
        $albumCover->setAttrib("id", "system");
        $albumCover->removeDecorator('Label');
        $albumCover->setDecorators(array('ViewHelper'));
        
        $title = new Zend_Form_Element_Text("title");
        $title->addFilter(new Zend_Filter_StripTags);
        $title->setAttrib("class", "form-control");
        $title->setAttrib("placeholder", "Enter Title");
        $title->setAttrib("id", "title");
        
        $title->removeDecorator('Label');
        $title->setDecorators(array('ViewHelper'));

        $description = new Zend_Form_Element_Textarea("description");
        
        $description->setAttrib("class", "form-control");
        $description->setAttrib("id", "ckeditor_full");
        $description->setDecorators(array('ViewHelper'));
        
        $url = new Zend_Form_Element_Text("url");
        $url->addFilter(new Zend_Filter_StripTags);
        
        $url->setAttrib("class", "form-control");
        $url->setAttrib("placeholder", "Enter URL");
        $url->setAttrib("id", "url");
        $url->removeDecorator('Label');
        $url->setDecorators(array('ViewHelper'));

        $image = new Zend_Form_Element_File('name');
        $image->setDestination(APPLICATION_PATH . "/../public/upload/albums/");
        $image->addValidator('Count', false, 1);
        $image->addValidator('Size', false, 2002400);
        $image->addValidator('Extension', false, 'jpg,png,gif');
        
        $image->setAttrib("class", "form-control");
        $image->setAttrib("id", "name");
        $image->setRequired();
        $image->removeDecorator('Label');
        $image->removeDecorator('Error');

        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");

        $this->addElements(array($id,$albumId,$album,$imageName,$albumCover,$title, $description, $url, $image, $submit));
    }


}

