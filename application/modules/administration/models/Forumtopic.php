<?php

class Administration_Model_Forumtopic extends Zend_Db_Table_Abstract
{
    protected $_name="Topic";
     public function addTopic($Subject,$body,$section_id,$user_id) {
        $inserted = array('subject'=>$Subject,'body'=>$body
                ,'section_id'=>$section_id,'user_id'=>$user_id);
        $this->insert($inserted);
    }
    public function editTopic($Subject,$body,$section_id,$user_id,$topic_id) {
        $edited = array('subject'=>$Subject,'body'=>$body
                ,'section_id'=>$section_id,'user_id'=>$user_id);
        $this->update($edited, "topic_id = $topic_id");
    }
    public function deleteTopic($topic_id) {
        $this->delete("topic_id=$topic_id");
    }
    public function listForumtopics($id) {
        $selected = $this->select()->where("section_id=$id");
        $selected = $this->fetchAll($selected)->toArray();
        $rep_model = new Administration_Model_Forumreply();
        for($i=0;$i<count($selected);$i++)
        {
            $selected[$i]['reply'] = $rep_model->listForumreplyNo($selected[$i]['id']);
        }
        return $selected;
    }
    
    public function viewTopic($id) {
        $selected = $this->select()
                         ->setIntegrityCheck(false)
                         ->from(array('t' => 'Topic'))
                         ->where("t.id=$id")
                         ->join(array('s' => 'Section'),'s.id = t.id')
                         ->join(array('u' => 'user'),'user_id = u.id');
        return $this->fetchAll($selected)->toArray();
    }
    
    public function listForumTopicsNo($id) {
        $selected = $this->select()
                         ->setIntegrityCheck(FALSE)
                         ->from('Topic', 'count(*)')
                         ->where("section_id=$id");
        return $this->fetchAll($selected)->toArray();
    }
    public function latestTopic($id) {
        $selected = $this->select()
                         ->setIntegrityCheck(FALSE)
                         ->from(array('t' => 'Topic'))
                         ->where("section_id=$id")
                         ->order(array('t.topic_id DESC'))
                         ->limit(1);
        return $this->fetchAll($selected)->toArray();
    }
}