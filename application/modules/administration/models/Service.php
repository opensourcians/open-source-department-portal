<?php

class Administration_Model_Service extends Zend_Db_Table_Abstract
{
    protected $_name = 'service';
    
    function addSerive($serviceNumber,$serviceTitle,$serviceBody,$serviceImage,$readMore){
        
        $row = $this->createRow();
        $row->serviceNumber = $serviceNumber;
        $row->serviceTitle = $serviceTitle;
        $row->serviceBody = $serviceBody;
        $row->serviceImage = $serviceImage;
        $row->readMore = $readMore;
        return $row->save();
    }

}

