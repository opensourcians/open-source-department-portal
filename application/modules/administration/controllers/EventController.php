<?php

class Administration_EventController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/event/list");
      
    }

    public function listAction()
    {
        $eventModel = new Administration_Model_Event();
        $events = $eventModel->listEvents();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($events);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
    }

    public function addAction()
    {
        $eventForm = new Administration_Form_Event();


        $this->view->form = $eventForm;
        $this->view->post = "/administration/event/add";
        $this->view->viewName = "Add Event";


        if ($this->getRequest()->isPost()) {
            if ($eventForm->isValid($this->getRequest()->getParams())) {
                $eventModel = new Administration_Model_Event();

                $eventId = $eventModel->addEvent($eventForm->getValues());

                if ($eventId) {
                    $this->_flashMessenger->addMessage('Event added successfully!');
                    $this->redirect("/administration/event/list");
                }
            } else {
                $this->view->errors = $eventForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction()
    {
         //Get instances
        $eventForm = new Administration_Form_Event();
        $eventModel = new Administration_Model_Event();


        //get user id from request
        $id = $this->getRequest()->getParam('id');

        //get user information
        $event = $eventModel->getEventById($id);

        //send values to view
        $this->view->post = "/administration/event/edit";
        $this->view->viewName = "Edit Event";
        
        
        if ($this->getRequest()->isPost()) {
            if ($eventForm->isValid($this->getRequest()->getParams())) {

                $eventModel->editEvent($eventForm->getValue("id"), $eventForm->getValues());

                $this->_flashMessenger->addMessage('Event edited successfully!');
                $this->redirect("/administration/event/list");
            } else {
                $this->view->errors = $eventForm->getMessages();
            }
        }

        if ($event) {
            $eventForm->populate($event[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no news category with this id!');
            $this->redirect("/administration/event/list");
        }

        

        $this->view->form = $eventForm;
        $this->render("form");
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //check for ajax reuest 
        if ($this->getRequest()->isXmlHttpRequest()) {
            //get item id from ajax request
            $id = $this->getRequest()->getParam("id");

            $eventModel = new Administration_Model_Event();
            $result = $eventModel->deleteEvent($id);
            if ($result) {
                $this->_flashMessenger->addMessage('Event deleted successfully!');
                echo $result;
            }
        }
    }
    
    

}