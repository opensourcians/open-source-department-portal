<?php

class Administration_Model_AdmissionQuestion extends Zend_Db_Table_Abstract
{
    protected $_name = "admissionQuestion";
    
     function listAdmissionQuestions($categoryId = 0, $levelId = 0){
        $select = $this->select()->setIntegrityCheck(FALSE)->from("admissionQuestion", array("*", "admissionQuestion.id as aqId"))->joinLeft("admissionCategory", "admissionQuestion.categoryId=admissionCategory.id", array("*", "admissionCategory.name as acName"))
                ->joinLeft("admissionQuestionLevel", "admissionQuestion.levelId=admissionQuestionLevel.id");
        if($categoryId){
            $select->where("categoryId = $categoryId");
        }
        if($levelId){
            $select->where("levelId = $levelId");
        }
        return $this->fetchAll($select)->toArray();
    }
    
    function addAdmissionQuestion($questionInfo) { 
        
        $row = $this->createRow();
        $row->categoryId = $questionInfo['categoryId'];
        $row->title = $questionInfo['title'];
        $row->body = $questionInfo['body'];
        $row->levelId = $questionInfo['levelId'];
        
        return $row->save();
    }
    
    function getAdmissionQuestionById($questionId) {
        
        return $this->find($questionId)->toArray();
    }
    
    function editAdmissionQuestion($questionId, $questionInfo) {
       
         return $this->update($questionInfo, "id = $questionId");

    }
    
    function deleteAdmissionQuestion($questionId) {
        return $this->delete("id = $questionId");
    }
    
    
    function getAdmissionQuestionForDisplay($questionId) {
        
        $select = $this->select()->setIntegrityCheck(FALSE)->from("news", array("*", "news.id as nId"))->joinLeft("newsCategory", "news.categoryId=newsCategory.id")
                ->joinLeft("user", "news.createdBy=user.id",array("user.id as uid","firstName","lastName"))->where("news.id = $articleId");
        
        return $this->fetchRow($select)->toArray();
        
    }
    
    function getRandomQuestion($questionCategory = 0 , $questionLevel = 0){
        
        //Need more optimization easy but not efficient! (for ~5000 or less rows)
        $select = $this->select()->order("RAND()")->limit(1);
        if($questionCategory){
            $select->where("categoryId = ?",$questionCategory);
        }
        if($questionLevel){
            $select->where("levelId = ?",$questionLevel);
        }
        
        return $this->fetchRow($select)->toArray();
        
        
        
    }
    


}

