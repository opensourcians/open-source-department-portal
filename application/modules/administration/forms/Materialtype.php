<?php

class Administration_Form_Materialtype extends Zend_Form
{

    public function init()
    {
        $name = new Zend_Form_Element_Text("materialtype");
        $name->setRequired();
        $name->addValidator(new Zend_Validate_Alpha);
        $name->addFilter(new Zend_Filter_StripTags);
        $name->setAttrib("class", "form-control");
        $name->setAttrib("placeholder", "Enter Material Type");
        $name->setAttrib("id", "categoryname");
        $name->setLabel("Write Material Type");   
        $submit = new Zend_Form_Element_Submit("Add Material Type");
        $submit->setName("submit");
        $submit->setAttrib("class", "btn btn-default");
        $this->addElements(array($name,$submit));
        /* Form Elements & Other Definitions Here ... */
    }


}

