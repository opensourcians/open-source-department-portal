<?php

/* default/layout/main_footer.tpl */
class __TwigTemplate_bbb72f1016888f9c6f9dcb54071ebffe0750c66ad912bcbf1fce2889d4a73dad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["show_footer"]) ? $context["show_footer"] : null) == true)) {
            // line 2
            echo "    ";
            $this->env->loadTemplate("default/layout/footer.tpl")->display($context);
        }
        // line 4
        echo "</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "default/layout/main_footer.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 4,  21 => 2,  19 => 1,);
    }
}
