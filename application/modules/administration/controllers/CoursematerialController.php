<?php

class Administration_CoursematerialController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function deleteAction()
    {
         $coursematerial_id=$this->_request->getParam('id');
         $coursematerial_model=new Administration_Model_Coursematerial();
         $coursematerial_model->deleteCourseMaterial($coursematerial_id);
         $this->_redirect('/administration/coursematerial/list?msg=1');
    }

    public function addAction()
    {
          $material=new Administration_Form_Material();
          $this->view->material=$material;
          if($this->getRequest()->isPost()){
                $course_id=$this->getRequest()->getParam('course');
                $materialtype_id=$this->getRequest()->getParam('materialtype');
                $materialname=$this->getRequest()->getParam('materialname');
                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination(APPLICATION_PATH.'/uploads')->addValidator('Count', false, 1)
						 ->addValidator('Size', false,2097152)
						 ->addValidator('Extension', false, 'pdf');
                
                try {
				// upload received file(s)
			$upload->receive();
		} 
		catch (Zend_File_Transfer_Exception $e) 
		{
			$e->getMessage();
			exit;
		}
                $filename= $upload->getFileName();
                $coursematerial_model = new Administration_Model_Coursematerial();
                $coursematerial_model->addCourseMaterial($course_id,  md5($filename),$materialtype_id,$materialname);
                $this->redirect('/administration/course/index');
        }
        // action body
    }
    
    public function listAction()
    {
         $coursematerial_model = new Administration_Model_Coursematerial();
         $coursematerialdata = $coursematerial_model->listCourseMaterial();
         $this->view->coursematerialdata=  $coursematerialdata;
    }

    public function editAction()
    {
        // action body
    }
   


}









