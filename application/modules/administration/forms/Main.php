<?php

class Administration_Form_Main extends Zend_Form
{

    public function init()
    {
        $slider = new Zend_Form_Element_Select("slider");
        
        $slider->addMultiOption("","Select Album");
        $slider->setAttrib("class", "form-control");
        $slider->setDecorators(array('ViewHelper'));
        $albumModel = new Administration_Model_Album();
        $albums = $albumModel->listAlbums();
        foreach($albums as $album){
            $slider->addMultiOption($album['id'],$album['title']);
        }
        
        $serviceName = new Zend_Form_Element_Text("servicesName");
        $serviceName->addFilter(new Zend_Filter_StripTags);
        $serviceName->setAttrib("class", "form-control");
        $serviceName->setAttrib("placeholder", "Enter Service Name");
        $serviceName->setAttrib("id", "servicesName");
        $serviceName->removeDecorator('Label');
        $serviceName->setDecorators(array('ViewHelper'));
        
        $services = new Zend_Form_Element_Select("services");
        
        $services->addMultiOption("","Select Album");
        $services->setAttrib("class", "form-control");
        $services->setDecorators(array('ViewHelper'));
        $albumModel = new Administration_Model_Album();
        $albums = $albumModel->listAlbums();
        foreach($albums as $album){
            $services->addMultiOption($album['id'],$album['title']);
        }
        
        $technologies = new Zend_Form_Element_Select("technologies");
        
        $technologies->addMultiOption("","Select Album");
        $technologies->setAttrib("class", "form-control");
        $technologies->setDecorators(array('ViewHelper'));
        $albumModel = new Administration_Model_Album();
        $albums = $albumModel->listAlbums();
        foreach($albums as $album){
            $technologies->addMultiOption($album['id'],$album['title']);
        }
        
        $technologiesName = new Zend_Form_Element_Text("TechnologiesName");
        $technologiesName->addFilter(new Zend_Filter_StripTags);
        $technologiesName->setAttrib("class", "form-control");
        $technologiesName->setAttrib("placeholder", "Enter Technologies Name");
        $technologiesName->setAttrib("id", "technologiesName");
        $technologiesName->removeDecorator('Label');
        $technologiesName->setDecorators(array('ViewHelper'));
        
        $newsName = new Zend_Form_Element_Text("articlesName");
        $newsName->addFilter(new Zend_Filter_StripTags);
        $newsName->setAttrib("class", "form-control");
        $newsName->setAttrib("placeholder", "Enter News Name");
        $newsName->setAttrib("id", "articlesName");
        $newsName->removeDecorator('Label');
        $newsName->setDecorators(array('ViewHelper'));
        
        $articlesNo = new Zend_Form_Element_Text("articlesNo");
        $articlesNo->addFilter(new Zend_Filter_StripTags);
        $articlesNo->setAttrib("class", "form-control");
        $articlesNo->setAttrib("placeholder", "Enter Articles Number");
        $articlesNo->setAttrib("id", "articlesNo");
        $articlesNo->removeDecorator('Label');
        $articlesNo->setDecorators(array('ViewHelper'));
       
        $poll = new Zend_Form_Element_Select("poll");
        
        $poll->addMultiOption("","Select Poll");
        $poll->setAttrib("class", "form-control");
        $poll->setDecorators(array('ViewHelper'));
        $pollModel = new Administration_Model_Poll();
        $polls = $pollModel->listPolls();
        foreach($polls as $pollx){
            $poll->addMultiOption($pollx['id'],$pollx['pollQuestion']);
        }
        
        $video = new Zend_Form_Element_Select("video");
        
        $video->addMultiOption("","Select Video");
        $video->setAttrib("class", "form-control");
        $video->setDecorators(array('ViewHelper'));
        $videoModel = new Administration_Model_Video();
        $videos = $videoModel->listVideos();
        foreach($videos as $videox){
            $video->addMultiOption($videox['id'],$videox['videoTitle']);
        }
        
        $videoName = new Zend_Form_Element_Text("videoName");
        $videoName->addFilter(new Zend_Filter_StripTags);
        $videoName->setAttrib("class", "form-control");
        $videoName->setAttrib("placeholder", "Enter Video Name");
        $videoName->setAttrib("id", "videoName");
        $videoName->removeDecorator('Label');
        $videoName->setDecorators(array('ViewHelper'));
        
        $pollName = new Zend_Form_Element_Text("pollName");
        $pollName->addFilter(new Zend_Filter_StripTags);
        $pollName->setAttrib("class", "form-control");
        $pollName->setAttrib("placeholder", "Enter Poll Name");
        $pollName->setAttrib("id", "pollName");
        $pollName->removeDecorator('Label');
        $pollName->setDecorators(array('ViewHelper'));
        
        $datesName = new Zend_Form_Element_Text("dateName");
        $datesName->addFilter(new Zend_Filter_StripTags);
        $datesName->setAttrib("class", "form-control");
        $datesName->setAttrib("placeholder", "Enter Date Name");
        $datesName->setAttrib("id", "dateName");
        $datesName->removeDecorator('Label');
        $datesName->setDecorators(array('ViewHelper'));
        
        $eventName = new Zend_Form_Element_Text("eventName");
        $eventName->addFilter(new Zend_Filter_StripTags);
        $eventName->setAttrib("class", "form-control");
        $eventName->setAttrib("placeholder", "Enter Event Name");
        $eventName->setAttrib("id", "eventName");
        $eventName->removeDecorator('Label');
        $eventName->setDecorators(array('ViewHelper'));
        
        $google = new Zend_Form_Element_Text("google");
        $google->addFilter(new Zend_Filter_StripTags);
        $google->setAttrib("class", "form-control");
        $google->setAttrib("placeholder", "Enter google");
        $google->setAttrib("id", "google");
        $google->removeDecorator('Label');
        $google->setDecorators(array('ViewHelper'));
        
        $twitter = new Zend_Form_Element_Text("twitter");
        $twitter->addFilter(new Zend_Filter_StripTags);
        $twitter->setAttrib("class", "form-control");
        $twitter->setAttrib("placeholder", "Enter twitter");
        $twitter->setAttrib("id", "twitter");
        $twitter->removeDecorator('Label');
        $twitter->setDecorators(array('ViewHelper'));
        
        
        $facebook = new Zend_Form_Element_Text("facebook");
        $facebook->addFilter(new Zend_Filter_StripTags);
        $facebook->setAttrib("class", "form-control");
        $facebook->setAttrib("placeholder", "Enter facebook");
        $facebook->setAttrib("id", "facebook");
        $facebook->removeDecorator('Label');
        $facebook->setDecorators(array('ViewHelper'));
        
        $linkedIn = new Zend_Form_Element_Text("linkedIn");
        $linkedIn->addFilter(new Zend_Filter_StripTags);
        $linkedIn->setAttrib("class", "form-control");
        $linkedIn->setAttrib("placeholder", "Enter linkedin");
        $linkedIn->setAttrib("id", "linkedIn");
        $linkedIn->removeDecorator('Label');
        $linkedIn->setDecorators(array('ViewHelper'));
        
        $activityName = new Zend_Form_Element_Text("activityName");
        $activityName->addFilter(new Zend_Filter_StripTags);
        $activityName->setAttrib("class", "form-control");
        $activityName->setAttrib("placeholder", "Enter Activity Name");
        $activityName->setAttrib("id", "activityName");
        $activityName->removeDecorator('Label');
        $activityName->setDecorators(array('ViewHelper'));
        
        $aboutUs = new Zend_Form_Element_Textarea("aboutUs");
        
        $aboutUs->setAttrib("class", "form-control");
        $aboutUs->setAttrib("id", "ckeditor_full");
        $aboutUs->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($slider,$services,$serviceName,$technologies,$technologiesName,
                $aboutUs,$articlesNo, $poll,$video,$pollName,$videoName,$newsName,$activityName,
            $google,$facebook,$twitter, $linkedIn, $submit));
    }


}

