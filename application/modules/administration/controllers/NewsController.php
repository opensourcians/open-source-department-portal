<?php

class Administration_NewsController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/news/list");
      
    }

    public function listAction()
    {
        $newsModel = new Administration_Model_News();
        if($this->getRequest()->getParam("categoryId")){
            $news = $newsModel->listImages($this->getRequest()->getParam("categoryId"));
        } else {
            $news = $newsModel->listImages();
        }
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($news);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
        
    }

    public function addAction()
    {
        $newsForm = new Administration_Form_News();


        $this->view->form = $newsForm;
        $this->view->post = "/administration/news/add";
        $this->view->viewName = "Add Article";


        if ($this->getRequest()->isPost()) {
            if ($newsForm->isValid($this->getRequest()->getParams())) {
                $newsModel = new Administration_Model_News();

                $articleId = $newsModel->addArticle($newsForm->getValues());

                if ($articleId) {
                    //post article to Facebook
                    $newsModel->postArticleToFb($newsForm->getValues(), $articleId);
                    $this->_flashMessenger->addMessage('Article added successfully!');
                    $this->redirect("/administration/news/list");
                }
            } else {
                $this->view->errors = $newsForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction()
    {
         //Get instances
        $newsForm = new Administration_Form_News();
        $newsModel = new Administration_Model_News();


        //get user id from request
        $id = $this->getRequest()->getParam('id');

        //get user information
        $article = $newsModel->getArticleById($id);

        //send values to view
        $this->view->post = "/administration/news/edit";
        $this->view->viewName = "Edit Article";
        
        //Not required to modify image on editing
        $sliderImage = $newsForm->getElement("sliderImage");
        $sliderImage->setRequired(FALSE);

        if ($this->getRequest()->isPost()) {
            if ($newsForm->isValid($this->getRequest()->getParams())) {

                $result = $newsModel->editArticle($newsForm->getValue("id"), $newsForm->getValues());

                $this->_flashMessenger->addMessage('Article edited successfully!');
                $this->redirect("/administration/news/list");
            } else {
                $this->view->errors = $newsForm->getMessages();
            }
        }

        if ($article) {
            $newsForm->populate($article[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no article with this id!');
            $this->redirect("/administration/news/list");
        }

        

        $this->view->form = $newsForm;
        $this->render("form");
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $newsModel = new Administration_Model_News();
        $result = $newsModel->deleteArticle($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Article deleted successfully!');
            echo $result;
        }
    }


}









