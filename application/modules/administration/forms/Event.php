<?php

class Administration_Form_Event extends Zend_Form
{

    public function init()
    {
         $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $title = new Zend_Form_Element_Text("title");
        $title->addFilter(new Zend_Filter_StripTags);
        $title->setAttrib("class", "form-control");
        $title->setAttrib("placeholder", "Enter Title");
        $title->setAttrib("id", "title");
        $title->setRequired();
        $title->removeDecorator('Label');
        $title->setDecorators(array('ViewHelper'));
        
        $fromDate = new Zend_Form_Element_Text("fromDate");
        $fromDate->setAttrib("class", "form-control");
        //$fromDate->setAttrib("disabled", "disabled");
        $fromDate->setRequired();
        $fromDate->removeDecorator('Label');
        $fromDate->setDecorators(array('ViewHelper'));
        
        $toDate = new Zend_Form_Element_Text("toDate");
        $toDate->setAttrib("class", "form-control");
        //$toDate->setAttrib("disabled", "disabled");
        $toDate->setRequired();
        $toDate->removeDecorator('Label');
        $toDate->setDecorators(array('ViewHelper'));
        
        $description = new Zend_Form_Element_Textarea("description");
        $description->setAttrib("class", "form-control");
        $description->setAttrib("id", "ckeditor_full");
        $description->removeDecorator('Label');
        $description->setDecorators(array('ViewHelper'));
        
        $place = new Zend_Form_Element_Text("place");
        $place->setAttrib("class", "form-control");
        $place->setAttrib("id", "toDate");
        $place->setRequired();
        $place->removeDecorator('Label');
        $place->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($id, $title,$fromDate,$toDate, $description,$place,$submit));
    
    }


}

