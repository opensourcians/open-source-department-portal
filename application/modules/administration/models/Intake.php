<?php

class Administration_Model_Intake extends Zend_Db_Table_Abstract
{
    protected $_name = "intake";
    
    function listIntakes(){
        return $this->fetchAll()->toArray();
    }

}