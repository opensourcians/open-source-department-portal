<?php

class Administration_VideoController extends Zend_Controller_Action
{
    /**
     * FlashMessenger
     *
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;

    public function init() {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction() {
        $this->redirect("/administration/video/list");
    }

    public function listAction() {
        $videoModel = new Administration_Model_Video();
        $videos = $videoModel->listVideos();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($videos);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
    }

    public function addAction() {
        
        $videoForm = new Administration_Form_Video();

        $this->view->form = $videoForm;
        $this->view->post = "/administration/video/add";
        $this->view->viewName = "Add Video";


        if ($this->getRequest()->isPost()) {
            if ($videoForm->isValid($this->getRequest()->getParams())) {
                $videoModel = new Administration_Model_Video();
                $videoId = $videoModel->addVideo($videoForm->getValues());

                if ($videoId) {
                    $this->_flashMessenger->addMessage('Video added successfully!');
                    $this->redirect("/administration/video/list");
                }
            } else {

                $this->view->errors = $videoForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction() {
        //Get instances
        $videoForm = new Administration_Form_Video();
        $videoModel = new Administration_Model_Video();

        //get page id from request
        $id = $this->getRequest()->getParam('id');

        //get page information
        $video = $videoModel->getVideoById($id);


        //send values to view
        $this->view->post = "/administration/video/edit";
        $this->view->viewName = "Edit Video";

        if ($video) {
            $videoForm->populate($video[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no video with this id!');
            $this->redirect("/administration/video/list");
        }

        if ($this->getRequest()->isPost()) {
            if ($videoForm->isValid($this->getRequest()->getParams())) {

                $result = $videoModel->editVideo($videoForm->getValue("id"), $videoForm->getValues());

                $this->_flashMessenger->addMessage('Video edited successfully!');
                $this->redirect("/administration/video/list");
            }
        }

        $this->view->form = $videoForm;
        $this->render("form");
    }

    public function deleteAction() {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");
        $videoModel = new Administration_Model_Video();
        $result = $videoModel->deleteVideo($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Video deleted successfully!');
            echo $result;
        }
    }


}

