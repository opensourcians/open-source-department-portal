<?php

class Administration_Model_NewsCategory extends Zend_Db_Table_Abstract
{
    protected $_name = "newsCategory";
    
     function listNewsCategories(){
        return $this->fetchAll()->toArray();
    }
    
    function addNewsCategory($newsCategoryInfo) {
        
        $row = $this->createRow();
        $row->name = $newsCategoryInfo['name'];
                
        return $row->save();
    }
    
    function getNewsCategoryById($newsCategoryId) {
        
        return $this->find($newsCategoryId)->toArray();
    }
    
    function editNewsCategory($newsCategoryId, $newsCatrgoryInfo) {
        
        return $this->update($newsCatrgoryInfo, "id = $newsCategoryId");

    }
    
    function deleteNewsCategory($newsCategoryId) {
        return $this->delete("id = $newsCategoryId");
    }
    

}

