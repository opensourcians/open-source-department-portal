<?php

class Administration_Model_Menu {

    function saveMenuAsXML($jsonObj) {
        $array = json_decode($jsonObj, TRUE);
        //return var_dump($array);
        $doc = new DOMDocument();
        $doc->formatOutput = TRUE;

        //create root element and top element
        $configData = $doc->createElement("configdata");
        $doc->appendChild($configData);

        $nav = $doc->createElement("nav");
        $configData->appendChild($nav);
        $incremntor = 0;
        foreach ($array as $innerArray) {
            $page = $doc->createElement("page_$incremntor");
            $nav->appendChild($page);
            foreach ($innerArray as $key => $value) {

                if ($key == 'navLabel') {
                    $label = $doc->createElement("label");
                    $label->appendChild($doc->createTextNode($value));
                    $page->appendChild($label);
                } elseif ($key == 'navUri') {
                    if (strpos($value, "http://") !== false || strpos($value, "www.") !== false || strpos($value, "#") !== false) {
                        $uri = $doc->createElement("uri");
                        $uri->appendChild($doc->createTextNode($value));
                        $page->appendChild($uri);
                    } else {
                        $uriArray = array_filter(explode("/", $value));
                        switch (count($uriArray)) {
                            case 0:
                                
                                
                                $controller = $doc->createElement("controller");
                                $controller->appendChild($doc->createTextNode("index"));
                                $page->appendChild($controller);

                                $action = $doc->createElement("action");
                                $action->appendChild($doc->createTextNode("index"));
                                $page->appendChild($action);

                                //add default module as no module specified
                                $module = $doc->createElement("module");
                                $module->appendChild($doc->createTextNode("default"));
                                $page->appendChild($module);
                                break;
                            case 1:
                                $controller = $doc->createElement("controller");
                                $controller->appendChild($doc->createTextNode($uriArray[0]));
                                $page->appendChild($controller);

                                $action = $doc->createElement("action");
                                $action->appendChild($doc->createTextNode("index"));
                                $page->appendChild($action);

                                //add default module as no module specified
                                $module = $doc->createElement("module");
                                $module->appendChild($doc->createTextNode("default"));
                                $page->appendChild($module);

                                break;
                            case 2:
                                //we have a controller and action
                                $controller = $doc->createElement("controller");
                                $controller->appendChild($doc->createTextNode($uriArray[0]));
                                $page->appendChild($controller);

                                $action = $doc->createElement("action");
                                $action->appendChild($doc->createTextNode($uriArray[1]));
                                $page->appendChild($action);

                                //add default module as no module specified
                                $module = $doc->createElement("module");
                                $module->appendChild($doc->createTextNode("default"));
                                $page->appendChild($module);

                                break;

                            case 3:

                                //we have a controller, action and module
                                $module = $doc->createElement("module");
                                $module->appendChild($doc->createTextNode($uriArray[0]));
                                $page->appendChild($module);

                                $controller = $doc->createElement("controller");
                                $controller->appendChild($doc->createTextNode($uriArray[1]));
                                $page->appendChild($controller);

                                $action = $doc->createElement("action");
                                $action->appendChild($doc->createTextNode($uriArray[2]));
                                $page->appendChild($action);

                                break;

                            //more than 3, oh! there is a number of parameters!
                            default:
                                $controller = $doc->createElement("controller");
                                $controller->appendChild($doc->createTextNode(array_shift($uriArray)));
                                $page->appendChild($controller);

                                $action = $doc->createElement("action");
                                $action->appendChild($doc->createTextNode(array_shift($uriArray)));
                                $page->appendChild($action);

                                //add default module as no module specified
                                $module = $doc->createElement("module");
                                $module->appendChild($doc->createTextNode("default"));
                                $page->appendChild($module);

                                $params = $doc->createElement("params");
                                $page->appendChild($params);

                                for ($i = 0; $i < count($uriArray); $i += 2) {
                                    $param = $doc->createElement($uriArray[$i]);
                                    $param->appendChild($doc->createTextNode($uriArray[$i + 1]));
                                    $params->appendChild($param);
                                }
                        }
                    }
                } else {
                    $pagesArr = $value;
                    $pages = $doc->createElement("pages");
                    $page->appendChild($pages);
                    foreach ($pagesArr as $valueArr) {
                        $incremntor++;
                        $page = $doc->createElement("page_$incremntor");
                        $pages->appendChild($page);
                        foreach ($valueArr as $key => $value) {
                            if ($key == 'navLabel') {
                                $label = $doc->createElement("label");
                                $label->appendChild($doc->createTextNode($value));
                                $page->appendChild($label);
                            } elseif ($key == 'navUri') {
                                if (strpos($value, "http://") !== false || strpos($value, "www.") !== false || strpos($value, "#") !== false) {
                                    $uri = $doc->createElement("uri");
                                    $uri->appendChild($doc->createTextNode($value));
                                    $page->appendChild($uri);
                                } else {
                                    $uriArray = array_filter(explode("/", $value));
                                    switch (count($uriArray)) {
                                        case 0:
                                            $controller = $doc->createElement("controller");
                                            $controller->appendChild($doc->createTextNode("index"));
                                            $page->appendChild($controller);

                                            $action = $doc->createElement("action");
                                            $action->appendChild($doc->createTextNode("index"));
                                            $page->appendChild($action);

                                            //add default module as no module specified
                                            $module = $doc->createElement("module");
                                            $module->appendChild($doc->createTextNode("default"));
                                            $page->appendChild($module);
                                            break;
                                        case 1:
                                            $controller = $doc->createElement("controller");
                                            $controller->appendChild($doc->createTextNode($uriArray[0]));
                                            $page->appendChild($controller);

                                            $action = $doc->createElement("action");
                                            $action->appendChild($doc->createTextNode("index"));
                                            $page->appendChild($action);

                                            //add default module as no module specified
                                            $module = $doc->createElement("module");
                                            $module->appendChild($doc->createTextNode("default"));
                                            $page->appendChild($module);

                                            break;
                                        case 2:
                                            //we have a controller and action
                                            $controller = $doc->createElement("controller");
                                            $controller->appendChild($doc->createTextNode($uriArray[0]));
                                            $page->appendChild($controller);

                                            $action = $doc->createElement("action");
                                            $action->appendChild($doc->createTextNode($uriArray[1]));
                                            $page->appendChild($action);

                                            //add default module as no module specified
                                            $module = $doc->createElement("module");
                                            $module->appendChild($doc->createTextNode("default"));
                                            $page->appendChild($module);

                                            break;

                                        case 3:

                                            //we have a controller, action and module
                                            $module = $doc->createElement("module");
                                            $module->appendChild($doc->createTextNode($uriArray[0]));
                                            $page->appendChild($module);

                                            $controller = $doc->createElement("controller");
                                            $controller->appendChild($doc->createTextNode($uriArray[1]));
                                            $page->appendChild($controller);

                                            $action = $doc->createElement("action");
                                            $action->appendChild($doc->createTextNode($uriArray[2]));
                                            $page->appendChild($action);

                                            break;

                                        //more than 3, oh! there is a number of parameters!
                                        default:
                                            $controller = $doc->createElement("controller");
                                            $controller->appendChild($doc->createTextNode(array_shift($uriArray)));
                                            $page->appendChild($controller);

                                            $action = $doc->createElement("action");
                                            $action->appendChild($doc->createTextNode(array_shift($uriArray)));
                                            $page->appendChild($action);

                                            //add default module as no module specified
                                            $module = $doc->createElement("module");
                                            $module->appendChild($doc->createTextNode("default"));
                                            $page->appendChild($module);

                                            $params = $doc->createElement("params");
                                            $page->appendChild($params);

                                            for ($i = 0; $i < count($uriArray); $i += 2) {
                                                $param = $doc->createElement($uriArray[$i]);
                                                $param->appendChild($doc->createTextNode($uriArray[$i + 1]));
                                                $params->appendChild($param);
                                            }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $incremntor++;
        }

        return $doc->save(APPLICATION_PATH . "/configs/navigation.xml");
    }

    function saveMenuAsHTML($menuHTML) {
        file_put_contents(APPLICATION_PATH . "/configs/menu.html", $menuHTML);
    }

    function loadMenuHTML() {
        return file_get_contents(APPLICATION_PATH . "/configs/menu.html");
    }

}
