<?php

class Application_Model_Project extends Zend_Db_Table_Abstract
{

    protected $_name="project";
    public function listProject() 
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        return $this->fetchAll($select)->toArray();
    }
    //public function 
    public function listtrack()
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from("project_track");
        return $this->fetchAll($select)->toArray();
    }
    public function listteam()
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from("project_team");
        return $this->fetchAll($select)->toArray();
    }
    public function listtag()
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from("project_tag");
        return $this->fetchAll($select)->toArray();
    }
    
}

