<?php

class Administration_Form_Project extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $pname = new Zend_Form_Element_Text('pname');
        $pname->setRequired();
        $pname->setLabel('Projec Name');
        $pname->setAttrib('size', '60');
        
        $tname = new Zend_Form_Element_Text('tname');
        $tname->setRequired();
        $tname->setLabel('Track')->setAttrib('size', '60');
        
        $tpid = new Zend_Form_Element_Text('tpid');
        $tpid->setRequired();
        $tpid->setLabel('Intake')->setAttrib('size', '60');
        
        $pdesc = new Zend_Form_Element_Textarea('pdesc');
        $pdesc->setRequired();
        $pdesc->setLabel('Description')->setAttrib('cols', '60')->setAttrib('rows', '4');
        
        $phis = new Zend_Form_Element_Textarea('phis');
        $phis->setRequired();
        $phis->setLabel('History')->setAttrib('cols', '60')->setAttrib('rows', '4');
        
        $pdoc = new Zend_Form_Element_Text('pdoc');
        $pdoc->setRequired();
        $pdoc->setLabel('Documentation')->setAttrib('size', '60');
        
        $pimage = new Zend_Form_Element_Text('pimage');
        $pimage->setRequired();
        $pimage->setLabel('Image')->setAttrib('size', '60');
        
        $tags = new Zend_Form_Element_Text('tag');
        $tags->setRequired();
        $tags->setLabel('Tags')->setAttrib('size', '60');
        
        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setValue('Submit');
        
        $reset = new Zend_Form_Element_Reset('reset');
        $reset->setValue('Reset');
        
        $this->addElements(array($pname,$tname,$tpid,$pdesc,$phis,$pdoc,$pimage,$tags,$submit,$reset));
    }


}

