<?php

class Administration_Model_Branch extends Zend_Db_Table_Abstract
{
    protected $_name = "branch";
    
    function listBranches(){
        return $this->fetchAll()->toArray();
    }

}