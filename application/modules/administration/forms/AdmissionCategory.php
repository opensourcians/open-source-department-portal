<?php

class Administration_Form_AdmissionCategory extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $name = new Zend_Form_Element_Text("name");
        $name->addFilter(new Zend_Filter_StripTags);
        $name->setRequired();
        $name->setAttrib("class", "form-control");
        $name->setAttrib("placeholder", "Enter Category Name");
        $name->setAttrib("id", "name");
        $name->removeDecorator('Label');
        $name->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($id,$name,$submit));
    }


}

