<?php

class Administration_Model_News extends Zend_Db_Table_Abstract
{
    protected $_name = "news";
    
     function listImages($categoryId = 0){
        $select = $this->select()->setIntegrityCheck(FALSE)->from("news", array("*", "news.id as nId"))->joinLeft("newsCategory", "news.categoryId=newsCategory.id")
                ->joinLeft("user", "news.createdBy=user.id",array("user.id as uid","firstName","lastName"))->order("news.createdOn Desc");
        if($categoryId){
            $select->where("categoryId = $categoryId");
        }
        return $this->fetchAll($select)->toArray();
    }
    
    function addArticle($articleInfo) {
        //created by
        $auth = Zend_Auth::getInstance();
        $userInfo = $auth->getIdentity();   
        
        $row = $this->createRow();
        $row->categoryId = $articleInfo['categoryId'];
        $row->title = $articleInfo['title'];
        $row->body = $articleInfo['body'];
        $row->sliderImage = $articleInfo['sliderImage'];
        $row->createdBy = $userInfo->id;
        
        return $row->save();
    }
    
    function getArticleById($articleId) {
        
        return $this->find($articleId)->toArray();
    }
    
    function editArticle($articleId, $articleInfo) {
        
        $articleInfo['updatedOn'] = date('Y-m-d H:i:s');
        
        //if no image do not update this column
        if (empty($articleInfo['sliderImage'])) {
            unset($articleInfo['sliderImage']);
        }
        
        //edited by
        $auth = Zend_Auth::getInstance();
        $userInfo = $auth->getIdentity();
        $articleInfo['updatedBy'] = $userInfo->id;
        
         return $this->update($articleInfo, "id = $articleId");

    }
    
    function deleteArticle($articleId) {
        return $this->delete("id = $articleId");
    }
    
    //Display on frontpage
    function getArticlesForDisplay($articleCount) {
        
        $select = $this->select()->order("createdOn Desc")->limit($articleCount);
        
        return $this->fetchAll($select)->toArray();
        
    }
    
    function getArticleForDisplay($articleId) {
        
        $select = $this->select()->setIntegrityCheck(FALSE)->from("news", array("*", "news.id as nId"))->joinLeft("newsCategory", "news.categoryId=newsCategory.id")
                ->joinLeft("user", "news.createdBy=user.id",array("user.id as uid","firstName","lastName"))->where("news.id = $articleId");
        
        return $this->fetchRow($select)->toArray();
        
    }
    
    function postArticleToFb($articleInfo,$article_id){
        
        $page_access_token = 'CAAHYUgKJa2YBAHvbfZBVqumQM4wZBmItsWR8udZC6XVZCvdh5NQMglsvzWH08uLW5ZC2VRcp8l8C3xz02nvwB1M4aB07RDZCppd4KEaDZAxR7HDylZCZAqor56JxwYwZCS32yQKHqNSr9vsLqB6ZBOIoy789xnbf9n00XIsghTgIgniHnoEMZBKuCR9v8hi3gSZCgiqsZD';
        $page_id = "587499874673518";
        
        $strippedString = strip_tags($articleInfo['body']);
        $formatedString = (strlen($strippedString) > 120) ? substr($strippedString, 0, 115) . '...' : $strippedString;
        
        //generate URL
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $url = $request->getScheme() . '://' . $request->getHttpHost();
        
        $data['picture'] = $url."/images/".$articleInfo['sliderImage'];
        $data['name'] = $articleInfo['title'];
        $data['link'] = $url."/news/view/id/$article_id";
        $data['message'] = $articleInfo['title']." ".html_entity_decode($formatedString);
        $data['caption'] = "Posted from os.iti.gov.eg";
        $data['description'] = $formatedString;
        $data['access_token'] = $page_access_token;
        $post_url = 'https://graph.facebook.com/'.$page_id.'/feed';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $post_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close($ch);
        
        
                                            
        
    }


}

