<?php

class Administration_Model_Applicant extends Zend_Db_Table_Abstract
{
    protected $_name = "admissionApplicant";
    
     function listApplicants(){
        return $this->fetchAll()->toArray();
    }
    
    function addApplicant($applicantInfo) {
        
        $row = $this->createRow();
        $row->applicantCode = $applicantInfo['code'];
        $row->applicantName = $applicantInfo['name'];
                
        return $row->save();
    }
    
    function getApplicantById($applicantId) {
        
        return $this->find($applicantId)->toArray();
    }
    
    function editApplicant($applicantId, $applicantInfo) {
        
        return $this->update($applicantInfo, "id = $applicantId");

    }
    
    function deleteApplicant($applicantId) {
        return $this->delete("id = $applicantId");
    }
    
    function calculateApplicantRank($applicantId){
        
    }

}

