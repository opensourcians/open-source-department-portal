<?php

class Application_Form_Registration extends Zend_Form
{

    public function init()
    {
       
        $firstName = new Zend_Form_Element_Text("firstName");
        $firstName->addFilter(new Zend_Filter_StripTags);
        $firstName->setRequired();
        $firstName->setAttrib("class", "form-control");
        $firstName->setAttrib("placeholder", "Enter First Name");
        $firstName->setAttrib("id", "firstName");
        $firstName->removeDecorator('Label');
        $firstName->setDecorators(array('ViewHelper'));
        
        $lastName = new Zend_Form_Element_Text("lastName");
        $lastName->addFilter(new Zend_Filter_StripTags);
        $lastName->setAttrib("class", "form-control");
        $lastName->setAttrib("placeholder", "Enter Last Name");
        $lastName->setAttrib("id", "lastName");
        $lastName->setRequired();
        $lastName->removeDecorator('Label');
        $lastName->setDecorators(array('ViewHelper'));
        
        
        $email = new Zend_Form_Element_Text("email");
        $email->addValidator(new Zend_Validate_EmailAddress);
        $email->addValidator(new Zend_Validate_Db_NoRecordExists(array("table"=>'user',"field"=>'email')));
        $email->addFilter(new Zend_Filter_StripTags);
        $email->setRequired();
        $email->setAttrib("class", "form-control");
        $email->setAttrib("placeholder", "Enter Email");
        $email->setAttrib("id", "email");
        $email->removeDecorator('Label');
        $email->setDecorators(array('ViewHelper'));
        
        
        $password = new Zend_Form_Element_Password("password");
        $password->setRequired();
        $password->addValidator(new Zend_Validate_StringLength(6, 20));
        $password->setAttrib("class", "form-control");
        $password->setAttrib("placeholder", "Enter Password");
        $password->setAttrib("id", "password");
        $password->removeDecorator('Label');
        $password->setDecorators(array('ViewHelper'));
        
        $passwordRetype = new Zend_Form_Element_Password("passwordRetype");
        $passwordRetype->setRequired();
        $passwordRetype->addValidator('identical', false, array('token' => 'password'));
        $passwordRetype->setAttrib("class", "form-control");
        $passwordRetype->setAttrib("placeholder", "Retype Password");
        $passwordRetype->setAttrib("id", "passwordRetype");
        $passwordRetype->removeDecorator('Label');
        $passwordRetype->setDecorators(array('ViewHelper'));
        
        $gender = new Zend_Form_Element_Hidden("gender");
        $gender->setDecorators(array('ViewHelper'));
        
        $jobTitle = new Zend_Form_Element_Hidden("jobTitle");
        $jobTitle->setDecorators(array('ViewHelper'));
        
        $facebook = new Zend_Form_Element_Hidden("facebook");
        $facebook->setDecorators(array('ViewHelper'));
        
        
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($firstName,$lastName,$email,$password,$passwordRetype,$jobTitle,$gender,$facebook,$submit));
        
        
    }


}

