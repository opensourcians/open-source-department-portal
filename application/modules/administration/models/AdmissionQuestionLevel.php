<?php

class Administration_Model_AdmissionQuestionLevel extends Zend_Db_Table_Abstract
{
    protected $_name = "admissionQuestionLevel";
    
     function listAdmissionQuestionLevels(){
        return $this->fetchAll()->toArray();
    }
    
}

