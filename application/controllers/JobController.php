<?php

class JobController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $this->redirect("/job/list");
    }

    public function listAction(){
        $jobModel = new Application_Model_Job();
        $jobs = $jobModel->listAll();
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($jobs);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->paginator = $paginator;
    }

    public function addAction()
    {
        // action body
        
        $jobForm = new Application_Form_Job();

        $this->view->form = $jobForm;
        $this->view->post = "/job/add";
        $this->view->viewName = "Add Job";
        
        if ($this->getRequest()->isPost()) {
            if ($jobForm->isValid($this->getRequest()->getParams())) {
                $jobModel = new Application_Model_Job();
                $jobModel->addJob($jobForm->getValues());
                //Auto FaceBook POST. 
                    $config = array();
                    $config['appId'] = '828426203854535';
                    $config['secret'] = '58f0783d91fc8e986dd5d63705592701';
                    $fb = new Facebook_Facebook($config);
                    $params = array(
                        "access_token" => "CAALxcwGadscBACILClyeZCblpHzdkZB48ZCFqwQ3VCDDiJ5ZCorE04ZBR2J5pgzEDniRTHtVtYanaD70ZBP3hGMK8c30BwZAqbQZC626ne0ZCQWd2mDq7jtHj1KNvkkDLq8fjdle5S1GmHQh6OdmDUyBRPs117E8lxW7h8H6d6B1J4ACpqeL9SvTMggi2uB8wBYc3Wk1lP3puGzyGQwDo49f3",
                        "message" => "",
                        "link" => "",
                        "picture" => "",
                        "name" => "",
                        "caption" => "",
                        "description" => ""
                    );
                    $fb->api('/100001429930425/feed', 'POST', $params); 
                //End FaceBook POST.
                $this->redirect("/job/list");
            } else {
                $this->view->errors = $jobForm->getMessages();
            }
        }
        $this->render("form");
    }
    
    /* function to display page for more details about job
     public function viewAction() {
        //Get instances
        $jobModel = new Application_Model_Job();
        //get Job id from request
        $id = $this->getRequest()->getParam('id');
        //get page information
        $job = $jobModel->getJobForDisplay($id);
        if ($job) {
            $this->view->job = $job;
        } else {
            $this->redirect("/");
        }
    }*/
}