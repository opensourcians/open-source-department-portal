<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModuleLayout
 *
 * @author Islam Askar 
 */
class Application_Plugin_Authorize extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        $authorization = Zend_Auth::getInstance();
        $view = Zend_Layout::getMvcInstance()->getView();
        if ($authorization->hasIdentity()) {
            $view->identity = $authorization->getIdentity();
        }
        //login form for frontend
            $loginForm = new Application_Form_Login();
            $view->loginForm = $loginForm;
            
        //settings
        $mainModel = new Administration_Model_Main();
        $settings = $mainModel->getSettings();
        $view->settings = $settings;
            
    }

}
