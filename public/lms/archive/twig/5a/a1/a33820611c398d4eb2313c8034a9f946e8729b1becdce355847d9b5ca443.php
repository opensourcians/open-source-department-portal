<?php

/* default/layout/show_footer.tpl */
class __TwigTemplate_5aa1a33820611c398d4eb2313c8034a9f946e8729b1becdce355847d9b5ca443 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        if (((isset($context["show_header"]) ? $context["show_header"] : null) == true)) {
            // line 8
            echo "    </section>
</div>
";
        }
        // line 11
        echo "
";
        // line 13
        if ((!(null === (isset($context["plugin_content_bottom"]) ? $context["plugin_content_bottom"] : null)))) {
            // line 14
            echo "    <div id=\"plugin_content_bottom\" class=\"span12\">
        ";
            // line 15
            echo (isset($context["plugin_content_bottom"]) ? $context["plugin_content_bottom"] : null);
            echo "
    </div>
";
        }
        // line 18
        echo "
";
        // line 19
        if (((isset($context["show_footer"]) ? $context["show_footer"] : null) == true)) {
            // line 20
            echo "        </div> <!-- end of #row\" -->
    </div> <!-- end of #main\" -->
    <div class=\"push\"></div>
    </div> <!-- end of #page section -->
</div> <!-- end of #wrapper section -->

";
        }
        // line 27
        echo "
";
        // line 28
        $this->env->loadTemplate("default/layout/main_footer.tpl")->display($context);
    }

    public function getTemplateName()
    {
        return "default/layout/show_footer.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 28,  54 => 27,  45 => 20,  43 => 19,  40 => 18,  34 => 15,  31 => 14,  29 => 13,  26 => 11,  21 => 8,  19 => 7,);
    }
}
