<?php

class Administration_Model_Image extends Zend_Db_Table_Abstract
{
    protected $_name = "image";
    
    
     function listImages($albumId = 0){
        $select = $this->select()->setIntegrityCheck(FALSE)->from("image", array("*", "image.id as iId", "image.title as iTitle"))->joinLeft("album", "image.albumId=album.id");
        if($albumId){
            $select->where("albumId = $albumId");
        }
        return $this->fetchAll($select)->toArray();
    }
    function addImage($imageInfo) {
                
        $row = $this->createRow();
        $row->name = $imageInfo['name'];
        $row->albumId = $imageInfo['albumId'];
        $row->title = isset($imageInfo['title']) ?  $imageInfo['title'] : '';
        $row->description = isset($imageInfo['description']) ? $imageInfo['description'] : '';
        $row->url = isset($imageInfo['url']) ? $imageInfo['url'] : '';
        return $row->save();
    }
    
    
    function getImageById($imageId) {
        
        return $this->find($imageId)->toArray();
    }
    
    function editImage($imageId, $imageInfo) {
        
        
        
        //if no image do not update this column
        if (empty($imageInfo['name'])) {
            unset($imageInfo['name']);
        }
        
         return $this->update($imageInfo, "id = $imageId");

    }
    
    function setAlbumCover($imageId,$albumId){
        $this->update(array('albumCover'=>0),"albumId = $albumId");
        return $this->update(array('albumCover'=>1),"id = $imageId");
    }
    
    function deleteImage($imageId) {
        return $this->delete("id = $imageId");
    }
}

