<?php

class Administration_AlbumController extends Zend_Controller_Action {

    public function init() {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction() {
        $this->redirect("/administration/album/list");
    }

    public function listAction() {
        $albumModel = new Administration_Model_Album();
        $albums = $albumModel->listAlbums();
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($albums);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
    }

    public function addAction() {
        $albumForm = new Administration_Form_Album();


        $this->view->form = $albumForm;
        $this->view->post = "/administration/album/add";
        $this->view->viewName = "Add Album";


        if ($this->getRequest()->isPost()) {
            if ($albumForm->isValid($this->getRequest()->getParams())) {
                $albumModel = new Administration_Model_Album();

                $albumId = $albumModel->addAlbum($albumForm->getValues());

                if ($albumId) {
                     $dir_name = APPLICATION_PATH . "/../public/upload/albums/$albumId";
                     mkdir($dir_name);
                    $this->_flashMessenger->addMessage('Album added successfully!');
                    $this->redirect("/administration/album/upload/id/$albumId");
                }
            } else {
                $this->view->errors = $albumForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function uploadAction() {
         $id = $this->getRequest()->getParam("id");
         $this->view->id = $id;
        if ($this->_request->isXmlHttpRequest()) {

            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();
            //ajax call request
            if (isset($_FILES['files'])) {
                $status = array();
                foreach ($_FILES as $file) {
                    
                    $image_data = getimagesize($file["tmp_name"][0]);
                    if ($image_data == false) {
                        echo "File is not an image.";
                        exit;
                    }
                    $dir_name = APPLICATION_PATH . "/../public/upload/albums/$id/";
                    if(!file_exists ( $dir_name)){
                        mkdir($dir_name);
                    }
                    $name = time()."-".$file['name'][0];
                    if (move_uploaded_file($file["tmp_name"][0], $dir_name.  $name)) {
                        $status["files"][]["name"]= $file['name'][0]." is uploaded ";
                        $imageModel = new Administration_Model_Image();
                        $imageModel->addImage(array("albumId" => $id, "name" =>$name));
                    } else {
                        $status["files"][]["name"]= $file['name'][0]." is not uploaded ";
                    }
                }
            }
            
            return $this->_helper->json($status);
        }
    }

    public function editAction() {
        //Get instances
        $albumForm = new Administration_Form_Album();
        $albumModel = new Administration_Model_Album();

        $id = $this->getRequest()->getParam('id');
        $album = $albumModel->getAlbumById($id);

        //send values to view
        $this->view->post = "/administration/album/edit";
        $this->view->viewName = "Edit Album";

        
        if ($this->getRequest()->isPost()) {
            if ($albumForm->isValid($this->getRequest()->getParams())) {

                $result = $albumModel->editAlbum($albumForm->getValue("id"), $albumForm->getValues());

                $this->_flashMessenger->addMessage('Album edited successfully!');
                $this->redirect("/administration/album/list");
            } else {
                $this->view->errors = $albumForm->getMessages();
            }
        }

        if ($album) {
            $albumForm->populate($album[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no album with this id!');
            $this->redirect("/administration/album/list");
        }



        $this->view->form = $albumForm;
        $this->render("form");
    }

    public function deleteAction() {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $albumModel = new Administration_Model_Album();
        $result = $albumModel->deleteAlbum($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Album deleted successfully!');
            echo $result;
        }
    }

}
