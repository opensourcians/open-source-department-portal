<?php

/* default/layout/layout_1_col.tpl */
class __TwigTemplate_6bea84ad8980a19488b72bf19b9f2acfbea20e9df5e1dd96eccd4036edb1195d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("default/layout/main.tpl");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/layout/main.tpl";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "    ";
        // line 6
        echo "    ";
        if ((isset($context["plugin_main_top"]) ? $context["plugin_main_top"] : null)) {
            // line 7
            echo "        <div id=\"plugin_main_top\" class=\"span12\">
            ";
            // line 8
            echo (isset($context["plugin_main_top"]) ? $context["plugin_main_top"] : null);
            echo "
        </div>
    ";
        }
        // line 11
        echo "
    ";
        // line 13
        echo "    ";
        if ((isset($context["plugin_content_top"]) ? $context["plugin_content_top"] : null)) {
            // line 14
            echo "        <div id=\"plugin_content_top\" class=\"span12\">
            ";
            // line 15
            echo (isset($context["plugin_content_top"]) ? $context["plugin_content_top"] : null);
            echo "
        </div>
    ";
        }
        // line 18
        echo "
    <div class=\"span12\">
        ";
        // line 20
        $this->env->loadTemplate("default/layout/page_body.tpl")->display($context);
        // line 21
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 28
        echo "        &nbsp;
    </div>

    ";
        // line 32
        echo "    ";
        if ((isset($context["plugin_content_bottom"]) ? $context["plugin_content_bottom"] : null)) {
            // line 33
            echo "        <div id=\"plugin_content_bottom\" class=\"span12\">
            ";
            // line 34
            echo (isset($context["plugin_content_bottom"]) ? $context["plugin_content_bottom"] : null);
            echo "
        </div>
    ";
        }
        // line 37
        echo "
    ";
        // line 39
        echo "    ";
        if ((isset($context["plugin_main_bottom"]) ? $context["plugin_main_bottom"] : null)) {
            // line 40
            echo "        <div id=\"plugin_main_bottom\" class=\"span12\">
            ";
            // line 41
            echo (isset($context["plugin_main_bottom"]) ? $context["plugin_main_bottom"] : null);
            echo "
        </div>
    ";
        }
    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        // line 22
        echo "            ";
        if ((!(null === (isset($context["content"]) ? $context["content"] : null)))) {
            // line 23
            echo "                <section id=\"main_content\">
                ";
            // line 24
            echo (isset($context["content"]) ? $context["content"] : null);
            echo "
                </section>
            ";
        }
        // line 27
        echo "        ";
    }

    public function getTemplateName()
    {
        return "default/layout/layout_1_col.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 27,  113 => 24,  110 => 23,  107 => 22,  104 => 21,  96 => 41,  93 => 40,  90 => 39,  87 => 37,  81 => 34,  78 => 33,  75 => 32,  70 => 28,  67 => 21,  65 => 20,  61 => 18,  55 => 15,  52 => 14,  49 => 13,  46 => 11,  40 => 8,  37 => 7,  34 => 6,  32 => 5,  29 => 4,);
    }
}
