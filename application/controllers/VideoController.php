<?php

class VideoController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->redirect("/video/list");
    }

    public function listAction()
    {
        $videoModel = new Administration_Model_Video();
        $videos = $videoModel->listVideos();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($videos);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        
        $this->view->paginator = $paginator;
    }


}