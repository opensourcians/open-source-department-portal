<?php

class ForumController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $cat_model = new Administration_Model_Forumcategory();
        $this->view->cats = $cat_model->listForumcategory();
    }

    public function sectionAction()
    {
        // action body
        $section_model=new Administration_Model_Forumsection();
        $this->view->array = $section_model->getforumsectionByid($this->_request->getParam('id'));
        $this->view->sec_id=$this->_request->getParam('id');
        $topic_model=new Administration_Model_Forumtopic();
        $this->view->topics = $topic_model->listForumtopics($this->_request->getParam('id'));
    }

    public function topicAction()
    {
        // action body
        $topic_model=new Administration_Model_Forumtopic();
        $this->view->topic = $topic_model->viewTopic($this->_request->getParam('id'));
        $this->view->id = $this->_request->getParam('id');
    }

    public function addtopicAction()
    {
        // action body
    }


}





