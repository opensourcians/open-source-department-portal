<?php

class Administration_Model_Competitioncategory extends Zend_Db_Table_Abstract
{
        protected $_name="categoryCompetition";
	public function addCompetitioncategory($name)
	{
		$inserted = array('name'=>$name);
		$this->insert($inserted);
                
               
	}
	public function editCompetitioncategory($name,$id)
	{
		$updated = array('name'=>$name);
		$this->update($updated,"id = $id");
	}
	public function deleteCompetitioncategory($id)
	{
		$this->delete("id= $id");
	}
	public function listCompetitioncategory()
	{

		$select = $this->select();
		return $this->fetchAll($select)->toArray();
	}
        public function getcompetitioncategoryByid($id) {
            $select= $this->select()->where("id=$id");
            return $this->fetchAll($select)->toArray();
        }
}

