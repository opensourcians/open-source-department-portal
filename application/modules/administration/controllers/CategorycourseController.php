<?php

class Administration_CategorycourseController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function addAction()
    {
        $category=new Administration_Form_Category();
        $this->view->category=$category;
        if($this->_request->isPost()){
            $category_name=$this->_request->getParam('categoryname');
            $Categorycourse_model = new Administration_Model_Categorycourse();
            $Categorycourse_model->addCourseCategory($category_name);
            $this->redirect('/administration/categorycourse/index');
        }
        
    }

    public function deleteAction()
    {
         $id=$this->_request->getParam('id');
         $categorycourse_model=new Administration_Model_Categorycourse();
         $categorycourse_model->deleteCourseCategory($id);
         $this->redirect('/administration/categorycourse/list');
        // action body
    }

    public function editAction()
    {
        $category_model=new Administration_Model_Categorycourse();
        $categoryid=$this->getRequest()->getParam('id');
        if($this->_request->isPost()){
            $categoryname=$this->getRequest()->getParam('categoryname');
            $category_model->editCourseCategory($categoryid,$categoryname);
            $this->view->msg="Course Edited Sucessfully";
            $this->redirect('/administration/categorycourse/list');
        }
        $category=new Administration_Form_Category();
        $this->view->category=$category;
        $categorydata=$category_model->getCourseCategoryById($categoryid);
        $category->getElement("categoryname")->setValue($categorydata[0]['coursecategory_name']);
        $category->getElement("Add")->setValue("Edit Category");
        
        }

    public function listAction()
    {
         $categorycourse_model = new Administration_Model_Categorycourse();
         $categorycoursedata = $categorycourse_model->listCourseCategory();
         $this->view->categorydata=  $categorycoursedata;
        
    }


}









