<?php

class Administration_ContactcategoryController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        
    }

    public function addAction()
    {
        $categoryForm= new Administration_Form_ContactCategory();
        $this->view->form=$categoryForm;
        
        if($this->_request->isPost())
        {
            $catName=$this->_request->getParam('cont_cat');
            $categoryModel= new Administration_Model_Contactcategory();
            $categoryModel->createContactCategory($catName);
            $this->redirect('administration/contactcategory/list');
        }
    }

    public function listAction()
    {
        $categoryModel= new Administration_Model_Contactcategory();
        $this->view->contactcategories= $categoryModel->selectContactCategory();
    }

    public function editAction()
    {
        $catId= $this->_request->getParam('id');//catch id from url
        $catModel= new Administration_Model_Contactcategory();
        $catValue=$catModel->selectCategoryById($catId);//return array for selecting data of certain id
        
        $catName = array("cont_cat" => $catValue['cat_name']);        
        $categoryForm = new Administration_Form_ContactCategoryEdit(); 
        $this->view->formedit=$categoryForm->populate($catName); //view data of certain id in the form for editing
        
        if($this->_request->isPost())
            {
                $catName=$this->_request->getParam("cont_cat");
                   
                $catModel = new Administration_Model_Contactcategory();
                $catModel->editContactCategory($catId , $catName);
                $this->redirect("administration/contactcategory/list");
            }
    }

    public function deleteAction()
    {
        $id= $this->_request->getParam('id');//catch id from url
        $catModel= new Administration_Model_Contactcategory();
        $catModel->deleteContactCategory($id);
        $this->redirect("administration/contactcategory/list");
    }


}











