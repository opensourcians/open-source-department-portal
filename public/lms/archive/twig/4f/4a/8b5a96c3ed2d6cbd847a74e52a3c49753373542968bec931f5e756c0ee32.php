<?php

/* default/layout/layout_2_col.tpl */
class __TwigTemplate_4f4a8b5a96c3ed2d6cbd847a74e52a3c49753373542968bec931f5e756c0ee32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("default/layout/main.tpl");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'login_form' => array($this, 'block_login_form'),
            'page_body' => array($this, 'block_page_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "default/layout/main.tpl";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        // line 5
        echo "
    ";
        // line 7
        echo "    ";
        if ((isset($context["plugin_main_top"]) ? $context["plugin_main_top"] : null)) {
            // line 8
            echo "        <div id=\"plugin_main_top\" class=\"span12\">
            ";
            // line 9
            echo (isset($context["plugin_main_top"]) ? $context["plugin_main_top"] : null);
            echo "
        </div>
    ";
        }
        // line 12
        echo "
\t";
        // line 14
        echo "\t<div class=\"span3 menu-column\">
        ";
        // line 15
        if ((isset($context["plugin_menu_top"]) ? $context["plugin_menu_top"] : null)) {
            // line 16
            echo "            <div id=\"plugin_menu_top\">
                ";
            // line 17
            echo (isset($context["plugin_menu_top"]) ? $context["plugin_menu_top"] : null);
            echo "
            </div>
        ";
        }
        // line 20
        echo "
\t    ";
        // line 22
        echo "        ";
        $this->displayBlock('login_form', $context, $blocks);
        // line 27
        echo "
        <div class=\"block_user_info\">
\t\t";
        // line 30
        echo "        ";
        echo (isset($context["user_image_block"]) ? $context["user_image_block"] : null);
        echo "

        ";
        // line 33
        echo "\t\t";
        echo (isset($context["profile_block"]) ? $context["profile_block"] : null);
        echo "
        </div>
        <div class=\"block_tools_info\">
        ";
        // line 37
        echo "\t\t";
        echo (isset($context["course_block"]) ? $context["course_block"] : null);
        echo "

        ";
        // line 40
        echo "\t\t";
        echo (isset($context["teacher_block"]) ? $context["teacher_block"] : null);
        echo "
        </div>
        <div class=\"user_notification\">
\t\t";
        // line 44
        echo "\t\t";
        echo (isset($context["notice_block"]) ? $context["notice_block"] : null);
        echo "

        ";
        // line 47
        echo "\t\t";
        echo (isset($context["help_block"]) ? $context["help_block"] : null);
        echo "

\t\t";
        // line 50
        echo "\t\t";
        echo (isset($context["navigation_course_links"]) ? $context["navigation_course_links"] : null);
        echo "

\t\t";
        // line 53
        echo "\t\t";
        echo (isset($context["reservation_block"]) ? $context["reservation_block"] : null);
        echo "

\t\t";
        // line 56
        echo "\t\t";
        echo (isset($context["search_block"]) ? $context["search_block"] : null);
        echo "
        </div>
\t\t";
        // line 59
        echo "\t\t";
        echo (isset($context["classes_block"]) ? $context["classes_block"] : null);
        echo "

\t\t";
        // line 62
        echo "\t\t";
        echo (isset($context["skills_block"]) ? $context["skills_block"] : null);
        echo "

\t\t";
        // line 65
        echo "        ";
        // line 66
        echo "
        ";
        // line 67
        if ((isset($context["plugin_menu_bottom"]) ? $context["plugin_menu_bottom"] : null)) {
            // line 68
            echo "            <div id=\"plugin_menu_bottom\">
                ";
            // line 69
            echo (isset($context["plugin_menu_bottom"]) ? $context["plugin_menu_bottom"] : null);
            echo "
            </div>
        ";
        }
        // line 72
        echo "\t</div>

\t<div class=\"span9 content-column\">
        ";
        // line 76
        echo "        ";
        if ((isset($context["plugin_content_top"]) ? $context["plugin_content_top"] : null)) {
            // line 77
            echo "            <div id=\"plugin_content_top\">
                ";
            // line 78
            echo (isset($context["plugin_content_top"]) ? $context["plugin_content_top"] : null);
            echo "
            </div>
        ";
        }
        // line 81
        echo "
\t\t";
        // line 83
        echo "        ";
        if ((isset($context["home_page_block"]) ? $context["home_page_block"] : null)) {
            // line 84
            echo "            <section id=\"homepage\">
                <div class=\"row\">
                    <div class=\"span9\">
                    ";
            // line 87
            echo (isset($context["home_page_block"]) ? $context["home_page_block"] : null);
            echo "
                    </div>
                </div>
            </section>
        ";
        }
        // line 92
        echo "
\t\t";
        // line 94
        echo "\t\t";
        echo (isset($context["sniff_notification"]) ? $context["sniff_notification"] : null);
        echo "

        ";
        // line 96
        $this->displayBlock('page_body', $context, $blocks);
        // line 99
        echo "
        ";
        // line 101
        echo "        ";
        if ((isset($context["welcome_to_course_block"]) ? $context["welcome_to_course_block"] : null)) {
            // line 102
            echo "            <section id=\"welcome_to_course\">
            ";
            // line 103
            echo (isset($context["welcome_to_course_block"]) ? $context["welcome_to_course_block"] : null);
            echo "
            </section>
        ";
        }
        // line 106
        echo "
        ";
        // line 107
        $this->displayBlock('content', $context, $blocks);
        // line 114
        echo "
\t\t";
        // line 116
        echo "        ";
        if ((isset($context["announcements_block"]) ? $context["announcements_block"] : null)) {
            // line 117
            echo "            <section id=\"announcements\">
            ";
            // line 118
            echo (isset($context["announcements_block"]) ? $context["announcements_block"] : null);
            echo "
            </section>
        ";
        }
        // line 121
        echo "
        ";
        // line 123
        echo "        ";
        if ((isset($context["course_category_block"]) ? $context["course_category_block"] : null)) {
            // line 124
            echo "            <section id=\"course_category\">
                <div class=\"row\">
                    <div class=\"span9\">
                    ";
            // line 127
            echo (isset($context["course_category_block"]) ? $context["course_category_block"] : null);
            echo "
                    </div>
                </div>
            </section>
        ";
        }
        // line 132
        echo "
\t\t";
        // line 134
        echo "\t\t";
        $this->env->loadTemplate("default/layout/hot_courses.tpl")->display($context);
        // line 135
        echo "
        ";
        // line 137
        echo "        ";
        if ((isset($context["plugin_content_bottom"]) ? $context["plugin_content_bottom"] : null)) {
            // line 138
            echo "            <div id=\"plugin_content_bottom\">
                ";
            // line 139
            echo (isset($context["plugin_content_bottom"]) ? $context["plugin_content_bottom"] : null);
            echo "
            </div>
        ";
        }
        // line 142
        echo "        &nbsp;
\t</div>

    ";
        // line 146
        echo "    ";
        if ((isset($context["plugin_main_bottom"]) ? $context["plugin_main_bottom"] : null)) {
            // line 147
            echo "        <div id=\"plugin_main_bottom\" class=\"span12\">
            ";
            // line 148
            echo (isset($context["plugin_main_bottom"]) ? $context["plugin_main_bottom"] : null);
            echo "
        </div>
    ";
        }
        // line 151
        echo "
";
    }

    // line 22
    public function block_login_form($context, array $blocks = array())
    {
        // line 23
        echo "\t\t";
        if (($this->getAttribute((isset($context["_u"]) ? $context["_u"] : null), "logged", array()) == 0)) {
            // line 24
            echo "\t\t\t";
            $this->env->loadTemplate("default/layout/login_form.tpl")->display($context);
            // line 25
            echo "\t\t";
        }
        // line 26
        echo "        ";
    }

    // line 96
    public function block_page_body($context, array $blocks = array())
    {
        // line 97
        echo "        ";
        $this->env->loadTemplate("default/layout/page_body.tpl")->display($context);
        // line 98
        echo "        ";
    }

    // line 107
    public function block_content($context, array $blocks = array())
    {
        // line 108
        echo "        ";
        if ((!(null === (isset($context["content"]) ? $context["content"] : null)))) {
            // line 109
            echo "            <section id=\"main_content\">
                ";
            // line 110
            echo (isset($context["content"]) ? $context["content"] : null);
            echo "
            </section>
        ";
        }
        // line 113
        echo "        ";
    }

    public function getTemplateName()
    {
        return "default/layout/layout_2_col.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  347 => 113,  341 => 110,  338 => 109,  335 => 108,  332 => 107,  328 => 98,  325 => 97,  322 => 96,  318 => 26,  315 => 25,  312 => 24,  309 => 23,  306 => 22,  301 => 151,  295 => 148,  292 => 147,  289 => 146,  284 => 142,  278 => 139,  275 => 138,  272 => 137,  269 => 135,  266 => 134,  263 => 132,  255 => 127,  250 => 124,  247 => 123,  244 => 121,  238 => 118,  235 => 117,  232 => 116,  229 => 114,  227 => 107,  224 => 106,  218 => 103,  215 => 102,  212 => 101,  209 => 99,  207 => 96,  201 => 94,  198 => 92,  190 => 87,  185 => 84,  182 => 83,  179 => 81,  173 => 78,  170 => 77,  167 => 76,  162 => 72,  156 => 69,  153 => 68,  151 => 67,  148 => 66,  146 => 65,  140 => 62,  134 => 59,  128 => 56,  122 => 53,  116 => 50,  110 => 47,  104 => 44,  97 => 40,  91 => 37,  84 => 33,  78 => 30,  74 => 27,  71 => 22,  68 => 20,  62 => 17,  59 => 16,  57 => 15,  54 => 14,  51 => 12,  45 => 9,  42 => 8,  39 => 7,  36 => 5,  34 => 4,  31 => 3,);
    }
}
