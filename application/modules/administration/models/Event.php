<?php

class Administration_Model_Event  extends Zend_Db_Table_Abstract
{
    protected $_name="event"; 
    
    function listEvents(){
        $select = $this->select("*")->setIntegrityCheck(FALSE)
                ->joinLeft("user", "event.createdBy=user.id",array("user.id as uid","firstName","lastName"));
        
        return $this->fetchAll($select)->toArray();
    }
    
    function addEvent($eventInfo) {
        
        //created by
        $auth = Zend_Auth::getInstance();
        $userInfo = $auth->getIdentity(); 
        
        $row = $this->createRow();
        $row->title = $eventInfo['title'];
        $row->fromDate = $eventInfo['fromDate'];
        $row->toDate = $eventInfo['toDate'];
        $row->description = $eventInfo['description'];
        $row->place = $eventInfo['place'];
        $row->createdBy = $userInfo->id;
        
        return $row->save();
    }
    
    function getEventById($eventId) {
        
        return $this->find($eventId)->toArray();
    }
    
    function editEvent($eventId, $eventInfo) {
        
        return $this->update($eventInfo, "id = $eventId");

    }
    
    
    function deleteEvent($eventId) {
        return $this->delete("id = $eventId");
    }
    
    function getMonthEvents($month,$year){
        
        $dbAdapter = $this->getDefaultAdapter();
        $result = $dbAdapter->query("SELECT * FROM  `event` WHERE CONCAT( MONTH(  `fromDate` ) , YEAR(  `fromDate` ) ) = '".$month.$year."'")->fetchAll();
        return $result;
        
    }
    

}

