<?php

class Administration_Model_Poll extends Zend_Db_Table_Abstract
{
    protected $_name = 'poll';
    protected $_dependentTables = array('Administration_Model_PollChoices');
    
    function listPolls(){
        return $this->fetchAll()->toArray();
    }
    
    function addPoll($pollQuestion){
        $row = $this->createRow();
        $row->pollQuestion = $pollQuestion;
        return $row->save();
    }
    
    function editPoll($pollId,$pollQuestion){
        
        return $this->update(array("pollQuestion"=>"$pollQuestion"), "id = $pollId");
    }
    
    function getPollById($pollId){
        return $this->find($pollId)->toArray();
    }
    
    function deletePoll($pollId) {
        return $this->delete("id = $pollId");      
    }
    
    function getPollForDisplayById($pollId){
        $pollRow = $this->find($pollId);
        $poll = $pollRow->current();
        
        $choices = $poll->findDependentRowset("Administration_Model_PollChoices");
        
        $pollChoicesModel = new Administration_Model_PollChoices();
        $choicesSum = $pollChoicesModel->sumPollChoices($pollId);
        
        return array($poll->toArray(),$choices->toArray(),$choicesSum);
        
    }


}

