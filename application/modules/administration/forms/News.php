<?php

class Administration_Form_News extends Zend_Form {

    public function init() {

        $this->setMethod('post');
        $this->setName('news');
        $this->setEnctype('multipart/form-data');
        
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $categoryId = new Zend_Form_Element_Select("categoryId");
        $categoryId->setRequired();
        $categoryId->addMultiOption("","Select News Category");
        $categoryId->setAttrib("class", "form-control");
        $categoryId->setDecorators(array('ViewHelper'));
        $newsCategoryModel = new Administration_Model_NewsCategory();
        $newsCategories = $newsCategoryModel->listNewsCategories();
        foreach($newsCategories as $newsCategory){
            $categoryId->addMultiOption($newsCategory['id'],$newsCategory['name']);
        }

        $title = new Zend_Form_Element_Text("title");
        $title->addFilter(new Zend_Filter_StripTags);
        $title->setAttrib("class", "form-control");
        $title->setAttrib("placeholder", "Enter Title");
        $title->setAttrib("id", "title");
        $title->setRequired();
        $title->removeDecorator('Label');
        $title->setDecorators(array('ViewHelper'));

        $body = new Zend_Form_Element_Textarea("body");
        $body->setRequired();
        $body->setAttrib("class", "form-control");
        $body->setAttrib("id", "ckeditor_full");
        $body->setDecorators(array('ViewHelper'));
        

        $sliderImage = new Zend_Form_Element_File('sliderImage');
        $sliderImage->setDestination(APPLICATION_PATH . "/../public/upload/news");
        $sliderImage->addValidator('Count', false, 1);
        $sliderImage->addValidator('Size', false, 2002400);
        $sliderImage->addValidator('Extension', false, 'jpg,png,gif');
        $sliderImage->setAttrib("class", "form-control");
        $sliderImage->setAttrib("id", "sliderImage");
        $sliderImage->setRequired();
        $sliderImage->removeDecorator('Label');
        $sliderImage->removeDecorator('Error');

        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");

        $this->addElements(array($id,$categoryId,$title, $body, $sliderImage, $submit));
    }

}
