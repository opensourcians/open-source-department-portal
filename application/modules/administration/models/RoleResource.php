<?php

class Administration_Model_RoleResource extends Zend_Db_Table_Abstract
{
    protected $_name = "roleResource";
    
    function listAll(){
        $select = $this->select("*")->setIntegrityCheck(FALSE)->join("role", "roleResource.roleId = role.id")
                ->join("resource", "roleResource.resourceId = resource.id");
        
        return $this->fetchAll($select)->toArray();
    }
    
    function addRolePermissions($roleId, $resourcesArray){
        if(is_array($resourcesArray)){
            foreach ($resourcesArray as $resourceId){
                $row = $this->createRow();
                $row->roleId = $roleId;
                $row->resourceId = $resourceId;
                $row->save();
            }
        }
        
    }
    
    function getRolePermissions($roleId){
        
        $select = $this->select()->setIntegrityCheck(FALSE)->from("roleResource", "resourceId")->where("roleId = $roleId");
        return $this->fetchAll($select)->toArray();
        
    }
    
    function editRolePermissions($roleId, $resourcesArray){
        $this->delete("roleId = $roleId");
        if(is_array($resourcesArray)){
            foreach ($resourcesArray as $resourceId){
                $row = $this->createRow();
                $row->roleId = $roleId;
                $row->resourceId = $resourceId;
                $row->save();
            }
        }
    }
            
    function listRolesPermissions(){
        $select = $this->select("*")->setIntegrityCheck(FALSE)->joinRight("role", "roleResource.roleId = role.id")
                ->joinRight("resource", "roleResource.resourceId = resource.id");
        
        return $this->fetchAll($select)->toArray();
    }
    
}

