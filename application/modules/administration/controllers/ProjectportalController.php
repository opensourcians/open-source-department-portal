<?php

class Administration_ProjectportalController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function listAction()
    {
        // action body
        $list= new Administration_Model_Projectportal();
        $this->view->list=$list->listProject();
    }

    public function addAction()
    {
        // action body
        if($this->_request->isPost()){
            $pname = $this->_request->getParam("pname");
            $track = $this->_request->getParam("track");
            $intake = $this->_request->getParam("intake");
            $desc = $this->_request->getParam("desc");
            $doc = $this->_request->getParam("doc");
            $his = $this->_request->getParam("his");
            $img = $this->_request->getParam("img");
            $id = $this->_request->getParam("id");
            $flag = $this->_request->getParam("flag");
            $tag2 = $this->_request->getParam("tag2");
            $tag1 = $this->_request->getParam("tag1");
            /*$this->view->user=$name;
            $this->view->mail=$email;
            $this->view->passwd=$passwd;*/
            $add = new Administration_Model_Projectportal();
            $add->addProject($id,$pname,$track,$intake,$desc,$his,$doc,$img,$flag);
            $add2 = new Administration_Model_Projecttag();
            $add2->addTag($tag1,$tag2);
            $this->redirect("/administration/projectportal/list");
        }
    }

    public function editAction()
    {
        // action body
        $id=$this->_request->getparam('id');
        $projectform = new Administration_Form_Project();
        $list = new Administration_Model_Projectportal();
        $data=$list->listProject();
        $projectform->populate($data[$id-1]);
        $data=$list->listTag();
        $projectform->populate($data[$id-1]);
        $this->view->form=$projectform;
        $this->view->id=$id;
        if($this->_request->isPost()){
            $pname = $this->_request->getParam("pname");
            $tname = $this->_request->getParam("tname");
            $tpid = $this->_request->getParam("tpid");
            $pdesc = $this->_request->getParam("pdesc");
            $phis = $this->_request->getParam("phis");
            $pdoc = $this->_request->getParam("pdoc");
            $pimage = $this->_request->getParam("pimage");
            $tags = $this->_request->getParam("tag");
            //$id = $this->_request->getParam("id");
          /*  $this->view->user=$name;
            $this->view->mail=$email;
            $this->view->passwd=$passwd;
            $this->view->id-$id;*/
            $edit = new Administration_Model_Projectportal();
            $edit->editProject($pname,$tname,$tpid,$pdesc,$phis,$pdoc,$pimage,$id);
            $edit = new Administration_Model_Projecttag();
            $edit->editTag($tags,$id);
            $this->redirect("/administration/projectportal/list");
        }
    }

    public function deleteAction()
    {
        // action body
        $id=$this->_request->getparam('id');
        $delete1 = new Administration_Model_Projectportal();
        $delete1->deleteProject($id);
        $delete2 = new Administration_Model_Projecttag();
        $delete2->deleteTag($id);
        $this->redirect("/administration/projectportal/list");
    }
}

















