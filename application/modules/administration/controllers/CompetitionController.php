<?php

class Administration_CompetitionController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        
    }

    public function addAction()
    {
        $list = new Administration_Model_Competitioncategory();
        $this->view->data = $list->listCompetitioncategory();
        //var_dump($this->view->data);
        if ($this->_request->isPost())
        {
            $comp = new Administration_Model_Competition();
            $id = $comp->addCompetition($this->_request->getParam("competitionName"),
            $this->_request->getParam("categoryName"));
            $this->redirect("/administration/question/add/id/".$id);
        }
    }


}



