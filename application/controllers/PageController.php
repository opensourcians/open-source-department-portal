<?php

class PageController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->redirect("/");
    }

    public function viewAction()
    {
        //Get instances
        $pageModel = new Administration_Model_Page();
        
        //get page id from request
        $id = $this->getRequest()->getParam('id');
        
        //get page information
        $page = $pageModel->getPageByID($id);
        if($page){        
            $this->view->page = $page[0];
        } else {
            $this->redirect("/");
        }
    }


}



