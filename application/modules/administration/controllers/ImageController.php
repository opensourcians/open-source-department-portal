<?php

class Administration_ImageController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/image/list");
      
    }

    public function listAction()
    {
        $imageModel = new Administration_Model_Image();
        if($this->getRequest()->getParam("albumId")){
            $this->view->albumId = $this->getRequest()->getParam("albumId");
            $images = $imageModel->listImages($this->getRequest()->getParam("albumId"));
        } else {
            $images = $imageModel->listImages();
        }
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($images);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
        
    }

    public function addAction()
    {
        $imageForm = new Administration_Form_Image();
        if($this->getRequest()->getParam("albumId")){
            $imageForm->getElement("albumId")->setValue($this->getRequest()->getParam("albumId"));
        }

        $this->view->form = $imageForm;
        $this->view->post = "/administration/image/add";
        $this->view->viewName = "Add Image";


        if ($this->getRequest()->isPost()) {
            $imageForm->getElement("name")->setDestination(APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("albumId"));
            
            if ($imageForm->isValid($this->getRequest()->getParams())) {
                $imageModel = new Administration_Model_Image();
                $imageInfo = $imageForm->getValues();
                rename(APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("albumId")."/".$imageForm->getValue("name"),\
                            APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("albumId")."/".time()."-".$imageForm->getValue("name"));
                $imageInfo['name'] = time()."-".$imageInfo['name'];
                $albumId = $imageModel->addImage($imageInfo);

                if ($albumId) {
                    
                    $this->_flashMessenger->addMessage('Image added successfully!');
                    $this->redirect("/administration/image/list/id/".$imageForm->getValue("albumId"));
                }
            } else {
                $this->view->errors = $imageForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction()
    {
         //Get instances
        $imageForm = new Administration_Form_Image();
        $imageModel = new Administration_Model_Image();


        //get user id from request
        $id = $this->getRequest()->getParam('id');

        //get user information
        $image = $imageModel->getImageById($id);

        //send values to view
        $this->view->post = "/administration/image/edit";
        $this->view->viewName = "Edit Image";
        
        //Not required to modify image on editing
        $name = $imageForm->getElement("name");
        
        $name->setRequired(FALSE);
        
        if ($this->getRequest()->isPost()) {
           
            $name->setDestination(APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("albumId"));
            
                
            if ($imageForm->isValid($this->getRequest()->getParams())) {
                
                $imageInfo = $imageForm->getValues();
                if(!empty($imageInfo['name'])){
                    rename(APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("albumId")."/".$imageForm->getValue("name"),\
                            APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("albumId")."/".time()."-".$imageForm->getValue("name"));
                $imageInfo['name'] = time()."-".$imageInfo['name'];
                }
                if($imageInfo['albumId'] != $imageInfo['album']){
                    rename(APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("album")."/".$this->getRequest()->getParam("imageName"),\
                            APPLICATION_PATH . "/../public/upload/albums/".$this->getRequest()->getParam("albumId")."/".$this->getRequest()->getParam("imageName"));
                }
                //remove temp values of old album & image Name
                unset($imageInfo['imageName'], $imageInfo['album']);
                
                $result = $imageModel->editImage($imageForm->getValue("id"), $imageInfo);
                
                $this->_flashMessenger->addMessage('Image edited successfully!');
                $this->redirect("/administration/image/list/id/".$imageForm->getValue("albumId"));
            } else {
                $this->view->errors = $imageForm->getMessages();
            }
        }

        if ($image) {
            //This values is used when changing the album, so we need to move the photo
            $image[0]['album'] = $image[0]['albumId'];
            $image[0]['imageName'] = $image[0]['name'];
            $imageForm->populate($image[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no image with this id!');
            $this->redirect("/administration/image/list");
        }

        

        $this->view->form = $imageForm;
        $this->render("form");
    }
    public function coverAction(){
        
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        
        $imageId = $this->getRequest()->getParam("id");
        $albumId = $this->getRequest()->getParam("albumId");
        
        $imageModel = new Administration_Model_Image();
        
        $result = $imageModel->setAlbumCover($imageId, $albumId);
        
        if($result){
            $this->_flashMessenger->addMessage('Cover photo is changed successfully');
            $this->redirect("/administration/image/list/albumId/$albumId");
        }else{
            $this->_flashMessenger->addMessage('Sorry!, but there is no image and/or album with this id!');
            $this->redirect("/administration/image/list");
        }
        
        
        
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $imageModel = new Administration_Model_Image();
        $result = $imageModel->deleteImage($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Image deleted successfully!');
            echo $result;
        }
    }


}

