<?php

class Administration_Form_Attendance extends Zend_Form {

    public function init() {

        $this->setMethod('post');
        $this->setName('news');
        
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $branchId = new Zend_Form_Element_Select("branchId");
        $branchId->setRequired();
        $branchId->addMultiOption("","Select Branch");
        $branchId->setAttrib("class", "form-control");
        $branchId->setAttrib("onchange", "this.form.submit()");
        $branchId->setDecorators(array('ViewHelper'));
        $branchModel = new Administration_Model_Branch();
        $branches = $branchModel->listBranches();
        foreach($branches as $branch){
            $branchId->addMultiOption($branch['id'],$branch['name']);
        }

        $trackId = new Zend_Form_Element_Select("trackId");
        $trackId->setRequired();
        $trackId->addMultiOption("","Select Track");
        $trackId->setAttrib("class", "form-control");
        $trackId->setAttrib("onchange", "this.form.submit()");
        $trackId->setDecorators(array('ViewHelper'));
        $trackModel = new Administration_Model_Track();
        $tracks = $trackModel->listTracks();
        foreach($tracks as $track){
            $trackId->addMultiOption($track['id'],$track['name']);
        }

        $att_date = new Zend_Form_Element_Text("att_date");
        $att_date->addFilter(new Zend_Filter_StripTags);
        $att_date->setRequired();
        $att_date->setAttrib("class", "form-control");
        $att_date->setAttrib("placeholder", "Enter Date");
        $att_date->setAttrib("id", "datepicker");
        $att_date->setValue("2015-05-28");
        $att_date->removeDecorator('Label');
        $att_date->setDecorators(array('ViewHelper'));

        

        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");

        $this->addElements(array($id, $branchId, $trackId, $att_date, $submit));
    }

}
