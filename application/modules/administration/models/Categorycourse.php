<?php

class Administration_Model_Categorycourse extends Zend_Db_Table_Abstract
{
    protected  $_name='coursecategory';
    function listAll(){ //for drop down menu
         $select  = $this->_db->select()->from($this->_name,array('key' => 'coursecategory_id','value' => 'coursecategory_name'));
         $result = $this->getAdapter()->fetchAll($select);
         return $result;
    }
    function addCourseCategory($coursecategory_name){
        $row = $this->createRow();
        $row->coursecategory_name =$coursecategory_name;
        return $row->save();   
    }
 
     function getCourseCategoryById($id){
        $sql=$this->select()->where("coursecategory_id=$id");
        return $this->fetchAll($sql)->toArray(); 
    }
    function deleteCourseCategory($id){
        $this->delete("coursecategory_id=$id");
    }
    
    function listCourseCategory(){
        return $this->fetchAll()->toArray();
    }
    function editCourseCategory($id,$coursecategory_name){
        $coursecategorydata=array('coursecategory_id'=>$id,'coursecategory_name'=>$coursecategory_name);
        $this->update($coursecategorydata,"coursecategory_id=$id" );
        
    }


}

