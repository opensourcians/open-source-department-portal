<?php

/* default/layout/show_header.tpl */
class __TwigTemplate_ff49c596472fccc9c8f074f8259e2301ffd1e05c9ed7e22f9d99d99826a26c1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("default/layout/main_header.tpl")->display($context);
        // line 8
        if (((isset($context["show_header"]) ? $context["show_header"] : null) == true)) {
            // line 9
            echo "    ";
            if ((!(null === (isset($context["plugin_content_top"]) ? $context["plugin_content_top"] : null)))) {
                // line 10
                echo "        <div id=\"plugin_content_top\" class=\"span12\">
            ";
                // line 11
                echo (isset($context["plugin_content_top"]) ? $context["plugin_content_top"] : null);
                echo "
        </div>
    ";
            }
            // line 14
            echo "    <div class=\"span12\">
        ";
            // line 15
            $this->env->loadTemplate("default/layout/page_body.tpl")->display($context);
            // line 16
            echo "        <section id=\"main_content\">
";
        }
    }

    public function getTemplateName()
    {
        return "default/layout/show_header.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 16,  38 => 15,  35 => 14,  29 => 11,  26 => 10,  23 => 9,  21 => 8,  19 => 1,);
    }
}
