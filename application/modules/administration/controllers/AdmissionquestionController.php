<?php

class Administration_AdmissionquestionController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/admissionquestion/list");
      
    }

    public function listAction()
    {
        $admissionQuestionModel = new Administration_Model_AdmissionQuestion();
        if($this->getRequest()->getParam("categoryId")){
            $admissionQuestion = $admissionQuestionModel->listAdmissionQuestions($this->getRequest()->getParam("categoryId"));
        } else {
            $admissionQuestion = $admissionQuestionModel->listAdmissionQuestions();
        }
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($admissionQuestion);
        $paginator->setItemCountPerPage(25);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
        
    }

    public function addAction()
    {
        $admissionQuestionForm = new Administration_Form_AdmissionQuestion();


        $this->view->form = $admissionQuestionForm;
        $this->view->post = "/administration/admissionquestion/add";
        $this->view->viewName = "Add Question";


        if ($this->getRequest()->isPost()) {
            if ($admissionQuestionForm->isValid($this->getRequest()->getParams())) {
                $admissionQuestionModel = new Administration_Model_AdmissionQuestion();

                $questionId = $admissionQuestionModel->addAdmissionQuestion($admissionQuestionForm->getValues());

                if ($questionId) {
                    $this->_flashMessenger->addMessage('Question added successfully!');
                    $this->redirect("/administration/admissionquestion/list");
                }
            } else {
                $this->view->errors = $admissionQuestionForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction()
    {
         //Get instances
        $admissionQuestionForm = new Administration_Form_AdmissionQuestion();
        $admissionQuestionModel = new Administration_Model_AdmissionQuestion();


        //get question id from request
        $id = $this->getRequest()->getParam('id');

        //get question information
        $question = $admissionQuestionModel->getAdmissionQuestionById($id);

        //send values to view
        $this->view->post = "/administration/admissionquestion/edit";
        $this->view->viewName = "Edit Question";
        

        if ($this->getRequest()->isPost()) {
            if ($admissionQuestionForm->isValid($this->getRequest()->getParams())) {

                $result = $admissionQuestionModel->editAdmissionQuestion($admissionQuestionForm->getValue("id"), $admissionQuestionForm->getValues());

                $this->_flashMessenger->addMessage('Question edited successfully!');
                $this->redirect("/administration/admissionquestion/list");
            } else {
                $this->view->errors = $admissionQuestionForm->getMessages();
            }
        }

        if ($question) {
            $admissionQuestionForm->populate($question[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no question with this id!');
            $this->redirect("/administration/admissionquestion/list");
        }

        

        $this->view->form = $admissionQuestionForm;
        $this->render("form");
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $admissionQuestionModel = new Administration_Model_AdmissionQuestion();
        $result = $admissionQuestionModel->deleteAdmissionQuestion($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Question deleted successfully!');
            echo $result;
        }
    }


}









