<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        //get all intances
        $albumModel = new Administration_Model_Album();
        $albums = $albumModel->getAlbumsForDisplay();
        
        
        $mainModel = new Administration_Model_Main();
        $settings = $mainModel->getSettings();
        
        if($settings[0]['articlesNo']){
        $newsModel = new Administration_Model_News();
        $newsArticles = $newsModel->getArticlesForDisplay($settings[0]['articlesNo']? $settings[0]['articlesNo']:5);
        
        if ($settings[0]['poll']){
        $pollModel = new Administration_Model_Poll();
        $poll = $pollModel->getPollForDisplayById($settings[0]['poll']);
        }
        
        if ($settings[0]['slider']){
        $mainSlider = $albumModel->getAlbumForDisplay($settings[0]['slider']); 
        
        }
        
        if ($settings[0]['services']){
        $services = $albumModel->getAlbumForDisplay($settings[0]['services']); 
        
        }
        
        if ($settings[0]['video']){
             $videosModel = new Administration_Model_Video();
        $video = $videosModel->getVideoById($settings[0]['video']); 
        
        }
        
        if ($settings[0]['technologies']){
        $technologies = $albumModel->getAlbumForDisplay($settings[0]['technologies']); 
        
        }
        
        }        
        //send to view
        $this->view->settings = $settings;
        $this->view->albums = $albums;
        $this->view->mainSlider = $mainSlider;
        $this->view->services = $services;
        $this->view->articles = $newsArticles;
        $this->view->technologies = $technologies; 
        $this->view->poll = $poll;
        $this->view->video = $video;
        
        
        
    }


}

