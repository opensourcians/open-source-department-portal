<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html lang="{{ document_language }}" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html lang="{{ document_language }}" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html lang="{{ document_language }}" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="{{ document_language }}" class="no-js"> <!--<![endif]-->
<head>
    {% block head %}
    {% include "default/layout/head.tpl" %}
    {% endblock %}
</head>
<body dir="{{ text_direction }}" class="{{ section_name }} {{ login_class }} gradient">
<noscript>{{ "NoJavascript"|get_lang }}</noscript>
<div class="container">
    <div class="row clearfix" id="header">
        <div class="col-md-12 column" id="social">
            <a href="/?>"><img class="visible-lg" src="/images/logo.png" id="logo"></a>

            <div id="socialText">Keep in touch</div>
            <ul>
                <li><a target="_blank" href="#" class="facebook"></a></li>
                <li><a target="_blank" href="#" class="google"></a></li>
                <li><a target="_blank" href="#" class="twitter"></a></li>
                <li><a target="_blank" href="#" class="linkedin"></a></li>
            </ul>
            <div id="colorText" class="visible-lg">Change Color</div>
            <ul id="color" class="visible-lg">
                <li><a class="red" onClick="page_style('red');"></a></li>
                <li><a class="blue" onClick="page_style('blue');"></a></li>
                <li><a class="green" onClick="page_style('green');"></a></li>
                <li><a class="yellow" onClick="page_style('yellow');"></a></li>
            </ul>
        </div>
    </div>
    <div class="row clearfix" id="nav">
        <div class="col-md-12 column">
            <ul class="nav nav-pills">
                {% for item in menu_items %}
                    {% if item.sub_items is defined %}
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{item.label}}<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                {% for sub_item in item.sub_items %}
                                <li>
                                    <a href="http://52.25.251.24/{{sub_item.href}}">{{sub_item.label}}</a>
                                </li>
                                {% endfor %}
                            </ul>
                        </li>
                    {% else %}
                        <li><a href="http://52.25.251.24/{{ item.href }}">{{ item.label }}</a></li>
                    {% endif%}
                {% endfor %}
                <ul class="navbar-right">
                    <li id="menuWelcome">
                        <img class="img-circle" width="32px" height="32px"
                             src="{{ _u.avatar_small }}"/>
                        Welcome <span class='label label-default'>{{ _u.complete_name }}</span>
                        <a href="{{logout_link}}">
                            <span title="Logout" id="logoutIcon" class="glyphicon glyphicon-off"></span>
                        </a>
                    </li>
                </ul>
            </ul>
        </div>
    </div>
    <!-- end nav -->
    {# breadcrumb #}
    {% block breadcrumb %}
    {{ breadcrumb }}
    {% endblock %}

    <div class="fill">
