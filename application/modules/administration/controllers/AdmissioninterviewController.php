<?php

class Administration_AdmissioninterviewController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        // action body
    }
    
    public function startAction()
    {
        $applicantNamespace = new Zend_Session_Namespace('Applicant');
        
        if(!isset($applicantNamespace->id)){
             $this->_flashMessenger->addMessage('Sorry!, You have to add an applicant first!');
            $this->redirect("/administration/applicant/list");
        }
        
        $this->view->applicantName = $applicantNamespace->name;
        $this->view->applicantCode = $applicantNamespace->code;
        $admissionCategoryModel = new Administration_Model_AdmissionCategory();
        $this->view->admissionCategories = $admissionCategoryModel->listAdmissionCategories();
        
        $admissionLevelModel = new Administration_Model_AdmissionQuestionLevel();
        $this->view->admissionLevels = $admissionLevelModel->listAdmissionQuestionLevels();
        
        
    }
    public function displayAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        
        $categotyId = $this->getRequest()->getParam("categotyId");
        $levelId = $this->getRequest()->getParam("levelId");
        
        $admissionQuestionModel = new Administration_Model_AdmissionQuestion();
        return $this->_helper->json($admissionQuestionModel->getRandomQuestion($categotyId,$levelId));
        
    }
    
    public function saveAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        
        $applicantNamespace = new Zend_Session_Namespace('Applicant');
        if(isset($applicantNamespace->id)){
            $applicantId = $applicantNamespace->id;
        }
        $questionId = $this->getRequest()->getParam("questionId");
        $result = $this->getRequest()->getParam("result");
        
        $applicantResultModel = new Administration_Model_ApplicantResult();
        return $applicantResultModel->saveApplicantResult($applicantId, $questionId, $result);
    }
    
    public function endAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        
        $applicantNamespace = new Zend_Session_Namespace('Applicant');
        if(!isset($applicantNamespace->id)){
             $this->_flashMessenger->addMessage('Sorry!, You have to add an applicant first!');
            $this->redirect("/administration/applicant/list");
        } else {
            $applicantId = $applicantNamespace->id;
        }
        
        $applicantModel = new Administration_Model_Applicant();
        $interviewSecondsNo = $this->getRequest()->getParam("interviewSecondsNo");
        $applicantInfo['interviewTime'] = gmdate("H:i:s", $interviewSecondsNo);
        
        $applicantResultModel = new Administration_Model_ApplicantResult();
        $questionsNo = $applicantResultModel->getApplicantTotalQuestionsNumber($applicantId);
        $applicantInfo['questionsNo'] = $questionsNo['qNo'];
        
        $totalGrades = $applicantResultModel->getApplicantTotalGrade($applicantId);
        $applicantInfo['totalGrades'] = $totalGrades['tGrade'];
        
        $applicantInfo['precentage'] = $applicantResultModel->getApplicantResultPrecentage($applicantInfo['questionsNo'], $applicantInfo['totalGrades']);
        $update =  $applicantModel->editApplicant($applicantId, $applicantInfo);
        
        if($update){
            $this->_helper->json(array('id'=>$applicantId));
        } else {
            echo 'error';
        }
        
    }
    
    public function resultAction(){
        
        $applicantNamespace = new Zend_Session_Namespace('Applicant');
        
        if(isset($applicantNamespace->id)){
            Zend_Session::namespaceIsset('Applicant');
        }
        
        $applicantId = $this->getRequest()->getParam("applicantId");
        if($applicantId){
            $applicantModel = new Administration_Model_Applicant();
            $this->view->applicant = $applicantModel->getApplicantById($applicantId); 
        } else {
            $this->_flashMessenger->addMessage('Sorry!, You have to select an applicant first!');
            $this->redirect("/administration/applicant/list");
        }
        $applicantResultModel = new Administration_Model_ApplicantResult();
        $this->view->categories = $applicantResultModel->getApplicantQuestionsCategories($applicantId);
        
        $this->view->levels = $applicantResultModel->getApplicantQuestionsLevels($applicantId);
        
        $this->view->grades = $applicantResultModel->getApplicantGradesCategories($applicantId);
        
            
    }


}

