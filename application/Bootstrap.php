<?php

/**
 * @author Islam Askar <isalah@iti.gov.eg>
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    /**
     * This function is used to intialise the layout and view placeholders
     * @return void
     *
     */
    protected function _initAutoLoad() {
        Zend_Loader_Autoloader::getInstance()->registerNamespace('OSD_');
        Zend_Loader_Autoloader::getInstance()->registerNamespace('Facebook_');
    }

    protected function _initRoutes() {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        include APPLICATION_PATH . "/configs/routes.php";
    }

    protected function _initRequest() {

        $this->bootstrap('FrontController');

        $front = $this->getResource('FrontController');
        $request = new Zend_Controller_Request_Http();

        $front->setRequest($request);
    }

    protected function _initPlaceholders() {
        $this->bootstrap('View');
        $view = $this->getResource('View');
        $view->doctype('XHTML1_STRICT');
        $view->setEncoding('UTF-8');
        //Meta
        $view->headMeta()->appendName('keywords', 'ITI, Information, Technology, Institute, Open, Source');
        $view->headMeta()->appendName('description', 'Open Source Department is one of Information Technology Institute departments intiated on 2009 (Intake 30)');
        $view->headMeta()->appendHttpEquiv('viewport', 'width=device-width, initial-scale=1.0');
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
        // Set the initial title and separator:
        $view->headTitle('Open Source Department Website')
                ->setSeparator(' :: ');

        // Set the initial stylesheet:
        $view->headLink()->appendStylesheet($this->view->baseUrl() . '/css/bootstrap.min.css');



        // Set the initial JS to load:
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/jquery-2.1.0.min.js');
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/bootstrap.min.js');
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/ckeditor/ckeditor.js');
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/ckeditor/adapters/jquery.js');
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/ckfinder/ckfinder.js');
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/jquery.ui.widget.js');
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/jquery.blueimp-gallery.min.js');
        $view->headScript()->appendFile($this->view->baseUrl() . '/js/bootstrap-image-gallery.min.js');
    }

    //initialize navigation
    protected function _initNavigation() {
        $this->bootstrap('View');
        $view = $this->getResource('View');
        $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml', 'nav');
        try {
            $navigation = new Zend_Navigation($config);
        } catch (Zend_Exception $e) {
            $navigation = new Zend_Navigation(array(
                array(
                    'label' => 'Home',
                    'contoller' => 'index',
                    'action' => 'index',
            )));
        }

        $view->navigation($navigation);
    }

    //Load Plugins
    protected function _initPlugins() {

        $front = Zend_Controller_Front::getInstance();
        $front->registerPlugin(new Application_Plugin_ModuleLayout());
        $front->registerPlugin(new OSD_Controller_Plugin_ACL());
        $front->registerPlugin(new Application_Plugin_Authorize());
    }

    //To activate session
    protected function _initSession() {
        $this->bootstrap('Db');
        $db = $this->getResource('Db');

        $config = array(
            'name' => 'session',
            'primary' => 'id',
            'modifiedColumn' => 'modified',
            'dataColumn' => 'data',
            'lifetimeColumn' => 'lifetime',
            'db' => $db,
        );

        
        #ini_set('session.gc_maxlifetime', 45);  // set session max lifetime to 45 seconds
        ini_set('session.gc_divisor', 1);       // 100% chance of running GC

        //create your Zend_Session_SaveHandler_DbTable and
        //set the save handler for Zend_Session
        Zend_Session::setSaveHandler(new Zend_Session_SaveHandler_DbTable($config));

        Zend_Session::start();

        //init Time Settings
        date_default_timezone_set('Africa/Cairo');
    }

}
