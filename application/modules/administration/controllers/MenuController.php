<?php

class Administration_MenuController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $pagesModel = new Administration_Model_Page();
        $pages = $pagesModel->listPages();
        $this->view->pages = $pages;
        
        $newsModel = new Administration_Model_News();
        $articles = $newsModel->listImages();
        $this->view->articles = $articles;
        
        $newsCategoriesModel = new Administration_Model_NewsCategory();
        $newsCategories = $newsCategoriesModel->listNewsCategories();
        $this->view->newsCategories = $newsCategories;
                
    }
    
    public function saveAction(){
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get parameters
        $jsonObj = $this->getRequest()->getParam("json");
        $menuHTML = $this->getRequest()->getParam("html");
        
        $menuModel = new Administration_Model_Menu();
        $menuModel->saveMenuAsHTML($menuHTML);
        return $menuModel->saveMenuAsXML($jsonObj);
        
    }
    
    public function loadAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        
        $menuModel = new Administration_Model_Menu();
        echo $menuModel->loadMenuHTML();
                
    }


}

