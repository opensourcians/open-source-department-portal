<?php

/* default/layout/main.tpl */
class __TwigTemplate_8d1c3a59b3825d75eeeec42d4ccbf4e237180c4a4a3abd58c99907f31c96c678 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('header', $context, $blocks);
        // line 4
        echo "

";
        // line 6
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "

";
        // line 13
        $this->displayBlock('footer', $context, $blocks);
    }

    // line 1
    public function block_header($context, array $blocks = array())
    {
        // line 2
        $this->env->loadTemplate("default/layout/main_header.tpl")->display($context);
    }

    // line 6
    public function block_body($context, array $blocks = array())
    {
        // line 7
        echo "\t";
        if (((isset($context["show_sniff"]) ? $context["show_sniff"] : null) == 1)) {
            // line 8
            echo "\t \t";
            $this->env->loadTemplate("default/layout/sniff.tpl")->display($context);
            // line 9
            echo "\t";
        }
    }

    // line 13
    public function block_footer($context, array $blocks = array())
    {
        // line 14
        echo "    ";
        // line 15
        echo "    ";
        if (((isset($context["show_footer"]) ? $context["show_footer"] : null) == true)) {
            // line 16
            echo "        </div> <!-- end of #row\" -->
        </div> <!-- end of #main\" -->
        <div class=\"push\"></div>
        </div> <!-- end of #page section -->
        </div> <!-- end of #wrapper section -->
    ";
        }
        // line 22
        echo "    ";
        $this->env->loadTemplate("default/layout/main_footer.tpl")->display($context);
    }

    public function getTemplateName()
    {
        return "default/layout/main.tpl";
    }

    public function getDebugInfo()
    {
        return array (  75 => 22,  67 => 16,  64 => 15,  62 => 14,  59 => 13,  54 => 9,  51 => 8,  48 => 7,  45 => 6,  41 => 2,  38 => 1,  34 => 13,  30 => 11,  28 => 6,  24 => 4,  22 => 1,);
    }
}
