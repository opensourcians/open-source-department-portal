<?php

class Administration_Model_Question extends Zend_Db_Table_Abstract
{
    protected $_name="Question";
    public function addQuestion($Question,$answerA,$answerB,$answerC,$answerD,$correctAnswer,$competitionId) {
        $inserted = array('Question'=>$Question,'answerA'=>$answerA,'answerB'=>$answerB,
            'answerC'=>$answerC,'answerD'=>$answerD,
            'correctAnswer'=>$correctAnswer,
           'competitionId'=>$competitionId );
       $this->insert($inserted);
       //return $id;
    }
    public function editQuestion($Question,$answerA,$answerB,$answerC,$answerD,$correctAnswer,
            $competitionId,$ques_id) {
        $edited = array('Question'=>$Question,'answerA'=>$answerA,'answerB'=>$answerB,
            'answerC'=>$answerC,'answerD'=>$answerD,
            'correctAnswer'=>$correctAnswer,
           'competitionId'=>$competitionId);
       $this->update($edited, "ques_id=$ques_id");
    }
    public function deleteQuestion($id) {
        $this->delete("id=$id");
    }
    public function listQuestion() {
        $select = $this->select()->setIntegrityCheck(false)
             ->from(array('c' => 'competition'),
                    array('name'))
             ->join(array('q' => 'Question'),
                    'q.competitionId = c.id');
     return $this->fetchAll($select)->toArray();
        
    }
}

