<?php

class Administration_Model_Contactcategory extends Zend_Db_Table_Abstract
{
    protected $_name= 'contact_category' ;//table name
    
    function createContactCategory($catName)
    {
        $catName=array('cat_name'=>$catName);
        $this->insert($catName);
    }
    
    function selectContactCategory()
    {
        $catName=$this->select();
        return $this->fetchAll($catName)->toArray();
    }
    
    function getCategoryById($id)
    {
        $CatId=$this->select()->where("cat_id = $id");
        return $this->fetchRow($CatId)->toArray();
    }
    
    function editContactCategory($id ,$catName)
    {
        $catName=array('cat_name'=>$catName);
        $this->update($catName , " cat_id=$id "); 
    }

    function deleteContactCategory($id)
    {
        $this->delete("cat_id = $id");

    }
}

