<?php

class Administration_CourseController extends Zend_Controller_Action
{
         
         
    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function deleteAction()
    {
         $course_id=$this->_request->getParam('id');
         $course_model=new Administration_Model_Course();
         $course_model->deleteCourse($course_id);
         $this->_redirect('/administration/course/list?msg=1');
        // action body
    }

    public function editAction()
    {
        $course_model=new Administration_Model_Course();
        $courseid=$this->_request->getParam('id');
        if($this->_request->isPost()){
            $coursecategory=$this->getRequest()->getParam('categoryname');
            $coursename=$this->getRequest()->getParam('coursename');
            $course_model->editCourse($courseid,$coursename,$coursecategory);
            $this->view->msg="Course Edited Sucessfully";
            $this->_redirect('/administration/course/list');
        }
        $course=new Administration_Form_Course();
        $this->view->course=$course;
        $coursedata=$course_model->getCourseById($courseid);
        $course->getElement("coursename")->setValue($coursedata[0]['course_name']);
        // action body
    }

    public function listAction()
    {
         $course_model = new Administration_Model_Course();
         $coursedata = $course_model->listCourse();
         $this->view->coursedata=  $coursedata;
        // action body
    }

    public function addAction()
    {
         $course=new Administration_Form_Course();
         $this->view->course=$course;
         if($this->_request->isPost()){
            $course_name=$this->getRequest()->getParam('coursename');
            
            $categoryid=$this->getRequest()->getParam('categoryname');
            $course_model = new Administration_Model_Course();
            $course_model->addCourse($course_name,$categoryid);
            $this->redirect('/administration/course/index');
        }
        // action body
    }


}









