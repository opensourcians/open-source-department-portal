<?php
class Administration_Model_Forumcategory extends Zend_Db_Table_Abstract
{
    protected $_name="Category";
    public function addForumcategory($name,$Description) {
        $inserted = array('name'=>$name,'Description'=>$Description);
        $this->insert($inserted);
    }
    public function editForumcategory($name,$Description,$id) {
        $edited = array('name'=>$name,'Description'=>$Description);
        $this->update($edited, "cat_id = $id");
    }
    public function deleteForumcategory($id) {
        $this->delete("cat_id=$id");
    }
    public function listForumcategory() {
        $selected = $this->select();
        $array = $this->fetchAll($selected)->toArray();
        for($i=0;$i<count($array);$i++)
        {
            $section_model=new Administration_Model_Forumsection();
            $array[$i]['sections'] = $section_model->listForumsections($array[$i]['cat_id']);
            $topic_model=new Administration_Model_Forumtopic();
            for($j=0;$j<count($array[$i]['sections']);$j++)
            {
                $array[$i]['sections'][$j]['topicsno']=$topic_model->listForumTopicsNo($array[$i]['sections'][$j]['id']);
                $array[$i]['sections'][$j]['latest']=$topic_model->latestTopic($array[$i]['sections'][$j]['id']);
            }
        }
        //print_r($array[0]['sections']);
        return $array;
        
    }
    public function getforumcategoryByid($id) {
            $select= $this->select()->where("cat_id=$id");
            return $this->fetchAll($select)->toArray();
    }
}