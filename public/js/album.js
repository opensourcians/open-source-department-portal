document.getElementById('links').onclick = function (event) {
        event = event || window.event;
        var target =  event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event, 
            
            onslide: function (index, slide) {
                var text = this.list[index].getAttribute('data-description'),
                    node = this.container.find('.description');
                node.empty();
                if (text) {
                    node[0].appendChild(document.createTextNode(text));
                }
            },
            
            
    }, 
            links = this.getElementsByTagName('a');
            if(target.src){
                blueimp.Gallery(links, options);
                document.body.style.overflow = '';
            }
    };