<?php

class GraduateController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $this->redirect("/graduate/list");
    }
    
    public function listAction()
    {
        
        //list branches
        $branchesModel = new Administration_Model_Branch();
        $branches = $branchesModel->listBranches();
        
        //list tracks
        $tracksModel = new Administration_Model_Track();
        $tracks = $tracksModel->listTracks();
        
        //list intakes
        $intakesModel = new Administration_Model_Intake();
        $intakes = $intakesModel->listIntakes();
        
        $usersModel = new Administration_Model_User();
        
        if($this->getRequest()->isPost){
            $branch = $this->getRequest()->getParam("branch") ? $this->getRequest()->getParam("branch") : 0;
            $track = $this->getRequest()->getParam("track") ? $this->getRequest()->getParam("track") : 0;
            $intake = $this->getRequest()->getParam("intake") ? $this->getRequest()->getParam("intake") : 0;
            $graduates = $usersModel->getGraduates($branch, $intake, $track);
            
        } else {
            $graduates = $usersModel->getGraduates();
        }
        
        //send objects to the view
        $this->view->tracks=$tracks;
        $this->view->branches=$branches;
        $this->view->intakes=$intakes;
        $this->view->graduates = $graduates;
        
        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($graduates);
        $paginator->setItemCountPerPage(20);
        $paginator->setCurrentPageNumber($page);
        
        $this->view->paginator = $paginator;
    }

}

