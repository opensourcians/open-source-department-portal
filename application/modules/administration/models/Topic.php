<?php

class Administration_Model_Topic extends Zend_Db_Table_Abstract
{
    protected $_name="Topic";
    public function addTopic($Subject,$body,$section_id,$user_id) {
        $inserted = array('subject'=>$Subject,'body'=>$body
                ,'section_id'=>$section_id,'user_id'=>$user_id);
        $this->insert($inserted);
    }
    public function editTopic($Subject,$body,$section_id,$user_id,$topic_id) {
        $edited = array('subject'=>$Subject,'body'=>$body
                ,'section_id'=>$section_id,'user_id'=>$user_id);
        $this->update($edited, "topic_id = $topic_id");
    }
    public function deleteTopic($topic_id) {
        $this->delete("topic_id=$topic_id");
    }
    public function listTopic() {
        $selected = $this->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'))
                ->join(array('t' => 'Topic'),
                    't.user_id = u.id')
                ->join(array('s'=>'Section'),
                    't.section_id= s.id');
        return $this->fetchAll($selected)->toArray();
    }
    public function gettopicByid($id) {
            $select= $this->select()
                    ->setIntegrityCheck(false)
                ->from(array('u' => 'user'))
                ->join(array('t' => 'Topic'),
                    't.user_id = u.id')
                ->join(array('s'=>'Section'),
                    't.section_id= s.id')->where("topic_id=$id");
            return $this->fetchAll($select)->toArray();
    }

}


