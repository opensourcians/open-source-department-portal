<div class="container">
    <div class="row clearfix" id="footer">
    <div class="col-md-12">
        <footer>
            <div id="footer-widgets">
                <div class="footer-4-column">
                    <div id="text-2" class="widget"><h3 class="widget-title "><span class="glyphicon glyphicon-bookmark"></span> About us</h3>			<div class="textwidget">

                            <p class="form-control-static">Islam</p>
                        </div>
                    </div><div id="blog_subscription-2" class="widget jetpack_subscription_widget"><h3 class="widget-title"><span class="glyphicon glyphicon-envelope"> </span> Newsletter</h3>
                        <form action="" method="post" accept-charset="utf-8" id="subscribe-blog-blog_subscription-2">
                            <p id="subscribe-text">Enter your email address to subscribe to our website and receive notifications .</p>
                            <p id="subscribe-email"><input class="form-control" type="text" name="email"  placeholder="Your Email" id="subscribe-field"/></p>

                            <p id="subscribe-submit">
                                <input type="submit" class="btn btn-primary" id="subscribe" value="Subscribe"  />
                            </p>
                        </form>


                    </div>
                    <div id="nav_menu-2" class="widget widget_nav_menu">
                        <h3 class="widget-title"><span class="glyphicon glyphicon-retweet"></span> Tweets</h3>
                        <!-- Carousel ================================================== -->
                        <div id="testimonial" class="carousel slide">
                            <!-- Indicators -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <blockquote>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                        <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                                    </blockquote>
                                </div>
                                <div class="item">
                                    <blockquote>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                        <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                                    </blockquote>
                                </div>
                                <div class="item">
                                    <blockquote>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                        <small>Someone famous in <cite title="Source Title">Source Title</cite></small>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="carousel-controls">
                                <a class="carousel-control left" href="#testimonial" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                <a class="carousel-control right" href="#testimonial" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>

                        </div><!-- End Carousel -->
                    </div>
                    <div class="clearfix"></div>
                </div><!-- close .width-container -->
            </div><!-- close #footer-widgets -->

            <div class="width-container">

                <div id="copyright" class="text-center">&copy; <?php echo @date("Y") ?> All Rights Reserved.</div>
                <div class="clearfix"></div>
            </div><!-- close .width-container -->
        </footer>

    </div>
</div>
</div>