<?php

class Administration_Model_AttendanceRule extends Zend_Db_Table_Abstract
{
    protected $_name = "attendanceRule";
    
    function listRules(){
        return $this->fetchAll()->toArray();
    }
    
    function addRule($ruleInfo) {
        
        $row = $this->createRow();

        $row->type = $ruleInfo['type'];
        $row->maximum = $ruleInfo['maximum'];
        $row->deduction = $ruleInfo['deduction'];
                
        return $row->save();
    }
    
    function getRuleById($ruleId) {
        
        return $this->find($ruleId)->toArray();
    }
    
    function editRule($ruleId, $ruleInfo) {
        
        return $this->update($ruleInfo, "id = $ruleId");

    }
    
    function deleteRule($ruleId) {
        return $this->delete("id = $ruleId");
    }
    


}

