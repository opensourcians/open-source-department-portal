<?php

class Application_Form_Editprofile extends Zend_Form
{

    public function init()
    {
        $this->addElement('text', 'first_name',array('label' => 'First Name:','required' => true,"class"=>"form-control" , "placeholder"=>"Enter Your First Name Here"));
        $this->getElement("first_name")->addValidator('Alpha');
        $this->getElement("first_name")->addDecorator("Errors", array("style"=>"color:red"));
        
        $this->addElement('text', 'last_name',array('label' => 'Last Name:','required' => true,"class"=>"form-control" , "placeholder"=>"Enter Your Last Name Here")); 
        $this->getElement("last_name")->addValidator('Alpha'); 
        $this->getElement("last_name")->addDecorator("Errors", array("style"=>"color:red"));
        
        $this->addElement('password', 'password',array('label' => 'Password:',"class"=>"form-control" , "placeholder"=>"Enter Password Here"));
        $this->addElement('password', 'confirm_password',array('label' => 'Confirm Password:',"class"=>"form-control" , "placeholder"=>"Enter Password Again"));
        $this->getElement("confirm_password")->addValidator('Identical', false, array('token' => 'password'));
        $this->getElement("confirm_password")->addDecorator("Errors", array("style"=>"color:red"));
        $this->getElement("confirm_password")->addErrorMessage('The passwords do not match');
        
        $this->addElement('text', 'email',array('label' => 'Email:','required' => true,"class"=>"form-control" , "placeholder"=>"Enter Email Here"));
        $this->getElement("email")->addValidator('EmailAddress');
        $this->getElement("email")->addDecorator("Errors", array("style"=>"color:red"));
        
        $upload = new Zend_Form_Element_File(["name"=>"image","class"=>"form-control","label"=>"Upload Your Photo"]);
        $upload->addValidator('IsImage')->addValidator('Size', TRUE, 1242880);
        $upload->addDecorator("Errors", array("style"=>"color:red"));
        $this->addElement($upload);
        
        $this->addElement('text', 'facebook',array('label' => 'Facebook:',"class"=>"form-control" , "placeholder"=>"Enter Your Facebook Account Address Here"));
        //$this->getElement("facebook")->addValidator('regex',true,array('facebook.com'));

        $this->addElement('text', 'twitter',array('label' => 'Twitter:',"class"=>"form-control" , "placeholder"=>"Enter Your Twitter Account Address Here"));
        //$this->getElement("twitter")->addValidator('regex',true,array('twitter.com'));
        
        $this->addElement('text', 'linkedin',array('label' => 'Linkedin:',"class"=>"form-control" , "placeholder"=>"Enter Your LinkedIn Account Address Here"));
        //$this->getElement("linkedin")->addValidator('regex',true,array('linkedin.com'));
        
        $this->addElement('textarea', 'description',array('label' => 'Description:','required' => true,"class"=>"form-control" , "placeholder"=>"Enter Description Here"));
        $this->getElement('description')->addDecorator("Errors", array("style"=>"color:red"));
        
        $select = new Zend_Form_Element_Select(["name"=>"gender","class"=>"form-control","label"=>"Gender:"]);
        $select->addMultiOptions(array("male"=>"Male","female"=>"Female"));
        $this->addElement($select);
        
        $track = new Zend_Form_Element_Select(["name"=>"track_id","class"=>"form-control","label"=>"Track:"]);
        $track->addMultiOptions(array("1"=>"Open Source","2"=>"Telecom"));
        $this->addElement($track);
        
        $this->addElement('text', 'job',array('label' => 'Job:','required' => true,"class"=>"form-control" , "placeholder"=>"Enter Your Job Here"));
        $this->getElement("job")->addValidator(new Zend_Validate_Alnum(array ('allowWhiteSpace' => TRUE)));
        $this->getElement("job")->addDecorator("Errors", array("style"=>"color:red"));
                
        $intake = new Zend_Form_Element_Select(["name"=>"intake","class"=>"form-control","label"=>"Intake"]);

        $intakes = date("Y") - 2014 + 34;
        for($i = 1 ; $i <= $intakes ; $i++){
            $intake->addMultiOption($i,$i);
        }
        $this->addElement($intake);
        
        $this->addElement("hidden","id");
        
        
        $this->addElement('submit', 'submit', array('ignore'=> true,'label'=> 'Submit',"class"=>"btn btn-default"));    
    }


}