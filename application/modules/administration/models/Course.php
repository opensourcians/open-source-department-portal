<?php

class Administration_Model_Course extends Zend_Db_Table_Abstract
{
    protected  $_name='course';
    function listAll(){ // for dropdown menu
       $select  = $this->_db->select()->from($this->_name,array('key' => 'course_id','value' => 'course_name'));
         $result = $this->getAdapter()->fetchAll($select);
         return $result;
    }
    function addCourse($course_name,$category_id){
        $row = $this->createRow();
        $row->course_name =$course_name;
        $row->category_id=$category_id;
        return $row->save();   
    }
    function editCourse($course_id,$course_name,$coursecategory){
         $coursedata=array('course_id'=>$course_id,'course_name'=>$course_name,'category_id'=>$coursecategory);
         $this->update($coursedata,"course_id=$course_id" );
        
    }
    function deleteCourse($course_id){
         $this->delete("course_id=$course_id");
    }
    function listCourse(){ //for listing course
         $select = $this->_db->select()
                ->from('course',array('course_id','course_name'))
                ->joinInner('coursecategory','course.category_id=coursecategory.coursecategory_id',array('coursecategory_name'));
        
         $result = $this->getAdapter()->fetchAll($select);
         return $result;
    }
    function getCourseById($courseid){
        $sql=$this->select()->where("course_id=$courseid");
        return $this->fetchAll($sql)->toArray();
        
    }


}

