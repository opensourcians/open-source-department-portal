<?php

class Administration_Model_Album extends Zend_Db_Table_Abstract
{
    protected $_name = "album";
    
    function listAlbums(){
        return $this->fetchAll()->toArray();
    }
    
    
    function addAlbum($albumInfo) {
                
        $row = $this->createRow();
        $row->title = $albumInfo['title'];
        $row->system = $albumInfo['system'];
        return $row->save();
    }
    
    function getAlbumById($albumId) {
        
        return $this->find($albumId)->toArray();
    }
    
    function getAlbumForDisplay($albumId){
        $select = $this->select()->setIntegrityCheck(false)->from("album", array("*", "album.id as aId","album.title as aTitle"))->join("image", "album.id=image.albumId")->where("album.id = $albumId");
        return $this->fetchAll($select)->toArray();
    }
            
    function editAlbum($albumId, $albumInfo) {
        
                
         return $this->update($albumInfo, "id = $albumId");

    }
    function getAlbumsForDisplay($albumCover = false){
        $select = $this->select()->setIntegrityCheck(false)->from("album", array("*", "album.id as aId","album.title as aTitle"))->join("image", "album.id=image.albumId")->where("album.system = 0");
        if($albumCover)
            $select->where ("image.albumCover=1");
        
        return $this->fetchAll($select)->toArray();
        
    }
    
    
    
   function deleteAlbum($albumId) {
        return $this->delete("id = $albumId");
    }
    
}

