$(document).ready(function()
{
    var position = new google.maps.LatLng(30.071118,31.021108);
    var myOptions = {
      zoom: 16,
      center: position,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(
        document.getElementById("map_canvas"),
        myOptions);

    var marker = new google.maps.Marker({
        position: position,
        map: map,
        title:"This is the place."
    });

    var contentString = '<h4>Information Technology Institute - ITI</h4>\
                            <p>148 cairo Alex Desert Rd. Abou Rawash\
                            02 35355656</p>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $("#getDirections").click(
            function () {
                $("#map_canvas").empty();
                var directionsService = new google.maps.DirectionsService(),
                directionsDisplay = new google.maps.DirectionsRenderer(),
                createMap = function (start) {
                  var travel = {
                    origin : (start.coords)? new google.maps.LatLng(start.lat, start.lng) : start.address,
                    destination : "Smart Village, Abou Rawash, Giza, Egypt",
                    travelMode : google.maps.DirectionsTravelMode.DRIVING
                    // Exchanging DRIVING to WALKING above can prove quite amusing :-)
                  },
                  mapOptions = {
                    zoom: 10,
                    // Default view: Cairo Egypt
                    center : new google.maps.LatLng(30.048293,31.23648),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                  };

                  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
                  directionsDisplay.setMap(map);
                  directionsDisplay.setPanel(document.getElementById("map-directions"));
                  directionsService.route(travel, function(result, status) {
                    if (status === google.maps.DirectionsStatus.OK) {
                      directionsDisplay.setDirections(result);
                    }
                  });
                };

                // Check for geolocation support
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function (position) {
                    // Success!
                    createMap({
                      coords : true,
                      lat : position.coords.latitude,
                      lng : position.coords.longitude
                    });
                  },
                  function () {
                  // Gelocation fallback
                    createMap({
                      coords : false,
                      address : "Cairo, Egypt"
                    });
                  });
                }
                else {
                  // No geolocation fallback: Defaults to Lisbon, Portugal
                  createMap({
                    coords : false,
                    address : "Cairo, Egypt"
                  });
                }
        });
});
