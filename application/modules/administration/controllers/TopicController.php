<?php

class Administration_TopicController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $list = new Administration_Model_Topic();
        $this->view->data = $list->listTopic();
        //var_dump($this->view->data);
    }

    public function addAction()
    {
        $list = new Administration_Model_Forumsection();
        $this->view->data = $list->listForumsection();
        //var_dump($this->view->data);
        if ($this->_request->isPost())
        {
            $add = new Administration_Model_Topic();
            $add->addTopic($this->_request->getParam('subject'), 
            $this->_request->getParam('body'),
            $this->_request->getParam('sectionname'), 1);
            $this->redirect("/administration/topic/");
        }
    }

    public function editAction()
    {
        $cat = new Administration_Model_Forumsection();
        $this->view->form= $cat->listForumsection();
        $edit = new Administration_Model_Topic();
        $id = $this->_request->getParam('id');
        $this->view->data = $edit->gettopicByid($this->_request->getParam('id'));
        if($this->_request->isPost())
        {
            $edit->editTopic($this->_request->getParam('subject'),
            $this->_request->getParam('body'),
            $this->_request->getParam('sectionname'),1, $id);
            $this->redirect("/administration/topic/");
        }
        //var_dump($this->view->data );
    }

    public function deleteAction()
    {
         $delete = new Administration_Model_Topic();
        if($this->_request->isPost())
        {
            $delete->deleteTopic($this->_request->getParam('id'));
            $this->redirect('/administration/topic');
        }
    }


}







