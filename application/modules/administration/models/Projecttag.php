<?php

class Administration_Model_Projecttag extends Zend_Db_Table_Abstract
{
    protected $_name="project_tag";
    public function addTag($tid,$tag)
    {
        $data = array('pid'=> $tid,'tag'=> $tag );
        $this->insert($data);
    }
    
    public function editTag($tag,$id) 
    {
        $data = array('tag'=>$tag);
        $this->update($data,"pid=$id");     
    }
    
    public function deleteTag($id)
    {
        $this->delete("pid=$id");
    }
}

