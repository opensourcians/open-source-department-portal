<?php

class Administration_Form_AttendanceRule extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $type = new Zend_Form_Element_Text("type");
        $type->addFilter(new Zend_Filter_StripTags);
        $type->setRequired();
        $type->setAttrib("class", "form-control");
        $type->setAttrib("placeholder", "Enter Rule Type");
        $type->setAttrib("id", "type");
        $type->removeDecorator('Label');
        $type->setDecorators(array('ViewHelper'));
        
        $maximum = new Zend_Form_Element_Text("maximum");
        $maximum->addFilter(new Zend_Filter_StripTags);
        $maximum->setRequired();
        $maximum->setAttrib("class", "form-control");
        $maximum->setAttrib("placeholder", "Maximum days for that rule");
        $maximum->setAttrib("id", "maximum");
        $maximum->removeDecorator('Label');
        $maximum->setDecorators(array('ViewHelper'));

        $deduction = new Zend_Form_Element_Text("deduction");
        $deduction->addFilter(new Zend_Filter_StripTags);
        $deduction->setRequired();
        $deduction->setAttrib("class", "form-control");
        $deduction->setAttrib("placeholder", "Deduction that will be applied");
        $deduction->setAttrib("id", "deduction");
        $deduction->removeDecorator('Label');
        $deduction->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setLabel("Save Rule");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($id,$type,$maximum,$deduction,$submit));
    }


}

