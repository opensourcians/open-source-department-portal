<?php

/* default/admin/settings_index.tpl */
class __TwigTemplate_6de1726c7fa3990c4b10de595dc3a7d24c80480471708a911f42d885e46dc1d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>
\$(document).ready(function() {
    \$.ajax({
        url:'";
        // line 4
        echo (isset($context["web_admin_ajax_url"]) ? $context["web_admin_ajax_url"] : null);
        echo "?a=version',
        success:function(version){
            \$(\".admin-block-version\").html(version);
        }
    });

    \$('.edit-block a').on('click', function(e) {
        e.preventDefault();

        var \$self = \$(this);

        var extraContent = \$.ajax('";
        // line 15
        echo $this->getAttribute((isset($context["_p"]) ? $context["_p"] : null), "web_ajax", array());
        echo "admin.ajax.php', {
            type: 'post',
            data: {
                a: 'get_extra_content',
                block: \$self.data('id')
            }
        });

        \$.when(extraContent).done(function(content) {
            \$('#extra-content').val(content);
            \$('#extra-block').val(\$self.data('id'));
            \$('#modal-extra-title').text(\$self.data('label'));

            \$('#modal-extra').modal('show');
        });
    });

    \$('#btn-block-editor-save').on('click', function(e) {
        e.preventDefault();

        var save = \$.ajax('";
        // line 35
        echo $this->getAttribute((isset($context["_p"]) ? $context["_p"] : null), "web_ajax", array());
        echo "admin.ajax.php', {
            type: 'post',
            data: \$('#block-extra-data').serialize() + '&a=save_block_extra'
        });

        \$.when(save).done(function() {
            window.location.reload();
        });
    });
});
</script>

<div id=\"settings\">
    <div class=\"row\">
    ";
        // line 49
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["blocks"]) ? $context["blocks"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["block_item"]) {
            // line 50
            echo "        <div id=\"tabs-";
            echo $this->getAttribute($context["loop"], "index", array());
            echo "\" class=\"span6\">
            <div class=\"well_border ";
            // line 51
            echo $this->getAttribute($context["block_item"], "class", array());
            echo "\">
                ";
            // line 52
            if (($this->getAttribute($context["block_item"], "editable", array()) && $this->getAttribute((isset($context["_u"]) ? $context["_u"] : null), "is_admin", array()))) {
                // line 53
                echo "                    <div class=\"pull-right edit-block\" id=\"edit-";
                echo $this->getAttribute($context["block_item"], "class", array());
                echo "\">
                        <a href=\"#\" data-label=\"";
                // line 54
                echo $this->getAttribute($context["block_item"], "label", array());
                echo "\" data-id=\"";
                echo $this->getAttribute($context["block_item"], "class", array());
                echo "\">
                            <img src=\"";
                // line 55
                echo $this->getAttribute((isset($context["_p"]) ? $context["_p"] : null), "web_img", array());
                echo "icons/22/edit.png\" alt=\"";
                echo get_lang("Edit");
                echo "\" title=\"";
                echo get_lang("Edit");
                echo "\">
                        </a>
                    </div>
                ";
            }
            // line 59
            echo "                <h4>";
            echo $this->getAttribute($context["block_item"], "icon", array());
            echo " ";
            echo $this->getAttribute($context["block_item"], "label", array());
            echo "</h4>
                <div style=\"list-style-type:none\">
                    ";
            // line 61
            echo $this->getAttribute($context["block_item"], "search_form", array());
            echo "
                </div>
                ";
            // line 63
            if ((!(null === $this->getAttribute($context["block_item"], "items", array())))) {
                // line 64
                echo "                    <ul>
    \t\t    \t";
                // line 65
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["block_item"], "items", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["url"]) {
                    // line 66
                    echo "    \t\t    \t\t<li>
                            <a href=\"";
                    // line 67
                    echo $this->getAttribute($context["url"], "url", array());
                    echo "\">
                                ";
                    // line 68
                    echo $this->getAttribute($context["url"], "label", array());
                    echo "
                            </a>
                        </li>
    \t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['url'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 72
                echo "                    </ul>
                ";
            }
            // line 74
            echo "
                ";
            // line 75
            if ((!(null === $this->getAttribute($context["block_item"], "extra", array())))) {
                // line 76
                echo "                    <div>
                    ";
                // line 77
                echo $this->getAttribute($context["block_item"], "extra", array());
                echo "
                    </div>
                ";
            }
            // line 80
            echo "
                ";
            // line 81
            if ($this->getAttribute($context["block_item"], "extraContent", array())) {
                // line 82
                echo "                    <div>";
                echo $this->getAttribute($context["block_item"], "extraContent", array());
                echo "</div>
                ";
            }
            // line 84
            echo "            </div>
        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['block_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "    </div>
</div>

";
        // line 90
        if ($this->getAttribute((isset($context["_u"]) ? $context["_u"] : null), "is_admin", array())) {
            // line 91
            echo "<div id=\"modal-extra\" class=\"modal hide fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modal-extra-title\" aria-hidden=\"true\">
    <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
        <h3 id=\"modal-extra-title\">";
            // line 94
            echo get_lang("Blocks");
            echo "</h3>
    </div>
    <div class=\"modal-body\">
        <form action=\"#\" method=\"post\" id=\"block-extra-data\">
            <textarea rows=\"5\" name=\"content\" class=\"input-block-level\" id=\"extra-content\"></textarea>
            <input type=\"hidden\" name=\"block\" id=\"extra-block\" value=\"\">
        </form>
    </div>
    <div class=\"modal-footer\">
        <button id=\"btn-block-editor-save\" class=\"btn btn-primary\">";
            // line 103
            echo get_lang("Save");
            echo "</button>
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/admin/settings_index.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 103,  224 => 94,  219 => 91,  217 => 90,  212 => 87,  196 => 84,  190 => 82,  188 => 81,  185 => 80,  179 => 77,  176 => 76,  174 => 75,  171 => 74,  167 => 72,  157 => 68,  153 => 67,  150 => 66,  146 => 65,  143 => 64,  141 => 63,  136 => 61,  128 => 59,  117 => 55,  111 => 54,  106 => 53,  104 => 52,  100 => 51,  95 => 50,  78 => 49,  61 => 35,  38 => 15,  24 => 4,  19 => 1,);
    }
}
