<?php

class Administration_Form_ContactCategory extends Zend_Form
{

    public function init()
    {
        //Add category form
       
        $this->setMethod('post');
        $this->addElement('text','cont_cat',array(
                                            'label'=>'Category Title:',
                                            'required'=>true , 
                                            'filters'=>array('StringTrim'),
                                            'class'=>'form-control',
                                            'placeholder'=>'Add a Category') 
                         );
       
        $this->addElement('submit', 'submit', array(
                            'label'    => 'Add Contact Category',
                            'ignore'   => true,
                            'class'=>'btn btn-primary'
                        ));
 
    }


}

