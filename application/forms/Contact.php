<?php

class Application_Form_Contact extends Zend_Form
{

    public function init()
    {
        //Contact form

        $this->setAction('/contact/index')
                    ->setMethod('post')
                    ->setAttrib("class", "form-horizontal")
                    ->setAttrib("role", "form");

        $name = $this->createElement('text', 'name');
        $name->setLabel("Name")
             ->setAttrib("class", "col-sm-2 control-label");
        $name->setRequired(true)
             ->setAttrib("class","form-control")
             ->setAttrib("placeholder", "Your Name")
             ->removeDecorator('Errors')
             ->setAttrib("id", "inputName");

        $email = $this->createElement('text', 'email');
        $email->setLabel("Email")
             ->setAttrib("class", "col-sm-2 control-label");
        $email->addValidator(new Zend_Validate_EmailAddress())
              ->setRequired(true)
              ->addFilter('StringTrim');
        $email->setAttrib("class","form-control")
             ->setAttrib("placeholder", "Your Email")
             ->removeDecorator('Errors')
             ->setAttrib("id", "inputEmail");

        //view categories in the form
        $categoryModel=new Administration_Model_Contactcategory();
        $catArray=$categoryModel->selectContactCategory();


        $subject=$this->createElement('select', 'subject');
        $subject->setLabel("Subject")
                 ->setAttrib("class", "col-sm-2 control-label");
        $subject->setRequired(true)
                 ->setAttrib("class","form-control")
                 ->setAttrib("placeholder", "Select")
                 ->removeDecorator('Errors')
                 ->setAttrib("id", "inputSubject");


         foreach ($catArray as $row)
         {
             $subject->addMultiOption($row['cat_id'],$row['cat_name']);

         }



        $message= $this->createElement('textarea', 'message');
        $message->setLabel("Message")
                 ->setAttrib("class", "col-sm-2 control-label");
        $message->setRequired(true)
                 ->setAttrib("class","form-control")
                 ->setAttrib("placeholder", "Write your message")
                 ->removeDecorator('Errors')
                 ->setAttrib("id", "inputArea")
                 ->setOptions(array('cols' => '4', 'rows' => '7'));

       $submit=$this->createElement('submit', 'submit');
       $submit->setAttrib("id", "submit")
              ->setAttrib("class", "btn btn-os")
              ->setValue("Submit");
       // Add elements to form:
       $this->addElement($name)
                   ->addElement($email)
                   ->addElement($subject)
                   ->addElement($message)
                   ->addElement($submit);

    }


}
