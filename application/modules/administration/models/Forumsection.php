<?php

class Administration_Model_Forumsection extends Zend_Db_Table_Abstract
{
    protected $_name="Section";
    public function addForumsection($sec_name,$description,$category_id) {
        $inserted = array('sec_name'=>$sec_name,'description'=>$description
                ,'category_id'=>$category_id);
        $this->insert($inserted);
    }
    public function editForumsection($sec_name,$description,$id,$category_id) {
        $edited = array('sec_name'=>$sec_name,'description'=>$description
                ,'category_id'=>$category_id);
        $this->update($edited, "id = $id");
    }
    public function deleteForumsection($id) {
        $this->delete("id=$id");
    }
    public function listForumsection() {
        $selected = $this->select()->setIntegrityCheck(false)
                ->from(array('c' => 'Category'),
                    array('name'))
                ->join(array('s' => 'Section'),
                    's.category_id = c.cat_id');
        return $this->fetchAll($selected)->toArray();
    }
    public function getforumsectionByid($id) {
            $select= $this->select()
                    ->setIntegrityCheck(false)
                ->from(array('c' => 'Category'),
                    array('name'))
                ->join(array('s' => 'Section'),
                    's.category_id = c.cat_id')->where("id=$id");
            return $this->fetchAll($select)->toArray();
    }
    public function listForumsections($id) {
        $selected = $this->select()->where("category_id=$id");
        return $this->fetchAll($selected)->toArray();
    }
}
