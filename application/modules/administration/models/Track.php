<?php

class Administration_Model_Track extends Zend_Db_Table_Abstract
{
    protected $_name = "track";
    
    function listTracks(){
        return $this->fetchAll()->toArray();
    }

}