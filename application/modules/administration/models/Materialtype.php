<?php

class Administration_Model_Materialtype extends Zend_Db_Table_Abstract
{
    protected  $_name='materialtype';
    function listAll(){
         $select  = $this->_db->select()->from($this->_name,array('key' => 'materialtype_id','value' => 'materialtype_name'));
         $result = $this->getAdapter()->fetchAll($select);
         return $result;
    }
    function addMaterialType($materialtype_name){
        $row = $this->createRow();
        $row->materialtype_name =$materialtype_name;
        return $row->save();   
    }
 
     function getMaterialTypeById($id){
        $sql=$this->select()->where("materialtype_id=$id");
        return $this->fetchAll($sql)->toArray(); 
    }
    function deleteMaterialType($id){
        $this->delete("materialtype_id=$id");
    }
     function listMaterialType(){
        return $this->fetchAll()->toArray();
    }
    function editMaterialType($id,$materialtype_name){
        $materialtypedata=array('materialtype_id'=>$id,'materialtype_name'=>$materialtype_name);
        $this->update($materialtypedata,"materialtype_id=$id" );
        
    }
    


}
