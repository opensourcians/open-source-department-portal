<?php

class Administration_Form_Applicant extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $code = new Zend_Form_Element_Text("code");
        $code->addFilter(new Zend_Filter_StripTags);
        $code->setRequired();
        $code->setAttrib("class", "form-control");
        $code->setAttrib("placeholder", "Enter Applicant ID");
        $code->setAttrib("id", "code");
        $code->removeDecorator('Label');
        $code->setDecorators(array('ViewHelper'));
        
        $name = new Zend_Form_Element_Text("name");
        $name->addFilter(new Zend_Filter_StripTags);
        $name->setRequired();
        $name->setAttrib("class", "form-control");
        $name->setAttrib("placeholder", "Enter Applicant Name");
        $name->setAttrib("id", "name");
        $name->removeDecorator('Label');
        $name->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setLabel("Start Interview");
        $submit->setAttrib("class", "btn btn-default");
        
        $this->addElements(array($id,$code,$name,$submit));
    }


}

