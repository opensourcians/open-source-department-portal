var ajaxResult;
var v  = 0;
var options = {  bootstrapMajorVersion: 3 , currentPage: 1};

function getGraduates(){
    track = $("form select#track").val();
    intake = $("#intake").val();
    name = $("#name").val();
    
    ajaxResult = $.ajax({
        type: 'POST',
        url: baseUrl + "/" + originalModule +"/" + originalController +"/graduatejson",
        data: { 
            'track': track, 
            'intake': intake,
            'name': name,
            'page': options.currentPage -1
        },
        beforeSend: hide ,
        success: statusCheck
    });
}

function hideMenu(){
	$("ul#std_info").css({"display":"none" });
}

function statusCheck(){
    jsonT = jQuery.parseJSON(ajaxResult.responseText);
		
    if(jsonT.stds.length > 0){ 
        $("div#students-container div.thumbnail").children().not(".loading").remove();
        $("div#students-container p.notfound").remove();
    }else{
        $("div#students-container").append("<p class='notfound'> No Result </p>");
    }
				
    var newChildren = "";
				
    for( i = 0 ; i < jsonT.stds.length ; i++ ) {
        var element = " \
                            <div class='col-sm-4 col-md-3'> \
                                 <div class='thumbnail'>\
				<img  src='"+jsonT.stds[i].img+"' alt='Graduate Image' class='img-circle' /> \
                            <div class='caption text-center'>\
                               <h3><a href='"+baseUrl+jsonT.stds[i].url+"' id='"+i+"' >"+jsonT.stds[i].name+"</a></h3> \
                                <span>"+jsonT.stds[i].track+" - intake "+jsonT.stds[i].intake+"</span><br/> \
                                <span>"+jsonT.stds[i].job+"</span> \
                        </div></div></div>";
						
	newChildren += element;
    }
		
    $(".loading").css("display","none");
    
    $("div#students-container").append(newChildren).fadeIn(1000);
    
    if(jsonT.pagesNo === 0 ){
         $("ul.pagination").fadeOut(100);
    }else{
         $("ul.pagination").fadeIn(100);
        options.totalPages = jsonT.pagesNo;        
        $('ul.pagination').bootstrapPaginator(options);
    }

    $("ul.pagination a").on("click",function(){
        switch ($(this).text()){
            case '<<' : options.currentPage = 1; break;
            case '<'  : options.currentPage--;   break;
            case '>>' : options.currentPage =  jsonT.pagesNo; break;
            case '>'  : options.currentPage++; break;
            default   : options.currentPage = $(this).text();
        }         
        $('ul.pagination').bootstrapPaginator(options);
        getGraduates();
    }); 
				
    $("div.thumbnail h3 a").on("mouseenter",function(){
        clearTimeout(v);
        var p  = $(this).position();
        var id = $(this).attr("id");
        $("ul#std_info").delay(100).css({"top": (p.top+25) , "left": (p.left +50) , "display":"block" });
        $("ul#std_info").html("");
					
        if( jQuery.trim(jsonT.stds[id].email) != "") $("ul#std_info").append("<li> <b>E-Mail : </b>" + jsonT.stds[id].email +"</li>");
        if( jQuery.trim(jsonT.stds[id].mobile) != "") $("ul#std_info").append("<li role='presentation' class='divider'></li><li> <b>Phone : </b>" + jsonT.stds[id].mobile +"</li>");
    });

    $("ul#std_info").on("mouseenter",function(){
        clearTimeout(v);
    });
				
    $("div.thumbnail h3 a , ul#std_info").on("mouseleave",function(){
        v = setTimeout("hideMenu()",500);
    });		
}

function hide(){
	$("div#students-container").children().not(".loading").hide();
	$("ul.pagination").hide();
        $(".loading").css("display","inline");
}

getGraduates();

$( "form select#track,#intake,#name" ).change(function() {
        getGraduates();			
});	

$( "#name" ).keyup(function() {
        getGraduates();			
});	


$("form#search").submit(function(){ return false; });





