<?php

class Administration_Model_Role extends Zend_Db_Table_Abstract
{
    protected $_name = "role";
    
    
    
    function getGeustRoleName(){
        return (string)'guest';
    }
    
    function getSuperAdminRoleId(){
        $select = $this->select()->where("name = ?","superadmin");
        $row =  $this->fetchRow($select)->toArray();
        return (int) $row['id'];
    }
    
    function getAdminRoleId(){
        $select = $this->select("id")->where("name = ?","admin");
        $row =  $this->fetchRow($select)->toArray();
        return (int) $row['id'];
    }
    
    function getGraduateRoleId(){
        $select = $this->select("id")->where("name = ?","graduate");
        $row =  $this->fetchRow($select)->toArray();
        return (int) $row['id'];
    }
    
    function getStudentRoleId(){
        $select = $this->select("id")->where("name = ?","student");
        $row =  $this->fetchRow($select)->toArray();
        return (int) $row['id'];
    }
    
    
    function getRoleByID($roleId){
        return $this->find($roleId)->toArray();
    }
    
    
    function listRoles() {
        return $this->fetchAll()->toArray();        
    }
    
    function addRole($roleName){
        $row = $this->createRow();
        $row->name= $roleName;
        return $row->save();
    }
    
    function editRole($roleId, $roleName){
        
         return $this->update(array("name"=>$roleName), "id = $roleId");
    }
    
    function deleteRole($roleId) {
        return $this->delete("id = $roleId");      
    }



}

