<?php

class Administration_Model_Projectteam extends Zend_Db_Table_Abstract
{
    protected $_name="project_team";
    
    public function addTeam($tmember)
    {
        $data = array( 'tmember'=>$tmember );
        $this->insert($data);
    }
    
    public function editTeam($tmember) 
    {
        $data = array('tmember'=>$tmember );
        $this->update($data,"id=$id");     
    }
    
    public function deleteTeam($id)
    {
        $this->delete("id=$id");
    }
}

