<?php

class Administration_Form_User extends Zend_Form
{

    public function init()
    {
        
        $this->setAttrib('enctype', 'multipart/form-data');
        
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $firstName = new Zend_Form_Element_Text("firstName");
        $firstName->addFilter(new Zend_Filter_StripTags);
        $firstName->setRequired();
        $firstName->setAttrib("class", "form-control");
        $firstName->setAttrib("placeholder", "Enter First Name");
        $firstName->setAttrib("id", "firstName");
        $firstName->removeDecorator('Label');
        $firstName->setDecorators(array('ViewHelper'));
        
        $lastName = new Zend_Form_Element_Text("lastName");
        $lastName->addFilter(new Zend_Filter_StripTags);
        $lastName->setAttrib("class", "form-control");
        $lastName->setAttrib("placeholder", "Enter Last Name");
        $lastName->setAttrib("id", "lastName");
        $lastName->setRequired();
        $lastName->removeDecorator('Label');
        $lastName->setDecorators(array('ViewHelper'));
        
        
        $email = new Zend_Form_Element_Text("email");
        $email->addValidator(new Zend_Validate_EmailAddress);
        $email->addValidator(new Zend_Validate_Db_NoRecordExists(array("table"=>'user',"field"=>'email')));
        $email->addFilter(new Zend_Filter_StripTags);
        $email->setRequired();
        $email->setAttrib("class", "form-control");
        $email->setAttrib("placeholder", "Enter Email");
        $email->setAttrib("id", "email");
        $email->removeDecorator('Label');
        $email->setDecorators(array('ViewHelper'));
        
        
        $password = new Zend_Form_Element_Password("password");
        $password->setRequired();
        $password->addValidator(new Zend_Validate_StringLength(6, 20));
        $password->setAttrib("class", "form-control");
        $password->setAttrib("placeholder", "Enter Password");
        $password->setAttrib("id", "password");
        $password->removeDecorator('Label');
        $password->setDecorators(array('ViewHelper'));
        
        $passwordRetype = new Zend_Form_Element_Password("passwordRetype");
        $passwordRetype->setRequired();
        $passwordRetype->addValidator('identical', false, array('token' => 'password'));
        $passwordRetype->setAttrib("class", "form-control");
        $passwordRetype->setAttrib("placeholder", "Retype Password");
        $passwordRetype->setAttrib("id", "passwordRetype");
        $passwordRetype->removeDecorator('Label');
        $passwordRetype->setDecorators(array('ViewHelper'));
        
        $gender = new Zend_Form_Element_Radio("gender");
        $gender->addMultiOption("male", "male");
        $gender->addMultiOption("female", "female");
        
        $gender->setAttrib("id", "gender");
        $gender->removeDecorator('Label');
        $gender->setDecorators(array('ViewHelper'));
        
        $isEnabled = new Zend_Form_Element_Checkbox("isEnabled",array("checked" => "checked"));
        $isEnabled->setAttrib("id", "isEnabled");
        $isEnabled->removeDecorator('Label');
        $isEnabled->setDecorators(array('ViewHelper'));
        
        $roleId = new Zend_Form_Element_Select("roleId");
        $roleId->setRequired();
        $roleId->addMultiOption("","Select Role");
        $roleId->setAttrib("class", "form-control");
        $roleId->setDecorators(array('ViewHelper'));
        $roleModel = new Administration_Model_Role();
        $roles = $roleModel->listRoles();
        foreach($roles as $role){
            if($role['name'] != "guest")
            $roleId->addMultiOption($role['id'],$role['name']);
        }
        
        $jobTitle = new Zend_Form_Element_Text("jobTitle");
        $jobTitle->setAttrib("class", "form-control");
        $jobTitle->setAttrib("placeholder", "Job Title @ Company");
        $jobTitle->setAttrib("id", "jobTitle");
        $jobTitle->removeDecorator('Label');
        $jobTitle->setDecorators(array('ViewHelper'));
        
        $description = new Zend_Form_Element_Textarea("description");
        $description->setAttrib("class", "form-control");
        $description->setAttrib("id", "ckeditor_standard");
        $description->removeDecorator('Label');
        $description->setDecorators(array('ViewHelper'));
        
        $facebook = new Zend_Form_Element_Text("facebook");
        $facebook->setAttrib("class", "form-control");
        $facebook->setAttrib("placeholder", "Facebook");
        $facebook->setAttrib("id", "facebook");
        $facebook->removeDecorator('Label');
        $facebook->setDecorators(array('ViewHelper'));
        
        $twitter = new Zend_Form_Element_Text("twitter");
        $twitter->setAttrib("class", "form-control");
        $twitter->setAttrib("placeholder", "Twitter");
        $twitter->setAttrib("id", "twitter");
        $twitter->removeDecorator('Label');
        $twitter->setDecorators(array('ViewHelper'));
        
        $linkedin = new Zend_Form_Element_Text("linkedin");
        $linkedin->setAttrib("class", "form-control");
        $linkedin->setAttrib("placeholder", "Linkedin");
        $linkedin->setAttrib("id", "linkedin");
        $linkedin->removeDecorator('Label');
        $linkedin->setDecorators(array('ViewHelper'));
        
        $google = new Zend_Form_Element_Text("google");
        $google->setAttrib("class", "form-control");
        $google->setAttrib("placeholder", "Google+");
        $google->setAttrib("id", "google");
        $google->removeDecorator('Label');
        $google->setDecorators(array('ViewHelper'));
        
        $mobile = new Zend_Form_Element_Text("mobile");
        $mobile->addValidator(new Zend_Validate_Digits);
        $mobile->setAttrib("class", "form-control");
        $mobile->setAttrib("placeholder", "Mobile");
        $mobile->setAttrib("id", "mobile");
        $mobile->removeDecorator('Label');
        $mobile->setDecorators(array('ViewHelper'));
        
       $image = new Zend_Form_Element_File('image');
       $image->setDestination(APPLICATION_PATH."/../public/upload/profile");
       $image->addValidator('Count', false, 1);
       $image->addValidator('Size', false, 2002400);
       $image->addValidator('Extension', false, 'jpg,png,gif');
       $image->setAttrib("class", "form-control");
       $image->setAttrib("id", "image");
       $image->removeDecorator('Label');
       $image->removeDecorator('Error');
       
       $removeImage = new Zend_Form_Element_Checkbox("removeImage");
       $removeImage->setAttrib("id", "removeImage");
       $removeImage->removeDecorator('Label');
       $removeImage->setDecorators(array('ViewHelper'));
        
       //for graduates
       $branch = new Zend_Form_Element_Select("branch");
       $branch->setRequired();
       $branch->addMultiOption("","Select Branch");
       $branchesModel = new Administration_Model_Branch();
        $branches = $branchesModel->listBranches();
        foreach($branches as $branche){
            
            $branch->addMultiOption($branche['id'],$branche['name']);
        }
       $branch->setAttrib("class", "form-control");
       $branch->setAttrib("id", "branch");
       $branch->removeDecorator('Label');
       $branch->setDecorators(array('ViewHelper'));
       
       $intake = new Zend_Form_Element_Select("intake");
       $intake->setRequired();
       $intake->addMultiOption("","Select Intake");
       //list intakes
        $intakesModel = new Administration_Model_Intake();
        $intakes = $intakesModel->listIntakes();
        
       foreach($intakes as $intakee){
            $intake->addMultiOption($intakee['id'],$intakee['name']);
        }
       $intake->setAttrib("class", "form-control");
       $intake->setAttrib("id", "intake");
       $intake->removeDecorator('Label');
       $intake->setDecorators(array('ViewHelper'));
       
       $track = new Zend_Form_Element_Select("track");
       $track->setRequired();
       $track->addMultiOption("","Select Track");
       
       $tracksModel = new Administration_Model_Track();
        $tracks = $tracksModel->listTracks();
        foreach($tracks as $tracke){
            $track->addMultiOption($tracke['id'],$tracke['name']);
        }
        
       $track->setAttrib("class", "form-control");
       $track->setAttrib("id", "track");
       $track->removeDecorator('Label');
       $track->setDecorators(array('ViewHelper'));
       
       $submit = new Zend_Form_Element_Submit("submit");
       $submit->setAttrib("class", "btn btn-default");
       
       $this->addElements(array($id,$firstName,$lastName,$email,$password,$passwordRetype,$gender,$image,$removeImage,
                          $isEnabled,$roleId,$jobTitle,$description,$facebook,$linkedin,$twitter,$google,$branch,$intake,$track,$submit));
        
    }


}

