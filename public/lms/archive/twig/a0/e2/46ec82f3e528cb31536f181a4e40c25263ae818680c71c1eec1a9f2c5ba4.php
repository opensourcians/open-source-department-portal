<?php

/* default/layout/page_body.tpl */
class __TwigTemplate_a0e246ec82f3e528cb31536f181a4e40c25263ae818680c71c1eec1a9f2c5ba4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (((isset($context["actions"]) ? $context["actions"] : null) != "")) {
            // line 3
            echo "    <div class=\"actions\">
        ";
            // line 4
            echo (isset($context["actions"]) ? $context["actions"] : null);
            echo "
    </div>
";
        }
        // line 7
        echo "
";
        // line 8
        echo (isset($context["flash_messages"]) ? $context["flash_messages"] : null);
        echo "

";
        // line 11
        if (((isset($context["header"]) ? $context["header"] : null) != "")) {
            // line 12
            echo "    <div class=\"page-header\">
        <h1>";
            // line 13
            echo (isset($context["header"]) ? $context["header"] : null);
            echo "</h1>
    </div>
";
        }
        // line 16
        echo "
";
        // line 18
        if (((isset($context["message"]) ? $context["message"] : null) != "")) {
            // line 19
            echo "    <section id=\"messages\">
        ";
            // line 20
            echo (isset($context["message"]) ? $context["message"] : null);
            echo "
    </section>
";
        }
    }

    public function getTemplateName()
    {
        return "default/layout/page_body.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 20,  54 => 19,  52 => 18,  49 => 16,  43 => 13,  40 => 12,  38 => 11,  33 => 8,  30 => 7,  24 => 4,  21 => 3,  19 => 2,);
    }
}
