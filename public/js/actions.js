/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function()
{      
        $('.selectAll').click(function() {
            
            var checkboxes = $(this).closest('.row').find(':checkbox');
            if($(this).prop('checked')) {
                checkboxes.prop('checked', true);
              } else {
                checkboxes.prop('checked', false);
              }
        });
        $('#selectAll').click(function() {
            
            var checkboxes = $(this).closest('.form-group').find(':checkbox');
            if($(this).prop('checked')) {
                checkboxes.prop('checked', true);
              } else {
                checkboxes.prop('checked', false);
              }
        });
        $("#alert_div").hide();
        $('#build_action').click(function()
	{
            $("form#buildForm").submit();
	});
	$('#leave_action').click(function()
	{
		$("#alert_div").hide();
                
	});
        
        $("i[class='glyphicon glyphicon-remove']").css('cursor', 'pointer');
       
        $("#delete_div").hide();
        
        $("i[class='glyphicon glyphicon-remove']").bind("click", showDiv);
	$('#delete_action').click(function()
	{
		
        $("#delete_div").hide();
        deleteItem();
        
	});
	$('#keep_action').click(function()
	{
		$("#delete_div").hide();
	});
        
        $( 'textarea#ckeditor_standard' ).ckeditor({width:'99%', height: '150px', toolbar: [
		{ name: 'document', items: [ 'Source', '-', 'Preview', '-', 'Templates' ] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
		[ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],			// Defines toolbar group without name.
		{ name: 'basicstyles', items: [ 'Bold', 'Italic' ,'Underline','Strike','Subscript','Superscript','-','RemoveFormat'] },
                { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent',
                '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
                { name: 'links', items : [ 'Link','Unlink'] },
                { name: 'colors', items : [ 'TextColor','BGColor' ] },
                
	]});
    $( 'textarea#ckeditor_full' ).ckeditor({width:'98%', height: '150px'});
    
        
      var div;
      div = $( "#graduateInfo" ).detach();
      if($("#roleId").find(":selected").text() != "registered" && $("#roleId").find(":selected").val() != ''){
                if ( div ) {
                  div.appendTo( "#graduateContainer" );
                  div = null;
                  }
              }
      $( "#roleId" ).change(function() {
          if($(this).find(":selected").text() != "registered" && $("#roleId").find(":selected").val() != ''){
                if ( div ) {
                  div.appendTo( "#graduateContainer" );
                  div = null;
                  }
        } else {
                if ( ! div ) {
                div = $( "#graduateInfo" ).detach();
                }
          }
            
      });
      
      
     
});
function showAlert(){
    $("#alert_div").show();
}
function showDiv(){
    $("#delete_div").show();
    var idArr = $(this).attr('id').split("_");
     id = idArr[1];
}

function deleteItem()
{
   		$.ajax(
		{
			type : "GET",
			url  : baseUrl + "/" + originalModule +"/" + originalController + "/delete/id/"+id,
			dataType: "json",
			success : function()
			{
						window.location = baseUrl + "/" + originalModule +"/" + originalController + "/list/";
					  },
			error : function(xhr, ajaxOptions, thrownError)
			{
                            alert(xhr.status);
                            alert(xhr.responseText);
                            alert(thrownError);
			}
		});
	}	