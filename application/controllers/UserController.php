<?php

class UserController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {

    }

    public function chamiloAction()
    {
        //$errors = [];
        $auth = Zend_Auth::getInstance();
        $userInfo = $auth->getIdentity();
        $sso_target = $this->_request->getParam('sso_target');
        function loginChamilo($userEmail, $sso_target, $request, &$errors)
        {
            $my_chamilo_server = $_SERVER['HTTP_HOST'];
            $master_auth_uri = $my_chamilo_server . '/?q=user';
            // Creating an array cookie that will be sent to Chamilo
            $userModel = new Administration_Model_User();
            $account = $userModel->getChamiloInfo($userEmail);
            if (!empty($account)) {
                $sso = array(
                    'username' => $account[0]['username'],
                    'secret' => sha1($account[0]['password']),
                    'master_domain' => $my_chamilo_server,
                    'master_auth_uri' => $master_auth_uri,
                    'lifetime' => time() + 3600,
                    'target' => $sso_target
                );
                $cookie = base64_encode(serialize($sso));
                $url = "http://" . $master_auth_uri;
                $final_url = $sso_target . "?" . 'sso_referer=' . urlencode($url) . '&sso_cookie=' . urlencode($cookie);
                $request->redirect($final_url);
            } else {
                $errors['Access Denied'] = "Sorry, your email isn't associated with any LMS account";
            }
        }

        if (empty($userInfo)) {
            $loginForm = new Application_Form_Login();


            if ($this->getRequest()->isPost()) {
                if ($loginForm->isValid($this->getRequest()->getParams())) {
                    $email = $loginForm->getValue("loginEmail");
                    $password = $loginForm->getValue("loginPassword");
                    $remember = $loginForm->getValue("remember");

                    //set up the auth adapter
                    // get the default db adapter
                    $db = Zend_Db_Table::getDefaultAdapter();

                    //create the auth adapter
                    $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'user', 'email', 'password');
                    //set the email and password
                    $authAdapter->setIdentity($email);
                    $authAdapter->setCredential(md5($password));
                    //authenticate
                    $result = $authAdapter->authenticate();
                    if ($result->isValid()) {
                        $auth = Zend_Auth::getInstance();
                        $storage = $auth->getStorage();
                        $storage->write($authAdapter->getResultRowObject(array('email', 'id', 'roleId', 'firstName', 'lastName', 'image', 'isEnabled')));
                        $userInfo = $auth->getIdentity();
                        if (!$userInfo->isEnabled) {
                            $errors['Account Error'] = "Sorry, but your account is deactivated";
                            $auth->clearIdentity();
                        } else {
                            $seconds = 60 * 60 * 24 * 30; // one month
                            if ($remember) {
                                $saveHandler = Zend_Session::getSaveHandler();
                                $saveHandler->setLifetime($seconds)
                                    ->setOverrideLifetime(true);
                                Zend_Session::RememberMe($seconds);
                            } else {
                                Zend_Session::ForgetMe();
                            }
                            loginChamilo($email, $sso_target, $this, $errors);
                        }
                    } else {
                        $errors['Wrong Credentials'] = "Sorry, your email or password was incorrect";
                    }
                } else {
                    $errors['Email Error'] = "Sorry, Please Enter a valid Email";
                }
            }
        }
        else{
            loginChamilo($userInfo->email, $sso_target, $this, $errors);
        }
        $this->view->loginForm = $loginForm;
        $this->view->errors = $errors;

    }

    public function loginAction()
    {

        if ($this->getRequest()->isXmlHttpRequest()) {
            //diable layout and view rendering as Ajax is used
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->layout->disableLayout();

            if ($this->getRequest()->isPost()) {

                $loginForm = new Application_Form_Login();
                if ($loginForm->isValid($this->getRequest()->getParams())) {

                    $email = $loginForm->getValue("loginEmail");
                    $password = $loginForm->getValue("loginPassword");
                    $remember = $loginForm->getValue("remember");

                    //set up the auth adapter
                    // get the default db adapter
                    $db = Zend_Db_Table::getDefaultAdapter();

                    //create the auth adapter
                    $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'user', 'email', 'password');
                    //set the email and password
                    $authAdapter->setIdentity($email);
                    $authAdapter->setCredential(md5($password));
                    //authenticate
                    $result = $authAdapter->authenticate();
                    //$session_info = new Zend_Session_Namespace("myinfo");
                    //$session_info->ali = 5;
                    if ($result->isValid()) {
                        $auth = Zend_Auth::getInstance();
                        $storage = $auth->getStorage();
                        $storage->write($authAdapter->getResultRowObject(array('email', 'id', 'roleId', 'firstName', 'lastName', 'image', 'isEnabled')));
                        $userInfo = $auth->getIdentity();
                        if (!$userInfo->isEnabled) {
                            $response = array("loginStatus" => "failed", "message" => "Sorry, but your account is deactivated");
                            $auth->clearIdentity();
                            return $this->_helper->json($response);
                        }
                        $seconds = 60 * 60 * 24 * 30; // one month
                        if ($remember) {
                            $saveHandler = Zend_Session::getSaveHandler();
                            $saveHandler->setLifetime($seconds)
                                ->setOverrideLifetime(true);
                            Zend_Session::RememberMe($seconds);
                        } else {
                            Zend_Session::ForgetMe();
                        }
                        $response = array("loginStatus" => "success");
                        return $this->_helper->json($response);
                    } else {

                        $response = array("loginStatus" => "failed", "message" => "Sorry, your email or password was incorrect");
                        return $this->_helper->json($response);
                    }
                } else {

                    $response = array("loginStatus" => "failed", "message" => "Sorry, Please Eneter a vaild Email");
                    return $this->_helper->json($response);
                }
            }
        }
    }

    public function logoutAction()
    {
        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();
        $this->redirect("/index/index");
    }


    public function profileAction() {
        $authorization = Zend_Auth::getInstance();
        if($authorization->hasIdentity()){
            $identity = $authorization->getIdentity();
            //Get instances
            $userForm = new Administration_Form_User();
            $userModel = new Administration_Model_User();
            $roleModel = new Administration_Model_Role();
            
            //remove required from password
        $password = $userForm->getElement("password");
        $password->setRequired(FALSE);
        $password->removeValidator("stringlength");

        $passwordRetype = $userForm->getElement("passwordRetype");
        $passwordRetype->setRequired(FALSE);
        $passwordRetype->removeValidator("identical");

        //disable editing email address
        $email = $userForm->getElement("email");
        $email->setAttrib("disabled", "disable");
        $email->removeValidator(new Zend_Validate_Db_NoRecordExists(array("table" => "user", "field" => "email")));

        //remove role & intake & track & isEnabled
        $userForm->removeElement("roleId");
        $userForm->removeElement("isEnabled");
        $this->view->isGraduate = true;
       
        if($identity->roleId != $roleModel->getSuperAdminRoleId() and
                $identity->roleId != $roleModel->getAdminRoleId() and
                $identity->roleId != $roleModel->getGraduateRoleId() and
                $identity->roleId != $roleModel->getStudentRoleId()) {
            $userForm->removeElement("branch");
            $userForm->removeElement("track");
            $userForm->removeElement("intake");
            
            $this->view->isGraduate = false;
            
        }
        
        $user = $userModel->getUserById($identity->id);
        $this->view->image = $user[0]['image'];
        //populate the form with data
        $userForm->populate($user[0]);
            
            if ($this->getRequest()->isPost()) {
            if ($userForm->isValidPartial($this->getRequest()->getParams())) {

                $result = $userModel->editUser($userForm->getValue("id"), $userForm->getValues());

                $this->view->messages = array('Your changes are saved successfully!');
            } else {
                $this->view->errors = $userForm->getMessages();
            }
        }
        
        $this->view->form = $userForm;
        }

    }
    public function registerAction()
    {
        if (!empty($this->view->identity)) {
            $this->redirect("/");
        }
        $userForm = new Application_Form_Registration();
        $this->view->form = $userForm;
        $user = OSD_Facebook::getUser();

        if ($user) {
            try {
                $userProfile = OSD_Facebook::api('/me', 'GET');
                $this->view->logoutUrl = OSD_Facebook::getLogoutUrl();
            } catch (FacebookApiException $e) {
                $loginUrl = OSD_Facebook::getLoginUrl(array('scope' => 'email'));
                $this->view->loginUrl = $loginUrl;
            }
        } else {

            // No user, print a link for the user to login
            $loginUrl = OSD_Facebook::getLoginUrl(array('scope' => 'email'));

            $this->view->loginUrl = $loginUrl;
        }
        if (!empty($userProfile)) {

            $data = array();
            $data['firstName'] = $userProfile['first_name'];
            $data['lastName'] = $userProfile['last_name'];
            $data['email'] = $userProfile['email'];
            $data['gender'] = $userProfile['gender'];
            if (!empty($userProfile['work']))
                $data['jobTitle'] = $userProfile['work'][0]['position']['name'] . "@" . $userProfile['work'][0]['employer']['name'];
            $data['facebook'] = $userProfile['link'];
            $userForm->populate($data);
        }
        if ($this->getRequest()->isPost()) {
            if ($userForm->isValid($this->getRequest()->getParams())) {
                $userModel = new Administration_Model_User();
                //add RoleId and isEnabled
                $userInfo = $userForm->getValues();
                $userInfo['roleId'] = 3;
                $userInfo['isEnabled'] = 1;

                $userId = $userModel->addUser($userInfo);

                if ($userId) {

                    $this->_flashMessenger->addMessage('User added successfully!');
                    $this->redirect("/");
                }
            } else {
                $this->view->errors = $userForm->getMessages();
            }
        }
    }

}
