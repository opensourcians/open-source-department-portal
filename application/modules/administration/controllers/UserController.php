<?php

class Administration_UserController extends Zend_Controller_Action {

    public function init() {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction() {
        $this->redirect("/administration/user/list");
    }

    public function listAction() {
        $userModel = new Administration_Model_User();
        $users = $userModel->listUsers();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($users);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
    }

    public function addAction() {
        $userForm = new Administration_Form_User();


        $this->view->form = $userForm;
        $this->view->post = "/administration/user/add";
        $this->view->viewName = "Add User";


        if ($this->getRequest()->isPost()) {
            if ($userForm->isValidPartial($this->getRequest()->getParams())) {
                $userModel = new Administration_Model_User();

                $userId = $userModel->addUser($userForm->getValues());

                if ($userId) {
                    $this->_flashMessenger->addMessage('User added successfully!');
                    $this->redirect("/administration/user/list");
                }
            } else {
                $this->view->errors = $userForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction() {
        //Get instances
        $userForm = new Administration_Form_User();
        $userModel = new Administration_Model_User();


        //get user id from request
        $id = $this->getRequest()->getParam('id');

        //get user information
        $user = $userModel->getUserById($id);

        //send values to view

        $this->view->post = "/administration/user/edit";
        $this->view->viewName = "Edit User";

        //remove required from password
        $password = $userForm->getElement("password");
        $password->setRequired(FALSE);
        $password->removeValidator("stringlength");

        $passwordRetype = $userForm->getElement("passwordRetype");
        $passwordRetype->setRequired(FALSE);
        $passwordRetype->removeValidator("identical");

        //disable editing email address
        $email = $userForm->getElement("email");
        $email->setAttrib("disabled", "disable");
        $email->removeValidator(new Zend_Validate_Db_NoRecordExists(array("table" => "user", "field" => "email")));


        if ($user) {
            $this->view->image = $user[0]['image'];
            $userForm->populate($user[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no user with this id!');
            $this->redirect("/administration/user/list");
        }

        if ($this->getRequest()->isPost()) {
            if ($userForm->isValidPartial($this->getRequest()->getParams())) {

                $result = $userModel->editUser($userForm->getValue("id"), $userForm->getValues());

                $this->_flashMessenger->addMessage('User edited successfully!');
                $this->redirect("/administration/user/list");
            } else {
                $this->view->errors = $userForm->getMessages();
            }
        }

        $this->view->form = $userForm;
        $this->render("form");
    }

    public function deleteAction() {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $roleModel = new Administration_Model_User();
        $result = $roleModel->deleteUser($id);
        if ($result) {
            $this->_flashMessenger->addMessage('User deleted successfully!');
            echo $result;
        }
    }

    public function profileAction() {
        //get user info
        $objAuth = Zend_Auth::getInstance();
        $userInfo = $objAuth->getIdentity();


        if ($userInfo) {
            //get user info
            $userModel = new Administration_Model_User();
            $user = $userModel->getUserById($userInfo->id);
            $this->view->user = $user[0];
        }
    }

    public function editprofileAction() {
        //Get instances
        $userForm = new Administration_Form_User();
        $userModel = new Administration_Model_User();


        $objAuth = Zend_Auth::getInstance();
        $userInfo = $objAuth->getIdentity();


        if ($userInfo) {
            //get user info
            
            $user = $userModel->getUserById($userInfo->id);
            $this->view->user = $user[0];
        }

        $this->view->post = "/administration/user/editprofile";
        $this->view->viewName = "Edit Profile";

        //remove required from password
        $password = $userForm->getElement("password");
        $password->setRequired(FALSE);
        $password->removeValidator("stringlength");

        $passwordRetype = $userForm->getElement("passwordRetype");
        $passwordRetype->setRequired(FALSE);
        $passwordRetype->removeValidator("identical");

        //disable editing email address
        $email = $userForm->getElement("email");
        $email->setAttrib("disabled", "disable");
        $email->removeValidator(new Zend_Validate_Db_NoRecordExists(array("table" => "user", "field" => "email")));

        //remove role & intake & track & isEnabled
        $userForm->removeElement("roleId");
        $userForm->removeElement("track");
        $userForm->removeElement("intake");
        $userForm->removeElement("isEnabled");



        if ($user) {
            $this->view->image = $user[0]['image'];
            $userForm->populate($user[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no user with this id!');
            $this->redirect("/administration/user/profile");
        }

        if ($this->getRequest()->isPost()) {
            if ($userForm->isValidPartial($this->getRequest()->getParams())) {

                $result = $userModel->editUser($userForm->getValue("id"), $userForm->getValues());

                $this->_flashMessenger->addMessage('Profile edited successfully!');
                $this->redirect("/administration/user/profile");
            } else {
                $this->view->errors = $userForm->getMessages();
            }
        }

        $this->view->form = $userForm;
    }

}
