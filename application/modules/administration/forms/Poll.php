<?php

class Administration_Form_Poll extends Zend_Form
{

    public function init()
    {
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $pollQuestion = new Zend_Form_Element_Text("pollQuestion");
        $pollQuestion->addFilter(new Zend_Filter_StripTags);
        $pollQuestion->setRequired();
        $pollQuestion->setAttrib("class", "form-control");
        $pollQuestion->setAttrib("placeholder", "Enter Poll Question");
        $pollQuestion->setAttrib("id", "pollQuestion");
        $pollQuestion->removeDecorator('Label');
        $pollQuestion->setDecorators(array('ViewHelper'));
        
        $pollChoiceOne = new Zend_Form_Element_Text("pollChoiceOne");
        $pollChoiceOne->addFilter(new Zend_Filter_StripTags);
        $pollChoiceOne->setRequired();
        $pollChoiceOne->setAttrib("class", "form-control");
        $pollChoiceOne->setAttrib("placeholder", "Enter Choice One");
        $pollChoiceOne->setAttrib("id", "pollChoiceOne");
        $pollChoiceOne->removeDecorator('Label');
        $pollChoiceOne->setDecorators(array('ViewHelper'));
        
        $pollChoiceOneId = new Zend_Form_Element_Hidden("pollChoiceOneId");
        $pollChoiceOneId->setDecorators(array('ViewHelper'));
        
        $pollChoiceTwo = new Zend_Form_Element_Text("pollChoiceTwo");
        $pollChoiceTwo->addFilter(new Zend_Filter_StripTags);
        $pollChoiceTwo->setRequired();
        $pollChoiceTwo->setAttrib("class", "form-control");
        $pollChoiceTwo->setAttrib("placeholder", "Enter Choice Two");
        $pollChoiceTwo->setAttrib("id", "pollChoiceTwo");
        $pollChoiceTwo->removeDecorator('Label');
        $pollChoiceTwo->setDecorators(array('ViewHelper'));
        
        $pollChoiceTwoId = new Zend_Form_Element_Hidden("pollChoiceTwoId");
        $pollChoiceTwoId->setDecorators(array('ViewHelper'));
        
        $pollChoiceThree = new Zend_Form_Element_Text("pollChoiceThree");
        $pollChoiceThree->addFilter(new Zend_Filter_StripTags);
        $pollChoiceThree->setAttrib("class", "form-control");
        $pollChoiceThree->setAttrib("placeholder", "Enter Choice Three");
        $pollChoiceThree->setAttrib("id", "pollChoiceThree");
        $pollChoiceThree->removeDecorator('Label');
        $pollChoiceThree->setDecorators(array('ViewHelper'));
        
        $pollChoiceThreeId = new Zend_Form_Element_Hidden("pollChoiceThreeId");
        $pollChoiceThreeId->setDecorators(array('ViewHelper'));
        
        $pollChoiceFour = new Zend_Form_Element_Text("pollChoiceFour");
        $pollChoiceFour->addFilter(new Zend_Filter_StripTags);
        $pollChoiceFour->setAttrib("class", "form-control");
        $pollChoiceFour->setAttrib("placeholder", "Enter Choise Four");
        $pollChoiceFour->setAttrib("id", "pollChoiceFour");
        $pollChoiceFour->removeDecorator('Label');
        $pollChoiceFour->setDecorators(array('ViewHelper'));
        
        $pollChoiceFourId = new Zend_Form_Element_Hidden("pollChoiceFourId");
        $pollChoiceFourId->setDecorators(array('ViewHelper'));
        
        $pollChoiceFive = new Zend_Form_Element_Text("pollChoiceFive");
        $pollChoiceFive->addFilter(new Zend_Filter_StripTags);
        $pollChoiceFive->setAttrib("class", "form-control");
        $pollChoiceFive->setAttrib("placeholder", "Enter Choice Five");
        $pollChoiceFive->setAttrib("id", "pollChoiceFive");
        $pollChoiceFive->removeDecorator('Label');
        $pollChoiceFive->setDecorators(array('ViewHelper'));
        
        $pollChoiceFiveId = new Zend_Form_Element_Hidden("pollChoiceFiveId");
        $pollChoiceFiveId->setDecorators(array('ViewHelper'));
        
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
       
       $this->addElements(array($id,$pollQuestion,$pollChoiceOne,$pollChoiceOneId,$pollChoiceTwo,$pollChoiceTwoId,
           $pollChoiceThree,$pollChoiceThreeId,$pollChoiceFour,$pollChoiceFourId,$pollChoiceFive,$pollChoiceFiveId,$submit));
    }


}

