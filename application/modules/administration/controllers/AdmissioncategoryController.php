<?php

class Administration_AdmissioncategoryController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/admissioncategory/list");
      
    }

    public function listAction()
    {
        $admissionCategoryModel = new Administration_Model_AdmissionCategory();
        $admissionCategories = $admissionCategoryModel->listAdmissionCategories();

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($admissionCategories);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
        
        
    }

    public function addAction()
    {
        $admissionCategoryForm = new Administration_Form_AdmissionCategory();


        $this->view->form = $admissionCategoryForm;
        $this->view->post = "/administration/admissioncategory/add";
        $this->view->viewName = "Add Admission Category";


        if ($this->getRequest()->isPost()) {
            if ($admissionCategoryForm->isValid($this->getRequest()->getParams())) {
                $admissionCategoryModel = new Administration_Model_AdmissionCategory();

                $admissionCategoryId = $admissionCategoryModel->addAdmissionCategory($admissionCategoryForm->getValues());

                if ($admissionCategoryId) {
                    $this->_flashMessenger->addMessage('Admission Category added successfully!');
                    $this->redirect("/administration/admissioncategory/list");
                }
            } else {
                $this->view->errors = $admissionCategoryForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction()
    {
         //Get instances
        $admissionCategoryForm = new Administration_Form_AdmissionCategory();
        $admissionCategoryModel = new Administration_Model_AdmissionCategory();


        //get user id from request
        $id = $this->getRequest()->getParam('id');

        //get user information
        $admissionCategory = $admissionCategoryModel->getAdmissionCategoryById($id);

        //send values to view
        $this->view->post = "/administration/admissioncategory/edit";
        $this->view->viewName = "Edit Admission Category";
        
        
        if ($this->getRequest()->isPost()) {
            if ($admissionCategoryForm->isValid($this->getRequest()->getParams())) {

                $admissionCategoryModel->editAdmissionCategory($admissionCategoryForm->getValue("id"), $admissionCategoryForm->getValues());

                $this->_flashMessenger->addMessage('Admission Category edited successfully!');
                $this->redirect("/administration/admissioncategory/list");
            } else {
                $this->view->errors = $admissionCategoryForm->getMessages();
            }
        }

        if ($admissionCategory) {
            $admissionCategoryForm->populate($admissionCategory[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no admission category with this id!');
            $this->redirect("/administration/admissioncategory/list");
        }

        

        $this->view->form = $admissionCategoryForm;
        $this->render("form");
    }

    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");

        $admissionCategoryModel = new Administration_Model_AdmissionCategory();
        $result = $admissionCategoryModel->deleteAdmissionCategory($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Admission Category deleted successfully!');
            echo $result;
        }
    }


}

