<?php

class Administration_Form_Page extends Zend_Form
{

    public function init()
    {
        $id = new Zend_Form_Element_Hidden("id");
        $id->setDecorators(array('ViewHelper'));
        
        $pageTitle = new Zend_Form_Element_Text("pageTitle");
        $pageTitle->addFilter(new Zend_Filter_StripTags);
        $pageTitle->setRequired();
        $pageTitle->setAttrib("class", "form-control");
        $pageTitle->setAttrib("placeholder", "Enter Page Title");
        $pageTitle->setAttrib("id", "pageTitle");
        $pageTitle->removeDecorator('Label');
        $pageTitle->setDecorators(array('ViewHelper'));
        
        $body = new Zend_Form_Element_Textarea("body");
        $body->setAttrib("class", "form-control");
        $body->setAttrib("id", "ckeditor_full");
        $body->removeDecorator('Label');
        $body->setDecorators(array('ViewHelper'));
        
        $submit = new Zend_Form_Element_Submit("submit");
        $submit->setAttrib("class", "btn btn-default");
       
       $this->addElements(array($id,$pageTitle,$body,$submit));
    }


}

