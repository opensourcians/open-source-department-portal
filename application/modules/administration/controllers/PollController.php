<?php

class Administration_PollController extends Zend_Controller_Action
{

    /**
     * FlashMessenger
     *
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;

    public function init() {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction() {
        $this->redirect("/administration/poll/list");
    }

    public function listAction() {
        $pollModel = new Administration_Model_Poll();
        $polls = $pollModel->listPolls();
        

        $page = $this->_getParam('page', 1);
        $paginator = Zend_Paginator::factory($polls);
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);
        
        $this->view->messages = $this->_flashMessenger->getMessages();
        $this->view->paginator = $paginator;
    }

    public function addAction() {
        
        $pollForm = new Administration_Form_Poll();

        $this->view->form = $pollForm;
        $this->view->post = "/administration/poll/add";
        $this->view->viewName = "Add Poll";


        if ($this->getRequest()->isPost()) {
            if ($pollForm->isValid($this->getRequest()->getParams())) {
                $pollModel = new Administration_Model_Poll();
                $pollId = $pollModel->addPoll($pollForm->getValue("pollQuestion"));
                
                if ($pollId) {
                    $pollChoicesModel = new Administration_Model_PollChoices();
                    $pollChoicesModel->addPollChoices($pollId, $pollForm->getValue("pollChoiceOne"));
                    $pollChoicesModel->addPollChoices($pollId, $pollForm->getValue("pollChoiceTwo"));
                    
                    if($pollForm->getValue("pollChoiceThree")){
                        $pollChoicesModel->addPollChoices($pollId, $pollForm->getValue("pollChoiceThree"));
                    }
                    
                    if($pollForm->getValue("pollChoiceFour")){
                        $pollChoicesModel->addPollChoices($pollId, $pollForm->getValue("pollChoiceFour"));
                    }
                    
                    if($pollForm->getValue("pollChoiceFive")){
                        $pollChoicesModel->addPollChoices($pollId, $pollForm->getValue("pollChoiceFive"));
                    }
                    
                    $this->_flashMessenger->addMessage('Poll added successfully!');
                    $this->redirect("/administration/poll/list");
                }
            } else {

                $this->view->errors = $pollForm->getMessages();
            }
        }

        $this->render("form");
    }

    public function editAction() {
        //Get instances
        $pollForm = new Administration_Form_Poll();
        $pollModel = new Administration_Model_Poll();
        $pollChoicesModel = new Administration_Model_PollChoices();

        //get page id from request
        $id = $this->getRequest()->getParam('id');

        //get page information
        $poll = $pollModel->getPollById($id);
        $pollChoices =  $pollChoicesModel->getChoicesByPollId($id);
        
        //add choices
        $poll[0]["pollChoiceOne"] = $pollChoices[0]["pollChoice"];
        $poll[0]["pollChoiceOneId"] = $pollChoices[0]["id"];
        
        $poll[0]["pollChoiceTwo"] = $pollChoices[1]["pollChoice"];
        $poll[0]["pollChoiceTwoId"] = $pollChoices[1]["id"];
        
        if(!empty($pollChoices[2]["pollChoice"])){
            $poll[0]["pollChoiceThree"] = $pollChoices[2]["pollChoice"];
            $poll[0]["pollChoiceThreeId"] = $pollChoices[2]["id"];
        }
        
        if(!empty($pollChoices[3]["pollChoice"])){
            $poll[0]["pollChoiceFour"] = $pollChoices[3]["pollChoice"];
            $poll[0]["pollChoiceFourId"] = $pollChoices[3]["id"];
        }
        
        if(!empty($pollChoices[4]["pollChoice"])){
            $poll[0]["pollChoiceFive"] = $pollChoices[4]["pollChoice"];
            $poll[0]["pollChoiceFiveId"] = $pollChoices[4]["id"];
        }

        //send values to view
        $this->view->post = "/administration/poll/edit";
        $this->view->viewName = "Edit Poll";

        if ($poll) {
            $pollForm->populate($poll[0]);
        } else {
            $this->_flashMessenger->addMessage('Sorry!, but there is no poll with this id!');
            $this->redirect("/administration/poll/list");
        }

        if ($this->getRequest()->isPost()) {
            if ($pollForm->isValid($this->getRequest()->getParams())) {

                $result = $pollModel->editPoll($pollForm->getValue("id"), $pollForm->getValue("pollQuestion"));
                $pollChoicesModel->editPollChoises($pollForm->getValue("id"), $pollForm->getValues());
                $this->_flashMessenger->addMessage('Poll edited successfully!');
                $this->redirect("/administration/poll/list");
            }
        }

        $this->view->form = $pollForm;
        $this->render("form");
    }

    public function deleteAction() {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");
        $pollModel = new Administration_Model_Poll();
        $result = $pollModel->deletePoll($id);
        if ($result) {
            $this->_flashMessenger->addMessage('Poll deleted successfully!');
            echo $result;
        }
    }




}

