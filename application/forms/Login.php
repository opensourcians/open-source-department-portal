<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        //Email
        $email = new Zend_Form_Element_Text("loginEmail");
        $email->setRequired();
        $email->addValidator(new Zend_Validate_EmailAddress);
        $email->addFilter(new Zend_Filter_StringTrim);
        $email->addFilter(new Zend_Filter_StripTags);
        $email->setAttrib("id", "loginEmail");
        $email->setAttrib("class", "form-control login-field");
        $email->setAttrib("placeholder", "Enter your email");
        $email->removeDecorator('label');
        $email->setDecorators(array('ViewHelper'));
        
        //Password
        $password = new Zend_Form_Element_Password("loginPassword");
        $password->setRequired();
        $password->addFilter(new Zend_Filter_StringTrim);
        $password->setAttrib("id", "loginPassword");
        $password->setAttrib("class", "form-control login-field");
        $password->setAttrib("placeholder", "Password");
        $password->removeDecorator('label');
        $password->setDecorators(array('ViewHelper'));
        
        $remember = new Zend_Form_Element_Checkbox("remember");
        $remember->setAttrib("id", "remember");
        $remember->removeDecorator('Label');
        $remember->setDecorators(array('ViewHelper'));
        
        //Submit
        $submit = new Zend_Form_Element_Submit("loginSubmit");
        $submit->setAttrib("id", "loginSubmit");
        $submit->setLabel("Login");
        $submit->setAttrib("class", "btn btn-default");
        $submit->setDecorators(array('ViewHelper'));
        
        
        //Add Elements
        $this->addElements(array($email,$password,$remember,$submit));
    }


}

