<?php

class Administration_Model_Projectportal extends Zend_Db_Table_Abstract
{

    protected $_name="project";
    public function listProject() 
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        return $this->fetchAll($select)->toArray();
    }
    //public function 
    
    public function listtrack()
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from("project_track");
        return $this->fetchAll($select)->toArray();
    }
    
    public function listteam()
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from("project_team");
        return $this->fetchAll($select)->toArray();
    }
    
    public function listtag()
    {
        $select = $this->select()->setIntegrityCheck(FALSE);
        $select->from("project_tag");
        return $this->fetchAll($select)->toArray();
    }
        
    public function addProject($id,$name,$track,$intake,$desc,$history,$doc,$img)
    {
        $data = array('pid'=>$id, 'pname'=>$name , 'pdesc'=>$desc , 'pdoc'=>$doc , 'pimage'=>$img , 'tname'=>$track ,'tpid'=>$intake , 'phis'=> $history );
        $this->insert($data);
    }
    
    public function editProject($name,$track,$intake,$desc,$history,$doc,$img,$id) 
    {
        $data = array('pname'=>$name , 'pdesc'=>$desc , 'pdoc'=>$doc , 'pimage'=>$img , 'tname'=>$track ,'tpid'=>$intake , 'phis'=> $history );
        $this->update($data,"pid=$id");     
    }
    
    public function deleteProject($id)
    {
        $this->delete("pid=$id");
    }
    
}