<?php

/* default/layout/main_header.tpl */
class __TwigTemplate_480455661fea8afdf196dceb92875446e101c0dbf16886e210f7d76d82bb6775 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html lang=\"";
        // line 3
        echo (isset($context["document_language"]) ? $context["document_language"] : null);
        echo "\" class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>
<html lang=\"";
        // line 5
        echo (isset($context["document_language"]) ? $context["document_language"] : null);
        echo "\" class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>
<html lang=\"";
        // line 7
        echo (isset($context["document_language"]) ? $context["document_language"] : null);
        echo "\" class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang=\"";
        // line 9
        echo (isset($context["document_language"]) ? $context["document_language"] : null);
        echo "\" class=\"no-js\"> <!--<![endif]-->
<head>
    ";
        // line 11
        $this->displayBlock('head', $context, $blocks);
        // line 14
        echo "</head>
<body dir=\"";
        // line 15
        echo (isset($context["text_direction"]) ? $context["text_direction"] : null);
        echo "\" class=\"";
        echo (isset($context["section_name"]) ? $context["section_name"] : null);
        echo " ";
        echo (isset($context["login_class"]) ? $context["login_class"] : null);
        echo " gradient\">
<noscript>";
        // line 16
        echo get_lang("NoJavascript");
        echo "</noscript>
<div class=\"container\">
    <div class=\"row clearfix\" id=\"header\">
        <div class=\"col-md-12 column\" id=\"social\">
            <a href=\"/?>\"><img class=\"visible-lg\" src=\"/images/logo.png\" id=\"logo\"></a>

            <div id=\"socialText\">Keep in touch</div>
            <ul>
                <li><a target=\"_blank\" href=\"#\" class=\"facebook\"></a></li>
                <li><a target=\"_blank\" href=\"#\" class=\"google\"></a></li>
                <li><a target=\"_blank\" href=\"#\" class=\"twitter\"></a></li>
                <li><a target=\"_blank\" href=\"#\" class=\"linkedin\"></a></li>
            </ul>
            <div id=\"colorText\" class=\"visible-lg\">Change Color</div>
            <ul id=\"color\" class=\"visible-lg\">
                <li><a class=\"red\" onClick=\"page_style('red');\"></a></li>
                <li><a class=\"blue\" onClick=\"page_style('blue');\"></a></li>
                <li><a class=\"green\" onClick=\"page_style('green');\"></a></li>
                <li><a class=\"yellow\" onClick=\"page_style('yellow');\"></a></li>
            </ul>
        </div>
    </div>
    <div class=\"row clearfix\" id=\"nav\">
        <div class=\"col-md-12 column\">
            <ul class=\"nav nav-pills\">
                ";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menu_items"]) ? $context["menu_items"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 42
            echo "                    ";
            if ($this->getAttribute($context["item"], "sub_items", array(), "any", true, true)) {
                // line 43
                echo "                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 44
                echo $this->getAttribute($context["item"], "label", array());
                echo "<b class=\"caret\"></b></a>
                            <ul class=\"dropdown-menu\">
                                ";
                // line 46
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "sub_items", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_item"]) {
                    // line 47
                    echo "                                <li>
                                    <a href=\"http://52.25.251.24/";
                    // line 48
                    echo $this->getAttribute($context["sub_item"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["sub_item"], "label", array());
                    echo "</a>
                                </li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "                            </ul>
                        </li>
                    ";
            } else {
                // line 54
                echo "                        <li><a href=\"http://52.25.251.24/";
                echo $this->getAttribute($context["item"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["item"], "label", array());
                echo "</a></li>
                    ";
            }
            // line 56
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                <ul class=\"navbar-right\">
                    <li id=\"menuWelcome\">
                        <img class=\"img-circle\" width=\"32px\" height=\"32px\"
                             src=\"";
        // line 60
        echo $this->getAttribute((isset($context["_u"]) ? $context["_u"] : null), "avatar_small", array());
        echo "\"/>
                        Welcome <span class='label label-default'>";
        // line 61
        echo $this->getAttribute((isset($context["_u"]) ? $context["_u"] : null), "complete_name", array());
        echo "</span>
                        <a href=\"";
        // line 62
        echo (isset($context["logout_link"]) ? $context["logout_link"] : null);
        echo "\">
                            <span title=\"Logout\" id=\"logoutIcon\" class=\"glyphicon glyphicon-off\"></span>
                        </a>
                    </li>
                </ul>
            </ul>
        </div>
    </div>
    <!-- end nav -->
    ";
        // line 72
        echo "    ";
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 75
        echo "
    <div class=\"fill\">
";
    }

    // line 11
    public function block_head($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $this->env->loadTemplate("default/layout/head.tpl")->display($context);
        // line 13
        echo "    ";
    }

    // line 72
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 73
        echo "    ";
        echo (isset($context["breadcrumb"]) ? $context["breadcrumb"] : null);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "default/layout/main_header.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 73,  182 => 72,  178 => 13,  175 => 12,  172 => 11,  166 => 75,  163 => 72,  151 => 62,  147 => 61,  143 => 60,  138 => 57,  132 => 56,  124 => 54,  119 => 51,  108 => 48,  105 => 47,  101 => 46,  96 => 44,  93 => 43,  90 => 42,  86 => 41,  58 => 16,  50 => 15,  47 => 14,  45 => 11,  40 => 9,  35 => 7,  30 => 5,  25 => 3,  21 => 1,);
    }
}
