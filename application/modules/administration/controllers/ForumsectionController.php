<?php

class Administration_ForumsectionController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $list = new Administration_Model_Forumsection();
        $this->view->data = $list->listForumsection();
    }

    public function deleteAction()
    {
         $delete = new Administration_Model_Forumsection();
        if($this->_request->isPost())
        {
            $delete->deleteForumsection($this->_request->getParam('id'));
            $this->redirect('/administration/forumsection');
        }
    }

    public function addAction()
    {
        $category = new Administration_Model_Forumcategory();
        $this->view->form = $category->listForumcategory();
        if($this->_request->isPost())
        {
            $add = new Administration_Model_Forumsection();
            $add->addForumsection($this->_request->getParam('name'),
                    $this->_request->getParam('description')
                    ,  $this->_request->getParam('categoryname'));
            $this->redirect("/administration/forumsection");
        }
    }

    public function editAction()
    {
        $cat = new Administration_Model_Forumcategory();
        $this->view->form= $cat->listForumcategory();
        $edit = new Administration_Model_Forumsection();
        $id = $this->_request->getParam('id');
        $this->view->data = $edit->getforumsectionByid($this->_request->getParam('id'));
        if($this->_request->isPost())
        {
            $edit->editForumsection($this->_request->getParam("name"), 
                    $this->_request->getParam('description'),$id, 
                    $this->_request->getParam('categoryname'));
            //echo $this->_request->getParam("sec_name");
            //echo $this->_request->getParam("description");
            $this->redirect("/administration/forumsection/index");
        }
        //var_dump($this->view->data);
        //var_dump($this->view->form);
    }


}







