<?php

class Administration_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        //Send login form to view
        $loginForm = new Application_Form_Login();
        $submit = $loginForm->getElement("loginSubmit");
        $submit->setAttrib("class", "btn btn-primary btn-lg btn-block");
        $this->view->loginForm = $loginForm;
        $this->view->messages = $this->_flashMessenger->getMessages();
        
    }
    
    public function loginAction()
    {
            
        if($this->getRequest()->isPost()){
            
            $loginForm = new Application_Form_Login();
            if($loginForm->isValid($this->getRequest()->getParams())){
                $email = $loginForm->getValue("loginEmail");
                $password = $loginForm->getValue("loginPassword");
                
                //set up the auth adapter
                // get the default db adapter
                $db = Zend_Db_Table::getDefaultAdapter();

                //create the auth adapter
                $authAdapter = new Zend_Auth_Adapter_DbTable($db, 'user','email', 'password');
                //set the email and password
                $authAdapter->setIdentity($email);
                $authAdapter->setCredential(md5($password));
                //authenticate
                $result = $authAdapter->authenticate();
                
                if ($result->isValid()) {
                    $auth = Zend_Auth::getInstance();
                    $storage = $auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject(array('email','id','roleId','firstName','lastName','image','isEnabled' )));
                    $userInfo = $auth->getIdentity();
                    if(!$userInfo->isEnabled){
                        $this->_flashMessenger->addMessage('Sorry, but your account is deactivated');
                        $auth->clearIdentity();
                        return $this->redirect('/administration/index');
                    }
                    return $this->redirect('/administration/dashboard');
                } else {
                    
                    $this->_flashMessenger->addMessage('Sorry, your username or password was incorrect');
                    return $this->redirect('/administration/index');
                }         
            } else {
                
                    $this->_flashMessenger->addMessage('Sorry, Please enter vaild email address');
                    return $this->redirect('/administration/index');
            }
        }
        
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();  
    }
    
    public function logoutAction()
    {
        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();
        $this->redirect("/administration/index/index");
    }
    
}

