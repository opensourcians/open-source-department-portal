$(document).ready(function()
{
    $("#question").hide();
    $(".btn.btn-link.questionCategory").click(function (){
        $("#categorySelect").val($(this).val());
        $("#categories").hide('slide');
        $("#question").show();
        $(".timer").TimeCircles({
            "animation": "smooth",
            "bg_width": 0.4,
            "fg_width": 0.05,
            "circle_bg_color": "#60686F",
             "time": {
        "Days": {
            "text": "Days",
            "color": "#FFCC66",
            "show": false
        },
        "Hours": {
            "text": "Hours",
            "color": "#99CCFF",
            "show": false
        },
        "Minutes": {
            "text": "Minutes",
            "color": "#99CCFF",
            "show": true
        },
        "Seconds": {
            "text": "Seconds",
            "color": "#FF9999",
            "show": true
        }
    }
        });
        displayQuestion();
    })
    
    $("#change").click(function (){
        displayQuestion();
    });
    
    $("#endInterview").click(function (){
        endInterview();
    });
    
    $("#categorySelect").change(function (){
        displayQuestion();
    });
    $("#levelSelect").change(function (){
        displayQuestion();
    });
    
    $('input[type=radio][name=answers]').change(function() {
        result = this.value;
        saveQuestionResult(result)
    });
    
});

function endInterview(){
    $.ajax(
            {
                type: "POST",
                url: baseUrl + "/administration/admissioninterview/end",
                data: {
                   'interviewSecondsNo' : Math.round(-$(".timer").TimeCircles().getTime()),
                },
                dataType: "json",
                success: function(data) {
                    if(data == 'error'){
                        $("#message").html('<div class="alert alert-danger alert-dismissable">\n\
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                                            <strong>Sorry, can not save applicant result!</strong>\n\
                                           </div>'
                            )
                    }else{
                        window.location = baseUrl + "/administration/admissioninterview/result/applicantId/"+data.id;
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                   alert(xhr.status);
                   alert(xhr.responseText);
                   alert(thrownError);
                      
                }
                
        });
}

function saveQuestionResult(result){
    $.ajax(
            {
                type: "POST",
                url: baseUrl + "/administration/admissioninterview/save",
                data: {
                   'questionId' : $("#questionId").val(),
                   'result' : result
                },
                success: function(data) {
                    $("#message").html('<div class="alert alert-success alert-dismissable">\n\
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                                            <strong>Grade saved successfuly!</strong>\n\
                                           </div>'
                            )
                },
                error: function(xhr, ajaxOptions, thrownError) {
//                    alert(xhr.status);
//                    alert(xhr.responseText);
//                    alert(thrownError);
                      $("#message").html('<div class="alert alert-danger alert-dismissable">\n\
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                                            <strong>Sorry, Question grade not saved!</strong>\n\
                                           </div>'
                            )
                }
                
        });
    
}
function displayQuestion(){
        $.ajax(
            {
                type: "POST",
                url: baseUrl + "/administration/admissioninterview/display",
                data: {
                   'categotyId' : $("#categorySelect").val(),
                   'levelId' : $("#levelSelect").val(),
                },
                dataType: "json",
                success: function(data) {
                    $("#message").html('')
                    $("#questionId").val(data.id);
                    $("#interviewQuestion").html('<h2 class="separator"><span class="label label-danger">Q:</span> '+data.title+'</h2><p>'+data.body+'</p>')
                    $('input[type=radio][name=answers]').prop('checked', false);
                },
                error: function(xhr, ajaxOptions, thrownError) {
//                    alert(xhr.status);
//                    alert(xhr.responseText);
//                    alert(thrownError);
                      $("#message").html('<div class="alert alert-danger alert-dismissable">\n\
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n\
                                            <strong>Sorry, We did not find any questions to display!</strong>\n\
                                           </div>'
                            )
                }
                
        });
}
