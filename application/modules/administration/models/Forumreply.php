<?php

class Administration_Model_Forumreply extends Zend_Db_Table_Abstract
{
    protected $_name="Post";
    public function listForumreplyNo($id) {
        $selected = $this->select()
                         ->setIntegrityCheck(FALSE)
                         ->from('Post', 'count(*)')
                         ->where("topic_id=$id");
        return $this->fetchAll($selected)->toArray();
    }
}

