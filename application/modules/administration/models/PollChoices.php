<?php

class Administration_Model_PollChoices extends Zend_Db_Table_Abstract
{
    protected $_name = 'pollChoices';
    protected $_referenceMap    = array(
        'Poll' => array(
            'columns'           => 'pollId',
            'refTableClass'     => 'Administration_Model_Poll',
            'refColumns'        => 'id'
        ));
    
    function listPolls(){
        return $this->fetchAll()->toArray();
    }
    
    function addPollChoices($pollId,$pollChoice){
        $row = $this->createRow();
        $row->pollId = $pollId;
        $row->pollChoice = $pollChoice;
        return $row->save();
    }
    
    function editPollChoises($pollId, $pollChoices) {

        unset($pollChoices['pollQuestion'], $pollChoices['id']);

        $this->update(array('pollChoice' => $pollChoices['pollChoiceOne']), " pollChoices.id =" . $pollChoices['pollChoiceOneId']);
        $this->update(array('pollChoice' => $pollChoices['pollChoiceTwo']), " pollChoices.id =" . $pollChoices['pollChoiceTwoId']);

        if ($pollChoices['pollChoiceThreeId']) {
            if ($this->find($pollChoices['pollChoiceThreeId'])) {
                if ($pollChoices['pollChoiceThree']) {
                    $this->update(array('pollChoice' => $pollChoices['pollChoiceThree']), " pollChoices.id = " . $pollChoices['pollChoiceThreeId']);
                } else {
                    $this->delete(" pollChoices.id = " . $pollChoices['pollChoiceThreeId']);
                }
            }
        } else {
            $this->insert(array('pollChoice' => $pollChoices['pollChoiceThree'], "pollId" => $pollId));
        }

        if ($pollChoices['pollChoiceFourId']) {
            if ($this->find($pollChoices['pollChoiceFourId'])) {
                if ($pollChoices['pollChoiceFour']) {
                    $this->update(array('pollChoice' => $pollChoices['pollChoiceFour']), " pollChoices.id =" . $pollChoices['pollChoiceFourId']);
                } else {
                    $this->delete(" pollChoices.id = " . $pollChoices['pollChoiceFourId']);
                }
            }
        } else {
            $this->insert(array('pollChoice' => $pollChoices['pollChoiceFour'], "pollId" => $pollId));
        }

        if ($pollChoices['pollChoiceFiveId']) {
            if ($this->find($pollChoices['pollChoiceFiveId'])) {
                if ($pollChoices['pollChoiceFive']) {
                    $this->update(array('pollChoice' => $pollChoices['pollChoiceFive']), " pollChoices.id = " . $pollChoices['pollChoiceFiveId']);
                } else {
                    $this->delete(" pollChoices.id = " . $pollChoices['pollChoiceFiveId']);
                }
            }
        } else {
            $this->insert(array('pollChoice' => $pollChoices['pollChoiceFive'], "pollId" => $pollId));
        }
    }

    function getChoicesByPollId($pollId){
        $select = $this->select()->where("pollId= $pollId");
        return $this->fetchAll($select)->toArray();
    }
    
    function incrementPollChoice($choiceId){
        $row = $this->find($choiceId)->current();
        $row->count++;
        return $row->save();
    }
    
    function sumPollChoices($pollId){
        $select = $this->select()->setIntegrityCheck(FALSE)->from("pollChoices",array('sum(count) as sum'))->where("pollId = $pollId");
        return $this->fetchRow($select)->toArray();
    }
    
    


}

