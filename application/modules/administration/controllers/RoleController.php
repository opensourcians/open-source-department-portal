<?php

class Administration_RoleController extends Zend_Controller_Action
{

    /**
     * FlashMessenger
     *
     * @var Zend_Controller_Action_Helper_FlashMessenger
     */
    protected $_flashMessenger = null;
    
    public function init()
    {
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        $this->initView();
    }

    public function indexAction()
    {
        $this->redirect("/administration/role/list");
    }

    public function listAction()
    {
        $roleModel = new Administration_Model_Role();
        $roles = $roleModel->listRoles();
        
        
         $page=$this->_getParam('page',1);
            $paginator = Zend_Paginator::factory($roles);
            $paginator->setItemCountPerPage(10);
            $paginator->setCurrentPageNumber($page);
            $this->view->messages = $this->_flashMessenger->getMessages();
            $this->view->paginator=$paginator;
    }

    public function addAction()
    {
        $roleForm = new Administration_Form_Role();
        $resourcesModel = new Administration_Model_Resource() ;
        
        $this->view->form = $roleForm;
        $this->view->post = "/administration/role/add";
        $this->view->viewName = "Add Role";
        $this->view->resources = $resourcesModel->listAll();
        
        if($this->getRequest()->isPost()){
            if($roleForm->isValid($this->getRequest()->getParams())){
                $roleModel = new Administration_Model_Role();
                $roleId = $roleModel->addRole($roleForm->getValue("name"));
                
                if($roleId){
                    if($this->getRequest()->getParam("resources")){
                        $resourcesArr = $this->getRequest()->getParam("resources");
                        $roleResourceModel = new Administration_Model_RoleResource();
                        $roleResourceModel->addRolePermissions($roleId,$resourcesArr);
                        $sess = new Zend_Session_Namespace('OSD_ACL');
                        $sess->clearACL = TRUE;
                }
                    $this->_flashMessenger->addMessage('Role added successfully!');
                    $this->redirect("/administration/role/list");
                }
            } else {
                
                $this->view->errors = $roleForm->getMessages();
            }
            
        }
        
        $this->render("form");
    }
    
    public function editAction()
    {
        //Get instances
        $roleForm = new Administration_Form_Role();
        $roleModel = new Administration_Model_Role();
        $resourcesModel = new Administration_Model_Resource() ;
        $roleResourcesModel = new Administration_Model_RoleResource();
        
        //get role id from request
        $id = $this->getRequest()->getParam('id');
        
        //get role information
        $role = $roleModel->getRoleByID($id);
        //get role permissions
        $rolePermissions = $roleResourcesModel->getRolePermissions($id);
        
        //send values to view
        $this->view->post = "/administration/role/edit";
        $this->view->viewName = "Edit Role";
        $this->view->rolePermissions = $rolePermissions;
        $this->view->resources = $resourcesModel->listAll();
        if($role){
        $roleForm->populate($role[0]);
        }else{
                    $this->_flashMessenger->addMessage('Sorry!, but there is no role with this id!');
                    $this->redirect("/administration/role/list");
        }
        
        if($this->getRequest()->isPost()){
            if($roleForm->isValid($this->getRequest()->getParams())){
                
                $result = $roleModel->editRole($roleForm->getValue("id"),$roleForm->getValue("name"));
                if($this->getRequest()->getParam("resources")){
                        $resourcesArr = $this->getRequest()->getParam("resources");
                        $roleResourcesModel->editRolePermissions($id, $resourcesArr);
                        $sess = new Zend_Session_Namespace('OSD_ACL');
                        $sess->clearACL = TRUE;
                }
                    $this->_flashMessenger->addMessage('Role edited successfully!');
                    $this->redirect("/administration/role/list");
                
            }
        }
        
        $this->view->form = $roleForm;
        $this->render("form");
        
    }
    
    public function deleteAction()
    {
        //diable layout and view rendering as Ajax is used
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
        //get item id from ajax request
        $id = $this->getRequest()->getParam("id");
        if ($id != 0 && $id != 1 && $id != 2){
            $roleModel = new Administration_Model_Role();
            $result = $roleModel->deleteRole($id);
            if($result){
                $this->_flashMessenger->addMessage('Role deleted successfully!');
                echo $result;
            }
        }
    }


}





