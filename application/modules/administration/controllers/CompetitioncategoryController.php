<?php

class Administration_CompetitioncategoryController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $list = new Administration_Model_Competitioncategory();
        $this->view->data = $list->listCompetitioncategory();
    }

    public function addAction()
    {
        $add = new Administration_Model_Competitioncategory();
        if($this->_request->isPost())
        {
            $add->addCompetitioncategory($this->_request->getParam('name'));
            $this->redirect("/administration/competitioncategory/");
        }
    }

    public function editAction()
    {
       $select = new Administration_Model_Competitioncategory();
       $this->view->data=$select->getcompetitioncategoryByid($this->_request->getParam('id'));
       if($this->_request->isPost())
       {
           $select->editCompetitioncategory($this->_request->getParam('name'), $this->_request->getParam('id'));
           $this->redirect("/administration/competitioncategory/");
       }
    }

    public function deleteAction()
    {
        $delete = new Administration_Model_Competitioncategory();
        if($this->_request->isPost())
        {
            $delete->deleteCompetitioncategory($this->_request->getParam('id'));
            $this->redirect('/administration/competitioncategory');
        }
    }


}







