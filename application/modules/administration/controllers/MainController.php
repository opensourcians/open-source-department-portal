<?php

class Administration_MainController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        
        
        $mainForm = new Administration_Form_Main();
        $mainModel = new Administration_Model_Main();
        
        
        
        
        if($this->getRequest()->isPost()){
            
             if ($mainForm->isValid($this->getRequest()->getParams())) {
                 $data = $mainForm->getValues();
                 $newData = array_filter($data);
                 $mainModel->updateSettings($newData);
                 
             }
            }
        $settings = $mainModel->getSettings();
        $this->view->form = $mainForm;
        $mainForm->populate($settings[0]);
        
        
       
    }

}
